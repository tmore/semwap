# Formalization of semantics of a small imperative language in Agda
All concepts come from the book "Semantics with application"

Developed using Agda version 2.6.1, Agda's standard library 1.3, agda prelude (https://github.com/UlfNorell/agda-prelude)
(commit id: e132dd4) and agda-prelude-extras https://gitlab.com/tmore/agda-prelude-extras

This is a hobby project and would most likely benefit by a little bit of cleanup.



The library includes:
- A specification of the simple "While" language
- operational semantics (natural semantics) of While,
- small step operational semantics
- denotational semantics
- axiomatic semantics (Hoare logic) with Agda predicates as its logical language
- a simple abstract machine and translation from While to its code.
- Various lemmas and examples of each implementation.
- Proofs that all semantics are equivalent (by isomorphism)
- Proof that the natural semantics implies the behavior of the abstract machine
- Proof that the Hoare logic structure is complete and sound.

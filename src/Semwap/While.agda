{-
Author: Tomas Möre 2020

Contains the definition of the abstract syntax tree of While for reuse in other
modules.

-}
module Semwap.While where


open import Prelude
open import Prelude.Extras

open import Semwap.Expr


-- While language as extracted form Table  2.1
data While : Set where
  _:=_ : Var → Aexp → While
  skip : While
  _∶_ : While → While → While
  if'_then_else_ : Bexp → While → While → While
  while_run_ : Bexp → While → While
-- kinda abirtaty values, needs to be corrected some day.
infix 2 _:=_
-- ∶ is right assosiative
infixr 1 _∶_


instance
  SizedWhile : Sized While
  size {{SizedWhile}} (_ := _) = 1
  size {{SizedWhile}} (skip) = 1
  size {{SizedWhile}} (l ∶ r) = 1 + size l + size r
  size {{SizedWhile}} (if' _ then l else r) = 1 + size l + size r
  size {{SizedWhile}} (while _ run r) = 1 + size r



-- The book calls these 'semantic pairs' but in fact they are just pairs
-- We define this utility function for syntactic similarity to the book
⟨_,_⟩ : ∀ {n m} {A : Set n} {B : Set m} → A → B → A × B
⟨_,_⟩ A B = A , B

⟨_,_,_⟩ : ∀ {n m o} {A : Set n} {B : Set m} {C : Set o} → A → B → C → A × B × C
⟨_,_,_⟩ A B C = A , B , C

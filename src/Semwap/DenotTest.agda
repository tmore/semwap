{-# OPTIONS --sized-types #-}

module Semwap.DenotTest where
open import Size using (∞ ; Size) public

open import Prelude hiding (force)
open import Prelude.Extras.Eq
open import Prelude.Extras.Delay
open import Relation.Unary

import Level

open import Semwap.Expr
open import Semwap.While

import Semwap.DenotationalSem as DS



-- data _⇾_ : (While × State) → State → Set where
--   `[assₙₛ] : ∀ {x a s}
--            → Σ (Sₙₛ⟦ (x := a) ⟧ s ⇓) (λ d → extract d ≡ (s [ x ↦ 𝓐⟦ a ⟧ s ]))
--            → ⟨ (x := a) , s ⟩ ⇾ (s [ x ↦ 𝓐⟦ a ⟧ s ])
--   `[skipₙₛ] : ∀ {s}
--             → Σ (Sₙₛ⟦ skip ⟧ s ⇓) (λ d → extract d ≡ s)
--             → ⟨ skip , s ⟩ ⇾ s
--   `[compₙₛ] :  ∀ {S₁ S₂ s s' s''}
--            → (⟨ S₁ , s ⟩ ⇾ s')
--            → (⟨ S₂ , s' ⟩ ⇾ s'')
--            → Σ (Sₙₛ⟦ (S₁ ∶ S₂) ⟧ s ⇓) (λ d → extract d ≡ s'')
--            → ⟨ S₁ ∶ S₂ , s ⟩ ⇾ s''
--   `[ifₙₛᵗᵗ] : ∀ {S₁ S₂ s s' b}
--            → (ℬ⟦ b ⟧ s ≡ true)
--            → ⟨ S₁ , s ⟩ ⇾ s'
--            → Σ (Sₙₛ⟦ if' b then S₁ else S₂ ⟧ s ⇓) (λ d → extract d ≡ s')
--            → ⟨ if' b then S₁ else S₂ , s ⟩ ⇾ s'
--   `[ifₙₛᶠᶠ] : ∀ {S₁ S₂ s s' b}
--            → (ℬ⟦ b ⟧ s ≡ false)
--            → ⟨ S₂ , s ⟩ ⇾ s'
--            → Σ (Sₙₛ⟦ if' b then S₁ else S₂ ⟧ s ⇓) (λ d → extract d ≡ s')
--            → ⟨ if' b then S₁ else S₂ , s ⟩ ⇾ s'

--   `[whileₙₛᵗᵗ] :
--     ∀ {b S s s' s''}
--     → (ℬ⟦ b ⟧ s ≡ true)
--     → ⟨ S , s ⟩ ⇾ s'
--     → ⟨ while b run S , s' ⟩ ⇾ s''
--     → Σ (Sₙₛ⟦ while b run S ⟧ s ⇓) (λ d → extract d ≡ s'')
--     → ⟨ while b run S , s ⟩ ⇾ s''
--   `[whileₙₛᶠᶠ] :
--     ∀ {S s b}
--     → (ℬ⟦ b ⟧ s ≡ false)
--     → Σ (Sₙₛ⟦ while b run S ⟧ s ⇓) (λ d → extract d ≡ s)
--     → ⟨ while b run S , s ⟩ ⇾ s

-- numWhileNodes : {S : While} {s s' : State} → ⟨ S , s ⟩ ⇾ s' → Nat
-- numWhileNodes (`[assₙₛ] _) = 0
-- numWhileNodes (`[skipₙₛ] _) = 0
-- numWhileNodes (`[compₙₛ] l r _) = numWhileNodes l + numWhileNodes r
-- numWhileNodes (`[ifₙₛᵗᵗ] _ l _) = numWhileNodes l
-- numWhileNodes (`[ifₙₛᶠᶠ] _ r _) = numWhileNodes r
-- numWhileNodes (`[whileₙₛᵗᵗ] _ body tail x) =
--   numWhileNodes body + suc (numWhileNodes tail)
-- numWhileNodes (`[whileₙₛᶠᶠ] _ _) =  0

-- termProof : {S : While} {s s' : State} → ⟨ S , s ⟩ ⇾ s' → Σ (Sₙₛ⟦ S ⟧ s ⇓) (λ d → extract d ≡ s')
-- termProof (`[assₙₛ] x) = x
-- termProof (`[skipₙₛ] x) = x
-- termProof (`[compₙₛ] _ _ x) = x
-- termProof (`[ifₙₛᵗᵗ] _ _ x) = x
-- termProof (`[ifₙₛᶠᶠ] _ _ x) = x
-- termProof (`[whileₙₛᵗᵗ] _ _ _ x) = x
-- termProof (`[whileₙₛᶠᶠ] _ x) = x


-- [assₙₛ]' : ∀ {x a s} → ⟨ (x := a) , s ⟩ ⇾ (s [ x ↦ 𝓐⟦ a ⟧ s ])
-- [assₙₛ]' {x} {a} {s} =
--   `[assₙₛ] ( now (s [ x ↦ (𝓐⟦ a ⟧ s) ])
--            , refl
--            )

-- [skipₙₛ]' : ∀ {s} →  ⟨ skip , s ⟩ ⇾ s
-- [skipₙₛ]' {s} = `[skipₙₛ] {s = s} (now s , refl)


-- [compₙₛ]' :
--   ∀ {S₁ S₂ s s' s''}
--   → (⟨ S₁ , s ⟩ ⇾ s') → (⟨ S₂ , s' ⟩ ⇾ s'')
--   → ⟨ S₁ ∶ S₂ , s ⟩ ⇾ s''
-- [compₙₛ]' {S₁} {S₂} {s} {s'} {s''}
--          ⟨S₁,s⟩⇾s'
--          ⟨S₂,s'⟩⇾s''
--   rewrite (sym (snd (termProof ⟨S₁,s⟩⇾s'))) =
--    `[compₙₛ] ⟨S₁,s⟩⇾s'
--             (transport
--               (λ z → ⟨ S₂ , z ⟩ ⇾ s'')
--               (snd (termProof ⟨S₁,s⟩⇾s'))
--               ⟨S₂,s'⟩⇾s'')
--             ( bind-⇓ S₁⇓ S₂⇓
--             , (extract (bind-⇓ S₁⇓ S₂⇓)
--             ≡⟨ extract-bind S₁⇓ S₂⇓ ⟩
--               extract S₂⇓
--             ≡⟨ snd (termProof ⟨S₂,s'⟩⇾s'') ⟩
--               s''
--             ∎))
--    where
--      S₁⇓ = (fst (termProof ⟨S₁,s⟩⇾s'))
--      S₂⇓ = (fst (termProof ⟨S₂,s'⟩⇾s''))


-- [ifₙₛᵗᵗ]' :
--   ∀ {S₁ S₂ s s' b}
--   → (ℬ⟦ b ⟧ s ≡ true)
--   → ⟨ S₁ , s ⟩ ⇾ s'
--   → ⟨ if' b then S₁ else S₂ , s ⟩ ⇾ s'
-- [ifₙₛᵗᵗ]' {S₁} {S₂} {s} {s'} {b} ℬ⟦b⟧s≡true ⟨S₁,s⟩⇾s'  =
--   `[ifₙₛᵗᵗ] ℬ⟦b⟧s≡true
--           ⟨S₁,s⟩⇾s'
--           (transport (λ z → Σ ((if z then Sₙₛ⟦ S₁ ⟧ s else (Sₙₛ⟦ S₂ ⟧ s)) ⇓)
--                             (λ d → extract d ≡ s'))
--                     (sym ℬ⟦b⟧s≡true)
--                     (termProof ⟨S₁,s⟩⇾s'))
-- [ifₙₛᶠᶠ]' :
--   ∀ {S₁ S₂ s s' b}
--   → (ℬ⟦ b ⟧ s ≡ false)
--   → ⟨ S₂ , s ⟩ ⇾ s'
--   → ⟨ if' b then S₁ else S₂ , s ⟩ ⇾ s'
-- [ifₙₛᶠᶠ]' {S₁} {S₂} {s} {s'} {b} ℬ⟦b⟧s≡false ⟨S₂,s⟩⇾s'  =
--   `[ifₙₛᶠᶠ] ℬ⟦b⟧s≡false
--           ⟨S₂,s⟩⇾s'
--           (transport (λ z → Σ ((if z then Sₙₛ⟦ S₁ ⟧ s else (Sₙₛ⟦ S₂ ⟧ s)) ⇓)
--                             (λ d → extract d ≡ s'))
--                     (sym ℬ⟦b⟧s≡false)
--                     (termProof ⟨S₂,s⟩⇾s'))

-- [whileₙₛᵗᵗ]' :
--   ∀ {b S s s' s''}
--   → (ℬ⟦ b ⟧ s ≡ true)
--   → ⟨ S , s ⟩ ⇾ s'
--   → ⟨ while b run S , s' ⟩ ⇾ s''
--   → ⟨ while b run S , s ⟩ ⇾ s''
-- [whileₙₛᵗᵗ]' {b} {S} {s} {s'} {s''} ℬ⟦b⟧s≡true ⟨S,s⟩⇾s' ⟨WS,s'⟩⇾s''
--   rewrite (sym (snd (termProof ⟨S,s⟩⇾s')))
--     =
--     solve (later (bind-⇓ S⇓ WS⇓))
--           ( extract {d = inner-recur} (later (bind-⇓ S⇓ WS⇓))
--            ≡⟨ extract-bind S⇓ WS⇓ ⟩
--              extract WS⇓
--            ≡⟨ snd (termProof ⟨WS,s'⟩⇾s'') ⟩
--              s''
--             ∎
--            )
--   where
--     S⇓ = fst (termProof ⟨S,s⟩⇾s')
--     WS⇓ = fst (termProof ⟨WS,s'⟩⇾s'')
--     inner-recur : Delay State ∞
--     inner-recur = later (natSemLoop S (Cofix.aux Domain (whileSem b S)) s)
--     reduction : (if ℬ⟦ b ⟧ s then inner-recur else now s) ≡ inner-recur
--     reduction = cong (if_then inner-recur else now s) ℬ⟦b⟧s≡true
--     solve : (inner-recur⇓ : inner-recur ⇓)
--           → extract inner-recur⇓ ≡ s''
--           → ⟨ while b run S , s ⟩ ⇾ s''
--     solve ir⇓ gives-s'' rewrite (sym reduction) =
--       `[whileₙₛᵗᵗ] ℬ⟦b⟧s≡true
--                    ⟨S,s⟩⇾s'
--                    (transport
--                      (λ z → ⟨ while b run S , z ⟩ ⇾ s'')
--                      (snd (termProof ⟨S,s⟩⇾s'))
--                      ⟨WS,s'⟩⇾s'')
--                     (ir⇓ , gives-s'')

-- [whileₙₛᶠᶠ]' :
--   ∀ {S s b}
--   → (ℬ⟦ b ⟧ s ≡ false)
--   → ⟨ while b run S , s ⟩ ⇾ s
-- [whileₙₛᶠᶠ]' {S} {s} {b} ℬ⟦b⟧s≡false rewrite (sym ℬ⟦b⟧s≡false) =
--     solve (now s)
--           refl
--   where
--     inner-recur : Delay State ∞
--     inner-recur = later (natSemLoop S (Cofix.aux Domain (whileSem b S)) s)
--     reduction : (if ℬ⟦ b ⟧ s then inner-recur else now s) ≡ now s
--     reduction = cong (if_then inner-recur else now s) ℬ⟦b⟧s≡false
--     solve : (inner-recur⇓ : now s ⇓)
--           → extract inner-recur⇓ ≡ s
--           → ⟨ while b run S , s ⟩ ⇾ s
--     solve ir⇓ gives-s'' rewrite (sym reduction) =
--       `[whileₙₛᶠᶠ] ℬ⟦b⟧s≡false ( ir⇓ , gives-s'')


-- module _ where
--   tProof :  {S : While} {s s'' : State}
--          →  ⟨ S , s ⟩⇾ s''
--          →  (Sₙₛ⟦  S ⟧ s ⇓)
--   tProof (nsTuple (t , _)) = t
--   open import Codata.Conat using (toℕ)


  -- nwtc : {S : While} {s s' : State} → (p : ⟨ S , s ⟩ ⇾ s')
  --      → numWhileNodes p ≡ toℕ (length-⇓ (fst (termProof p)))
  -- nwtc {S = x := a} p@(`[assₙₛ] _)
  --   with termProof p
  -- ...| (now _ , _) = refl
  -- nwtc p@(`[skipₙₛ] _)
  --   with termProof p
  -- ...| (now _ , _) = refl
  -- nwtc p@(`[compₙₛ] {S₁} {S₂} {s} {s'} {s''} l r (bind⇓ , _)) =
  --      numWhileNodes p
  --   ≡⟨ refl ⟩
  --     numWhileNodes l + numWhileNodes r
  --   ≡⟨ cong₂ _+_ (nwtc l) (nwtc r) ⟩
  --     toℕ (length-⇓ l⇓) + toℕ (length-⇓ r⇓)
  --   ≡⟨ cong (_ +_) bar ⟩
  --     toℕ (length-⇓ l⇓) + toℕ (length-⇓ r'⇓)
  --   ≡⟨ sym (bind-⇓-lenght-add bind⇓ l⇓ r'⇓)
  --     ⟩
  --     toℕ (length-⇓ bind⇓)
  --   ∎
  --   where
  --     l⇓ = fst (termProof l)
  --     r⇓ = fst (termProof r)
  --     r'⇓ : (Sₙₛ⟦ S₂ ⟧ extract l⇓) ⇓
  --     r'⇓ = (transport (λ z → (Sₙₛ⟦ S₂ ⟧ z) ⇓) (sym (snd (termProof l))) r⇓)
  --     bar : toℕ (length-⇓ r⇓) ≡ toℕ (length-⇓ r'⇓)
  --     bar rewrite (snd (termProof l)) = refl
  -- nwtc (`[ifₙₛᵗᵗ] {S₁} {S₂} {s} {s'} {b} ℬ⟦b⟧s≡true l (if⇓ , gives-s'))
  --   rewrite sym (cong (if_then Sₙₛ⟦ S₁ ⟧ s else (Sₙₛ⟦ S₂ ⟧ s)) ℬ⟦b⟧s≡true) =
  --      {!!}
  -- nwtc (`[ifₙₛᶠᶠ] _ r _) = numWhileNodes r
  -- numWhileNodes (`[whileₙₛᵗᵗ] _ body tail x) =
  --   numWhileNodes body + suc (numWhileNodes tail)
  -- numWhileNodes (`[whileₙₛᶠᶠ] _ _) =  0

  -- while-size-add :
  --       {b : Bexp} {S : While} {s s' s'' : State}
  --         → (ℬ⟦b⟧s≡true : ℬ⟦ b ⟧ s ≡ true)
  --         → (⟨S,s⟩⇾s' : ⟨ S , s ⟩⇾ s')
  --         → (⟨WS,s'⟩⇾s'' : ⟨ while b run S , s' ⟩⇾ s'')
  --         → toℕ (length-⇓ (tProof ([whileₙₛᵗᵗ] ℬ⟦b⟧s≡true ⟨S,s⟩⇾s' ⟨WS,s'⟩⇾s'')))
  --           ≡ toℕ (length-⇓ (tProof ⟨S,s⟩⇾s')) + toℕ (length-⇓ (tProof ⟨WS,s'⟩⇾s''))
  -- while-size-add ℬ⟦b⟧s≡true ⟨S,s⟩⇾s' ⟨WS,s'⟩⇾s'' = {!!}

  -- while-size-decreases :
  --   {b : Bexp} {S : While} {s s'' : State}
  --   → (p : ⟨ while b run S , s ⟩⇾ s'')
  --   → (ℬ⟦b⟧s≡true : ℬ⟦ b ⟧ s ≡ true)
  --   → toℕ (length-⇓ (tProof (snd (snd ([whileₙₛᵗᵗ]-destr p ℬ⟦b⟧s≡true)))))
  --     <  toℕ (length-⇓ (tProof p))
  -- while-size-decreases p ℬ⟦b⟧s≡true
  --   with ([whileₙₛᵗᵗ]-destr p ℬ⟦b⟧s≡true)
  -- ...| (a , (b , c)) = {!!}

  -- -- [whileₙₛᵗᵗ]-destr :
  -- --   {b : Bexp} {S : While} {s s'' : State}
  -- --   → ⟨ while b run S , s ⟩⇾ s'' → ℬ⟦ b ⟧ s ≡ true
  -- --   → Σ State (λ s' → (⟨ S , s ⟩⇾ s') × ⟨ while b run S , s' ⟩⇾ s'')

-- This is a version of a context where the state is non total
module Semwap.NonTotal where

open import Data.Nat using (ℕ)
import Data.Nat as ℕ
open import Data.Integer using (ℤ)
import Data.Integer as ℤ
import Data.Integer.Properties as ℤ
open import Data.Bool using (Bool ; if_then_else_ ; not ; _∨_ ; _∧_) renaming (true to tt ; false to ff)
open import Function

import Relation.Nullary as Dec
import Relation.Nullary.Decidable as Dec

open import Data.Maybe using (Maybe ; just ; nothing)

open import Data.Product using (_×_ ; _,_)

-- Variables are natural
Var : Set
Var = ℕ

-- Values are either not defiened or some
Val : Set
Val = Maybe ℤ


module Val where
  open Data.Maybe using ( _>>=_  )renaming (just to pure ; ap to _<*>_ )
  infix 8 _==_
  _==_ : Val → Val → Maybe Bool
  _==_ ma mb = do
    a ← ma
    b ← mb
    pure (Dec.isYes (a ℤ.≟ b))

  infix 8 _≤_
  _≤_ : Val → Val → Maybe Bool
  _≤_ ma mb = do
    a ← ma
    b ← mb
    pure (Dec.isYes (a ℤ.≤? b))

val : ℤ → Val
val = just

*-val : Val
*-val = nothing

State : Set
State = Var → Val

[] : State
[] = λ _ → nothing

infixr 21 _↦_]
_↦_] : Var → ℤ → State
_↦_] var val  = λ v → if var ℕ.≡ᵇ v then just val else nothing


[_↦_﹐_ : Var → ℤ → State → State
[ var ↦ val ﹐ state =
  λ v → if var ℕ.≡ᵇ v then just val else state v

foo : State
foo = [ 0 ↦ (ℤ.+ 1) ﹐ 1 ↦ (ℤ.+ 2) ]



data Aexp : Set where
  𝕟 : ℕ → Aexp
  𝕧 : Var → Aexp
  _+_ : Aexp → Aexp → Aexp
  _*_ : Aexp → Aexp → Aexp
  _-_ : Aexp → Aexp → Aexp

-_ : Aexp → Aexp
- a = 𝕟 0 - a

module _ where
  open Data.Maybe renaming (just to pure ; ap to _<*>_)

  -- As defined in table 1.8
  𝓐⟦_⟧_ : Aexp → State → Val
  𝓐⟦ 𝕟 n ⟧ _ = just (ℤ.+ n)
  𝓐⟦ 𝕧 v ⟧ s = s v
  𝓐⟦ a₁ + a₂ ⟧ s = ⦇ 𝓐⟦ a₁ ⟧ s ℤ.+  𝓐⟦ a₂ ⟧ s ⦈
  𝓐⟦ a₁ * a₂ ⟧ s = ⦇ 𝓐⟦ a₁ ⟧ s ℤ.*  𝓐⟦ a₂ ⟧ s ⦈
  𝓐⟦ a₁ - a₂ ⟧ s = ⦇ 𝓐⟦ a₁ ⟧ s ℤ.-  𝓐⟦ a₂ ⟧ s ⦈



-- Ex 1.8 Proven by agda

data Bexp : Set where
  true : Bexp
  false : Bexp
  _==_ : Aexp → Aexp → Bexp
  _<=_ : Aexp → Aexp → Bexp
  ! : Bexp → Bexp
  _&&_ : Bexp → Bexp → Bexp

module _ where
  open Data.Maybe renaming (just to pure ; ap to _<*>_)


  ℬ⟦_⟧_ : Bexp → State → Maybe Bool
  ℬ⟦ true ⟧ _ = pure tt
  ℬ⟦ false ⟧ _ = pure ff
  ℬ⟦ a₁ == a₂ ⟧ s = 𝓐⟦ a₁ ⟧ s Val.== 𝓐⟦ a₁ ⟧ s
  ℬ⟦ a₁ <= a₂ ⟧ s = 𝓐⟦ a₁ ⟧ s Val.≤ 𝓐⟦ a₁ ⟧ s
  ℬ⟦ ! b ⟧ s = map not (ℬ⟦ b ⟧ s)
  ℬ⟦ b₁ && b₂ ⟧ s = ⦇ (ℬ⟦ b₁ ⟧ s) ∧ (ℬ⟦ b₂ ⟧ s) ⦈

{-

Author: Tomas Möre 2020 with help from Paolo G. Giarrusso making it denotational

-}
module Semwap.DenotationalSem where

open import Prelude hiding (force)
open import Prelude.Extras
open import Prelude.Extras.Delay
open import Relation.Unary

import Level

open import Semwap.Expr
open import Semwap.While

-- The actual natural semantic function as extracted from Table 2.1
-- Here Delay gives us the possibility of reasoning about non termination
Domain : Size → Set
Domain i = State → Delay State i

-- The actual natural semantic function as extracted from Table 2.1
-- Here Delay gives us the possibility of reasoning about non termination
mutual
  -- defined separately because of some technicalites with pattern matching
  -- lambdas


  S⟦_⟧_ : ∀ {i} → While → State → Delay State i
  S⟦ x := a ⟧  s = now (s [ x ↦ (𝓐⟦ a ⟧ s) ])
  S⟦ skip ⟧ s = now s
  S⟦ S₁ ∶ S₂ ⟧  s =
    (S⟦ S₁ ⟧ s) >>= S⟦ S₂ ⟧_
  S⟦ if' guard then S₁ else S₂ ⟧ s =
    if ℬ⟦ guard ⟧ s
    then S⟦ S₁ ⟧ s
    else S⟦ S₂ ⟧ s
  S⟦ while guard run S ⟧ s =
    cofix Domain
         (λ rec s' →
             if ℬ⟦ guard ⟧ s'
             then later (whileThunk S rec s')
             else now s'
         ) s


  whileThunk : ∀ {i} → While → Thunk (λ j → State → Delay State j) i → State → Thunk (Delay State) i
  whileThunk S rec s .force =  ((S⟦ S ⟧ s) >>= force rec)

  -- Definied as a synonym in types later on
  whileSem : Bexp → While → ∀[ Thunk Domain ⇒ Domain ]
  whileSem guard S rec s =
    if ℬ⟦ guard ⟧ s
    then later (whileThunk S rec s)
    else now s

  cofix-unfold : (b : Bexp) (S : While) (s : State) → ℬ⟦ b ⟧ s ≡ true
               → cofix Domain (whileSem b S) s ≡ later (whileThunk S (Cofix.aux Domain (whileSem b S)) s)
  cofix-unfold b S s beq rewrite beq = refl


  private
    x y z : Var
    x = 0
    y = 1
    z = 2

-- The natural semantic relation as defined in chapter 2. The relations says
-- that ⟨ S 、 s ⟩⇾ s' is valid if S executed with s terminates and the final
-- state is s' Here we encode it by saying that there exists a proof of
-- termination and the results is equivalent to s'
-- ⟨_⸴_⟩⇾_ : While → State → State → Set
-- ⟨_⸴_⟩⇾_ S s s' =
--  Σ (natSem S s ⇓) (λ d → Delay.extract d ≡ s')

data _↠_ : While × State → State → Set where
  semProof : ∀ {S s s'} → (d : S⟦ S ⟧ s ⇓) → extract d ≡ s' → ⟨ S , s ⟩ ↠ s'

termProof : ∀ {S s s'} → ⟨ S , s ⟩ ↠ s' → S⟦ S ⟧ s ⇓
termProof (semProof x _ ) = x

resultProof : ∀ {S s s'} → (wp : ⟨ S , s ⟩ ↠ s')
            → extract (termProof wp) ≡ s'
resultProof (semProof _ x) = x

-- record _⇾_ : (While × State) → State → Set where
--   constructor semProof
--   field
--     semProof' : ∀ {S s s'} → (d : S⟦ S ⟧ s ⇓) → extract d ≡ s' → ⟨ S , s ⟩ ⇾ s'

instance
  SizeDSSemProof : ∀ {S s s'} → Sized (⟨ S , s ⟩ ↠ s')
  size {{SizeDSSemProof}} (semProof termProof _) =
    size termProof


-- record ⟨_,_⟩⇾_ (S : While) (s : State) (s' : State) : Set where
--   inductive
--   constructor semProof_
--   field
--      semProof' :  Σ (S⟦ S ⟧ s ⇓) (λ d → extract d ≡ s')
-- open ⟨_,_⟩⇾_ {{...}} public

-- Utilies on Natural semantics tuple

-- Also Theorem 2.9
-- The resulting value of two semantic tuples with the same statement and
-- starting state are unique
result-sem-unique :
  {S : While} {s s' s'' : State}
  → ⟨ S , s ⟩ ↠ s' → ⟨ S , s ⟩ ↠ s''
  → s' ≡ s''
result-sem-unique {S} (semProof Sₗ⇓ S↠s') (semProof Sᵣ⇓ S↠s'')
  rewrite ⇓-unique Sₗ⇓ Sᵣ⇓ = (sym S↠s') ⟨≡⟩ S↠s''

-- -- result-sem-unique' :
-- --   {S : While} {s s' s'' : State}
-- --   → ⟨ S , s ⟩↠ s' ≡ ⟨ S , s ⟩↠ s''
-- -- result-sem-unique' {skip} {s} = refl

-- Semantic rules as defined in Table 2.1
[ass] : ∀ {x a s} → ⟨ (x := a) , s ⟩ ↠ (s [ x ↦ 𝓐⟦ a ⟧ s ])
[ass] {x} {a} {s} =
  semProof (now (s [ x ↦ (𝓐⟦ a ⟧ s) ]))
          refl


[skip] : ∀ {s} →  ⟨ skip , s ⟩ ↠ s
[skip] {s} = semProof (now s) refl

[comp] :
  ∀ {S₁ S₂ s s' s''}
  → ⟨ S₁ , s ⟩ ↠ s' → ⟨ S₂ , s' ⟩ ↠ s''
  → ⟨ S₁ ∶ S₂ , s ⟩ ↠ s''
[comp] {S₁} {S₂} {s} {s'} {s''}
         (semProof S₁⇓ S₁↠s')
         (semProof S₂⇓ S₂↠s'')
  rewrite (sym S₁↠s')
  =
   semProof
     (bind-⇓ S₁⇓ S₂⇓)
     ( extract (bind-⇓ S₁⇓ S₂⇓)
     ≡⟨ extract-bind S₁⇓ S₂⇓ ⟩
       extract S₂⇓
     ≡⟨ S₂↠s'' ⟩
       s''
     ∎)

[ifᵗᵗ] :
  ∀ {S₁ S₂ s s' b}
  → (ℬ⟦ b ⟧ s ≡ true) → ⟨ S₁ , s ⟩ ↠ s'
  → ⟨ if' b then S₁ else S₂ , s ⟩ ↠ s'
[ifᵗᵗ] {S₁} {S₂} {s} {s'} {b} ℬ⟦b⟧s≡true (semProof S₁⇓  S₁↠s')
  rewrite (sym (cong (if_then S⟦ S₁ ⟧ s else S⟦ S₂ ⟧ s) ℬ⟦b⟧s≡true)) =
  semProof S₁⇓ S₁↠s'

[ifᶠᶠ] :
  ∀ {S₁ S₂ s s' b}
  → (ℬ⟦ b ⟧ s ≡ false) → ⟨ S₂ , s ⟩ ↠ s'
  → ⟨ if' b then S₁ else S₂ , s ⟩ ↠ s'
[ifᶠᶠ] {S₁} {S₂} {s} {s'} {b} ℬ⟦b⟧s≡false (semProof S₂⇓  S₂↠s')
  rewrite (sym (cong (if_then S⟦ S₁ ⟧ s else S⟦ S₂ ⟧ s) ℬ⟦b⟧s≡false)) =
   semProof S₂⇓ S₂↠s'





-- -- unfold-while-cofix2 : (S : While) (s : State) (b : Bexp)  → ℬ⟦ b ⟧ s ≡ true →
-- --   cofix Domain (whileSem b S) s
-- --   ≡ later (λ where . force →
-- --                            (bind (S⟦ S ⟧ s) (force (λ where .force → cofix (λ i → List (Σ Nat (λ _ → Int)) → Delay (List (Σ Nat (λ _ → Int))) i)
-- --                                                                             (λ rec s₁ → if ℬ⟦ b ⟧ s₁ then later (natSemLoop S _ s₁) else now s₁)))))
-- --unfold-while-cofix2 S s b ℬ⟦b⟧s≡true rewrite ℬ⟦b⟧s≡true = refl


[whileᵗᵗ] :
  ∀ {b S s s' s''}
  → (ℬ⟦ b ⟧ s ≡ true)
  → ⟨ S , s ⟩ ↠ s'
  → ⟨ while b run S , s' ⟩ ↠ s''
  → ⟨ while b run S , s ⟩ ↠ s''
[whileᵗᵗ] {b} {S} {s} {s'} {s''} ℬ⟦b⟧s≡true (semProof S⇓ S↠s') (semProof WS⇓ WS↠s'')
 rewrite (sym S↠s') =
    solve (later (bind-⇓ S⇓ WS⇓))
          ( extract {d = inner-recur} (later (bind-⇓ S⇓ WS⇓))
           ≡⟨ extract-bind S⇓ WS⇓ ⟩
             extract WS⇓
           ≡⟨ WS↠s'' ⟩
             s''
            ∎
           )
  where
    inner-recur : Delay State ∞
    inner-recur = later (whileThunk S (Cofix.aux Domain (whileSem b S)) s)
    reduction : (if ℬ⟦ b ⟧ s then inner-recur else now s) ≡ inner-recur
    reduction = cong (if_then inner-recur else now s) ℬ⟦b⟧s≡true
    solve : (inner-recur⇓ : inner-recur ⇓)
          → extract inner-recur⇓ ≡ s''
          → ⟨ while b run S , s ⟩ ↠ s''
    solve ir⇓ gives-s'' rewrite (sym reduction) = semProof ir⇓ gives-s''

[whileᶠᶠ] :
  ∀ {S s b}
  → (ℬ⟦ b ⟧ s ≡ false)
  → ⟨ while b run S , s ⟩ ↠ s
[whileᶠᶠ] {S} {s} {b} ℬ⟦b⟧s≡false =
    solve (now s)
          refl
  where
    inner-recur : Delay State ∞
    inner-recur = later (whileThunk S (Cofix.aux Domain (whileSem b S)) s) -- later (natSemLoop b S s)
    reduction : (if ℬ⟦ b ⟧ s then inner-recur else now s) ≡ now s
    reduction = cong (if_then inner-recur else now s) ℬ⟦b⟧s≡false
    solve : (inner-recur⇓ : now s ⇓)
          → extract inner-recur⇓ ≡ s
          → ⟨ while b run S , s ⟩ ↠ s
    solve ir⇓ gives-s'' rewrite (sym reduction) = semProof ir⇓ gives-s''


_ : ⟨ (x := 𝕟 1) , ε ⟩ ↠ (ε [ x ↦ 1 ])
_ = [ass]


module _ where
  private
    s₀ s₁ s₂ s₃ : State
    s₀ = (ε [ x ↦ 5 ]) [ y ↦ 7 ]
    s₁ = s₀ [ z ↦ 5 ]
    s₂ = s₁ [ x ↦ 7 ]
    s₃ = s₂ [ y ↦ 5 ]

  ex-2-1 : ⟨ ((z := 𝕧 x ∶ x := 𝕧 y) ∶ y := 𝕧 z) , s₀ ⟩ ↠ s₃
  ex-2-1 = [comp] ([comp] [ass] [ass]) [ass]


module _ where
  private
    s₀  : State
    s₀ = ε [ x ↦ 3 ]

  ex-2-2 : ⟨ (y := 𝕟 1 ∶ while !' (𝕧 x ==' 𝕟 1) run (y := 𝕧 y *' 𝕧 x ∶ x := 𝕧 x -' 𝕟 1)) , s₀ ⟩ ↠ (s₀ [ y ↦ 6 ] [ x ↦ 1 ])
  ex-2-2 = [comp] [ass]
                   ([whileᵗᵗ] refl
                              ([comp] [ass] [ass])
                              ([whileᵗᵗ] refl
                                         ([comp] [ass] [ass])
                                         ([whileᶠᶠ] refl)))


module _ where
  private
    s₀ : State
    s₀ = ε [ x ↦ 17 ] [ y ↦ 5 ]

  ex-2-3 : ⟨ (z := 𝕟 0 ∶ while 𝕧 y <=' 𝕧 x run (z := 𝕧 z +' 𝕟 1 ∶ x := 𝕧 x -' 𝕧 y)) , s₀ ⟩ ↠ _
  ex-2-3 = [comp] [ass]
                    ([whileᵗᵗ]
                      refl
                      ([comp] [ass] [ass])
                      ([whileᵗᵗ]
                        refl
                        ([comp] [ass] [ass])
                        ([whileᵗᵗ]
                          refl
                          ([comp] [ass] [ass])
                          ([whileᶠᶠ] refl))))

-- -- Semantic equivalence

record _≡ds_ (S₁ S₂ : While) : Set where
  constructor ns-eq
  field
    S₁⇒S₂ : ((s s' : State) → ⟨ S₁ , s ⟩ ↠ s' → ⟨ S₂ , s ⟩ ↠ s')
    S₂⇒S₁ : ((s s' : State) → ⟨ S₂ , s ⟩ ↠ s' → ⟨ S₁ , s ⟩ ↠ s')


≡ds-refl : {S : While} → (S ≡ds S)
≡ds-refl =
  ns-eq (λ s s' t → t) (λ s s' t → t)

≡ds-sym : {S₁ S₂ : While} → S₁ ≡ds S₂ → S₂ ≡ds S₁
≡ds-sym (ns-eq to from) = ns-eq from to

≡ds-trans : {S₁ S₂ S₃ : While} → S₁ ≡ds S₂ → S₂ ≡ds S₃ → S₁ ≡ds S₃
≡ds-trans (ns-eq to from) (ns-eq to' from') =
  ns-eq (λ s s' S₁ → to' s s' (to s s' S₁))
        (λ s s' S₃ → from s s' (from' s s' S₃))

-- -- ≡-cong {S₁ S₂ S₃ : While} (f :  → S₁ ≡ S₂ → S₂ ≡ S₃ → S₁ ≡ S₃


--  If we know that a ⟨ while b run S , s ⟩↠ s'' holds and that the boolean
-- guard is true in s then we can extract the proofs used to construct said
-- statement. That is, there exists a state `s'` such that `⟨ S , s ⟩↠ s'` and
-- `⟨ while b run S , s' ⟩↠ s''` We also show that the size of the original
-- statement is equal to the 1 + the size of the parts of the split. Proved
-- thogether with the main lemma since a separate proof is harder. Needed for
-- use this in induction on the proof.
--
-- See the two following definitions for a separation of concerns split
[whileᵗᵗ]-destr-and-size :
  {b : Bexp} {S : While} {s s'' : State}
  → (wp : ⟨ while b run S , s ⟩ ↠ s'')
  → ℬ⟦ b ⟧ s ≡ true
  → Σ State (λ s' → Σ ((⟨ S , s ⟩ ↠ s') × ⟨ while b run S , s' ⟩ ↠ s'')
                      (λ split → size wp ≡ 1 + size (fst split) + size (snd split)))
[whileᵗᵗ]-destr-and-size {b} {S} {s} {s''} (semProof WS⇓ WS→s'') ℬ⟦b⟧s≡true
  rewrite ℬ⟦b⟧s≡true
  with inspect WS⇓
...| (later bind⇓ , ingraph WS⇓≡laterd)
  rewrite WS⇓≡laterd =
       s' , (((semProof S⟦S⟧s⇓ refl) , (semProof S⟦while⟧s'⇓ (trans extract-S⟦while⟧s'⇓≡s'' extract-bind⇓≡s'')))
            , cong suc (bind-⇓-length-add bind⇓ S⟦S⟧s⇓ S⟦while⟧s'⇓))
  where
    S⟦S⟧s⇓ : _
    S⟦S⟧s⇓ = (bind-⇓-injₗ bind⇓)
    s' : State
    s' = extract S⟦S⟧s⇓
    S⟦while⟧s'⇓ : _
    S⟦while⟧s'⇓ = (bind-⇓-injᵣ {d = (S⟦_⟧_ S s)} bind⇓)

    extract-S⟦while⟧s'⇓≡s'' : extract S⟦while⟧s'⇓ ≡ extract bind⇓
    extract-S⟦while⟧s'⇓≡s'' =
      extract-bind-⇓-injᵣ≡extract-bind-⇓ {d = (S⟦_⟧_ S s)} bind⇓
    extract-bind⇓≡s'' : extract bind⇓ ≡ s''
    extract-bind⇓≡s'' with inspect WS⇓ | inspect WS→s''
    ...| (later d , ingraph WS⇓≡later) | (WS→s'' , _)
      rewrite WS⇓≡later = WS→s''


-- Specialized version of [whileᵗᵗ]-destr-and-size for retrieving the destructor
-- only.
[whileᵗᵗ]-destr :
  {b : Bexp} {S : While} {s s'' : State}
  → (wp : ⟨ while b run S , s ⟩ ↠ s'')
  → ℬ⟦ b ⟧ s ≡ true
  → Σ State (λ s' → ⟨ S , s ⟩ ↠ s' × ⟨ while b run S , s' ⟩ ↠ s'')
[whileᵗᵗ]-destr wp ℬ⟦b⟧s≡true =
  let (s' , (split , _ )) = [whileᵗᵗ]-destr-and-size wp ℬ⟦b⟧s≡true
  in s' , split

-- Specialized verson of [whileᵗᵗ]-destr-and-size for getting the size lemma
-- only
[whileᵗᵗ]-destr-size :
  {b : Bexp} {S : While} {s s'' : State}
  → (wp : ⟨ while b run S , s ⟩ ↠ s'')
  → (bp : ℬ⟦ b ⟧ s ≡ true)
  → size wp ≡ 1 + size (fst (snd ([whileᵗᵗ]-destr wp bp))) + size (snd (snd ([whileᵗᵗ]-destr wp bp)))
[whileᵗᵗ]-destr-size wp bp =
  let (_ , (_ , eq )) = [whileᵗᵗ]-destr-and-size wp bp
  in eq

[ifᵗᵗ]-destr :
  {b : Bexp} {S₁ S₂ : While} {s s' : State}
  → ⟨ if' b then S₁ else S₂ , s ⟩ ↠ s' → ℬ⟦ b ⟧ s ≡ true
  → ⟨ S₁ , s ⟩ ↠ s'
[ifᵗᵗ]-destr {b} {S₁} {S₂} {s} {s'} (semProof if⇓ if→s') ℬ⟦b⟧s≡true
  rewrite ℬ⟦b⟧s≡true = semProof if⇓ if→s'

[ifᶠᶠ]-destr :
  {b : Bexp} {S₁ S₂ : While} {s s' : State}
  → ⟨ if' b then S₁ else S₂ , s ⟩ ↠ s' → ℬ⟦ b ⟧ s ≡ false
  → ⟨ S₂ , s ⟩ ↠ s'
[ifᶠᶠ]-destr {b} {S₁} {S₂} {s} {s'} (semProof if⇓ if→s') ℬ⟦b⟧s≡false
  rewrite ℬ⟦b⟧s≡false = semProof if⇓ if→s'

-- Proof that composition can be split into two separate termination proofs, one
-- for each side. Includes proof that their sizes sum to the original proof
-- size. Proved together as a separate proof is tricky. Se following to definitions for separation of concerns
[comp]-destr-and-size :
  {S₁ S₂ : While} {s s'' : State}
  → (wp : ⟨ S₁ ∶ S₂ , s ⟩ ↠ s'')
  → Σ State (λ s' →
               Σ (⟨ S₁ , s ⟩ ↠ s' × ⟨ S₂ , s' ⟩ ↠ s'')
               (λ split → size wp ≡ size (fst split) + size (snd split)))
[comp]-destr-and-size {S₁} {S₂} {s} {s''} (semProof C⇓ C↠s'') =
  s'
  , (semProof S₁⇓  refl
  , (semProof (bind-⇓-injᵣ {d = (S⟦ S₁ ⟧ s)} C⇓)
              (trans (extract-bind-⇓-injᵣ≡extract-bind-⇓ {d = (S⟦ S₁ ⟧ s)} C⇓) C↠s'')
    ))
  , (bind-⇓-length-add C⇓ S₁⇓ (bind-⇓-injᵣ {d = (S⟦ S₁ ⟧ s)} C⇓))
  where
    S₁⇓ : S⟦ S₁ ⟧ s ⇓
    S₁⇓ = bind-⇓-injₗ C⇓
    s' : State
    s' = extract S₁⇓

[comp]-destr :
  {S₁ S₂ : While} {s s'' : State}
  → ⟨ S₁ ∶ S₂ , s ⟩ ↠ s''
  → Σ State (λ s' →  (⟨ S₁ , s ⟩ ↠ s' × ⟨ S₂ , s' ⟩ ↠ s''))
[comp]-destr wp =
  let (s' , (split , _)) = [comp]-destr-and-size wp
  in  s' , split

[comp]-destr-size :
  {S₁ S₂ : While} {s s'' : State}
  → (wp : ⟨ S₁ ∶ S₂ , s ⟩ ↠ s'')
  → size wp ≡ size (fst (snd ([comp]-destr wp))) + size (snd (snd ([comp]-destr wp)))
[comp]-destr-size wp =
  let (_ , (_ , eq)) = [comp]-destr-and-size wp
  in  eq


-- lem-2-5-raw : (b : Bexp) (S : While) (s : State)
--             → S⟦ while b run S ⟧ s ≡ S⟦ (if' b then (S ∶ while b run S) else skip) ⟧ s
-- lem-2-5-raw b S s with inspect (ℬ⟦ b ⟧ s)
-- ...| false , ingraph ℬ⟦b⟧s≡false
--            rewrite ℬ⟦b⟧s≡false = refl
-- ...| true , ingraph ℬ⟦b⟧s≡true  =
--      (S⟦ while b run S ⟧ s)
--   ≡⟨ cofix-unfold  b S s ℬ⟦b⟧s≡true ⟩
--     later (natSemLoop S (Cofix.aux Domain (whileSem b S)) s)
--   ≡⟨ {!!} ⟩
--     S⟦ S ⟧ s >>= S⟦ while b run S ⟧_
--   ≡⟨ refl  ⟩
--     S⟦ (S ∶ while b run S) ⟧ s
--   ≡⟨ cong (if_then S⟦ (S ∶ while b run S) ⟧ s else _) (sym ℬ⟦b⟧s≡true)  ⟩
--     (if ℬ⟦ b ⟧ s
--     then S⟦ (S ∶ while b run S) ⟧ s
--     else S⟦ skip ⟧ s)
--   ≡⟨ refl ⟩
--     (S⟦ (if' b then (S ∶ while b run S) else skip) ⟧ s)
--   ∎

  -- cong _ (lem-2-5-raw b S (extract {!!}))

lem-2-5 : (b : Bexp) (S : While)
        → (while b run S) ≡ds (if' b then (S ∶ while b run S) else skip)
lem-2-5 b S =
  record { S₁⇒S₂ = while-to-unfold
         ; S₂⇒S₁ = unfold-to-while
         }
  where
    lhs = while b run S
    rhs = if' b then (S ∶ while b run S) else skip
    while-to-unfold : (s s'' : State) → ⟨ lhs , s ⟩ ↠ s'' → ⟨ rhs , s ⟩ ↠ s''
    while-to-unfold s s'' (semProof S⇓ S→s'')
      with inspect (ℬ⟦ b ⟧ s)
    ...| (false , ingraph ℬ⟦b⟧s≡false)
      rewrite ℬ⟦b⟧s≡false
      rewrite (sym (cong (λ z → if z then (bind (S⟦ S ⟧ s) (λ s₁ → if ℬ⟦ b ⟧ s₁ then later (whileThunk S (Cofix.aux Domain (whileSem b S)) s₁) else now s₁)) else (now s)) ℬ⟦b⟧s≡false)) =

        semProof S⇓ S→s''
    ...| (true , ingraph ℬ⟦b⟧s≡true ) =
         let (s' , STuple , WhileTuple) = [whileᵗᵗ]-destr (semProof S⇓ S→s'')  ℬ⟦b⟧s≡true
         in [ifᵗᵗ] ℬ⟦b⟧s≡true ([comp] STuple WhileTuple)

    unfold-to-while : (s s'' : State) → ⟨ rhs , s ⟩ ↠ s'' → ⟨ lhs , s ⟩ ↠ s''
    unfold-to-while s s'' (semProof S⇓ S→s'')
      with inspect (ℬ⟦ b ⟧ s)
    ...| (false , ingraph ℬ⟦b⟧s≡false)
         rewrite ℬ⟦b⟧s≡false
         rewrite trans (sym S→s'') (extract-now S⇓)  =
           [whileᶠᶠ] ℬ⟦b⟧s≡false
    ...| (true , ingraph ℬ⟦b⟧s≡true)
         rewrite ℬ⟦b⟧s≡true =
         let (s' , bodyTuple , whileTuple ) = [comp]-destr {S₁ = S}
                                                            (semProof S⇓ S→s'')
         in [whileᵗᵗ] ℬ⟦b⟧s≡true
                       bodyTuple
                       whileTuple


ex-2-6 : {S₁ S₂ S₃ : While} → (S₁ ∶ S₂ ∶ S₃) ≡ds ((S₁ ∶ S₂) ∶ S₃)
ex-2-6 {S₁} {S₂} {S₃} =
  ns-eq (λ s s''' ⟨S₁∶S₂∶S₃,s⟩↠s''' →
           let (s' , ⟨S₁,s⟩↠s' , ⟨S₂∶S₃,s'⟩↠s''') = [comp]-destr ⟨S₁∶S₂∶S₃,s⟩↠s'''
               (s'' , ⟨S₂,s'⟩↠s'' , ⟨S₃,s''⟩↠s''') = [comp]-destr ⟨S₂∶S₃,s'⟩↠s'''
           in [comp] ([comp] ⟨S₁,s⟩↠s' ⟨S₂,s'⟩↠s'') ⟨S₃,s''⟩↠s'''
        )
        (λ s s''' ⟨[S₁∶S₂]∶S₃,s⟩↠s''' →
           let (s' , ⟨S₁∶S₂,s⟩↠s'' , ⟨S₃,s''⟩↠s''') = [comp]-destr ⟨[S₁∶S₂]∶S₃,s⟩↠s'''
               (s'' , ⟨S₁,s⟩↠s' , ⟨S₂,s'⟩↠s'') = [comp]-destr ⟨S₁∶S₂,s⟩↠s''
           in [comp] ⟨S₁,s⟩↠s' ([comp] ⟨S₂,s'⟩↠s'' ⟨S₃,s''⟩↠s''')
        )

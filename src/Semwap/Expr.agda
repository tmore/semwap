{-
Author: Tomas Möre 2020
Email: tomas.o.more@gmail.com

Definitions and proofs roughly as given in Chapter 1

-}
module Semwap.Expr where

open import Prelude

open import Prelude.Extras





-- Variables are natural
Var : Set
Var = Nat

VarSet : Set
VarSet = List Var

-- Values are either not defiened or some
Val : Set
Val = Int

State : Set
State = List (Var × Val)

-- For definition only, we say that the empty state only zeroes
ε : State
ε = []

-- Essensialy an insertion sort on lists
state-set : State → Var → Val → State
state-set [] var val  = singleton (var , val)
state-set (t@(var' , _) ∷ ss) var val =
  case (compare var' var) of
    λ { (equal eq) → (var , val) ∷ ss
      ; (less lt) →  (t ∷ state-set ss var val)
      ; (greater gt) → (var , val) ∷ t ∷ ss
      }

lookupDefault : State → Var → Val → Val
lookupDefault [] _ def = def
lookupDefault ((var' , val) ∷ ss) var def =
  if var' ==? var
  then val
  else lookupDefault ss var def

module _ where
  open import Tactic.Cong
  lookup[x]∘set[x]≡v :
    (s : State) (x : Var) (v : Val)
    → lookupDefault (state-set s x v) x (pos 0) ≡ v
  lookup[x]∘set[x]≡v [] x v
    with x == x
  ...| yes _ =  refl
  ...| no ¬eq = ⊥-elim (¬eq refl)
  lookup[x]∘set[x]≡v ((x' , v') ∷ ss) x v
    with compare x' x
  ...| (equal eq) rewrite (==?-reflexive x) = refl
  ...| less lt rewrite (≢⇒==? (<⇒≢ {A = Nat} lt))
               rewrite lookup[x]∘set[x]≡v ss x v =
            refl
  ...| greater gt rewrite (==?-reflexive x) = refl

  lookup[y]∘set[x]≡lookup[y] :
    (s : State) (y : Var) (x : Var) (v : Val)
    → (¬ (x ≡ y))
    → lookupDefault (state-set s x v) y (pos 0) ≡ lookupDefault s y (pos 0)
  lookup[y]∘set[x]≡lookup[y] [] y x v ¬eq
    rewrite (≢⇒==? ¬eq) = refl
  lookup[y]∘set[x]≡lookup[y] (t@(x' , v') ∷ ss) y x v ¬eq
    rewrite (≢⇒==? ¬eq)
    with compare x' x
  ...| equal eq rewrite eq
                rewrite (≢⇒==? ¬eq) =
       refl
  ...| greater gt rewrite (≢⇒==? (¬sym (<⇒≢ {A = Nat} gt)))
                  rewrite (≢⇒==? ¬eq) =
       sub-cases
       where
         sub-cases : _ ≡ _
         sub-cases with x' ==? y
         ...| true = refl
         ...| false = refl
  ...| less lt rewrite (≢⇒==? (<⇒≢ {A = Nat} lt))
               rewrite lookup[y]∘set[x]≡lookup[y] ss y x v ¬eq =
       sub-cases
       where
         sub-cases : _ ≡ _
         sub-cases with x' ==? y
         ...| true = refl
         ...| false = refl



       -- cong (if_then _ else _) (¬a≡b⇒a==?b≡false (<⇒≢ lt))

record TotalIndexedContainer {a b} (A : Set a) (B : Set b) : Set (a ⊔ b) where
  field get : A → Var → B

open TotalIndexedContainer {{...}} public


record Substitutable {a b} (A : Set a) (B : Set b) : Set (a ⊔ b) where
   field _[_↦_] : A → Var → B → A

open Substitutable {{...}} public



instance
   SubstitutableStateInt : Substitutable State Int
   _[_↦_] {{SubstitutableStateInt}} state var val = state-set state var val

   TotalIndexedContainerStateVal : TotalIndexedContainer State Int
   get {{TotalIndexedContainerStateVal}} state var = lookupDefault state var 0

{-# DISPLAY state-set s x v = s [ x ↦ v ] #-}
{-# DISPLAY lookupDefault s z (pos 0) = get s z #-}

instance
  SubstitutableStatePred : ∀ {ℓ} → Substitutable (State → Set ℓ) (State → Int)
  _[_↦_] {{SubstitutableStatePred}} P var val = λ state → P (state-set state var (val state))


data Aexp : Set where
   𝕟 : Nat → Aexp
   𝕧 : Var → Aexp
   _+'_ : Aexp → Aexp → Aexp
   _*'_ : Aexp → Aexp → Aexp
   _-'_ : Aexp → Aexp → Aexp

instance
  NumberAexp : Number Aexp
  Number.Constraint NumberAexp _ = ⊤
  Number.fromNat NumberAexp n = 𝕟 n

  SemiringAexp : Semiring Aexp
  zro {{SemiringAexp}} = 0
  one {{SemiringAexp}} = 1
  _+_ {{SemiringAexp}} = _+'_
  _*_ {{SemiringAexp}} = _*'_

  SubtractiveAexp : Subtractive Aexp
  _-_    {{SubtractiveAexp}}   = _-'_
  negate {{SubtractiveAexp}} n = 0 - n


-- -_ : Aexp → Aexp
-- - a = 𝕟 0 -' a

-- As defined in table 1.8
𝓐⟦_⟧_ : Aexp → State → Val
𝓐⟦ 𝕟 n ⟧ _ = fromNat n
𝓐⟦ 𝕧 v ⟧ s = get s v
𝓐⟦ a₁ +' a₂ ⟧ s = (𝓐⟦ a₁ ⟧ s) +  (𝓐⟦ a₂ ⟧ s)
𝓐⟦ a₁ *' a₂ ⟧ s = (𝓐⟦ a₁ ⟧ s) *  (𝓐⟦ a₂ ⟧ s)
𝓐⟦ a₁ -' a₂ ⟧ s = (𝓐⟦ a₁ ⟧ s) -  (𝓐⟦ a₂ ⟧ s)



-- -- Ex 1.8 Proven by agda


data Bexp : Set where
  true' : Bexp
  false' : Bexp
  _=='_ : Aexp → Aexp → Bexp
  _<='_ : Aexp → Aexp → Bexp
  !' : Bexp → Bexp
  _&&'_ : Bexp → Bexp → Bexp


ℬ⟦_⟧_ : Bexp → State → Bool
ℬ⟦ true' ⟧ _ = true
ℬ⟦ false' ⟧ _ = false
ℬ⟦ (a₁ ==' a₂) ⟧ s = ((𝓐⟦ a₁ ⟧ s) ==? (𝓐⟦ a₂ ⟧ s))
ℬ⟦ a₁ <=' a₂ ⟧ s = (𝓐⟦ a₁ ⟧ s) ≤? (𝓐⟦ a₂ ⟧ s)
ℬ⟦ !' b ⟧ s = not (ℬ⟦ b ⟧ s)
ℬ⟦ b₁ &&' b₂ ⟧ s = (ℬ⟦ b₁ ⟧ s) && (ℬ⟦ b₂ ⟧ s)


record FV {a} (A : Set a) : Set a where
  field fv : A → List Var

open FV {{...}} public

aexp-fv : Aexp → List Var
aexp-fv (𝕟 n) = []
aexp-fv (𝕧 v) = [ v ]
aexp-fv (a₁ +' a₂) = (aexp-fv a₁) ++ (aexp-fv a₂)
aexp-fv (a₁ *' a₂) = (aexp-fv a₁) ++ (aexp-fv a₂)
aexp-fv (a₁ -' a₂) = (aexp-fv a₁) ++ (aexp-fv a₂)

bexp-fv : Bexp → VarSet
bexp-fv true' = empty
bexp-fv false' = empty
bexp-fv (a₁ ==' a₂) = aexp-fv a₁ ++ aexp-fv a₂
bexp-fv (a₁ <=' a₂) = aexp-fv a₁ ++ aexp-fv a₂
bexp-fv (!' b) = bexp-fv b
bexp-fv (a₁ &&' a₂) = bexp-fv a₁ ++ bexp-fv a₂

instance
  FVAexp : FV Aexp
  fv {{FVAexp}} aexp = aexp-fv aexp

  FVBexp : FV Bexp
  fv {{FVBexp}} bexp = bexp-fv bexp

module Properies where
  open import Tactic.Cong
  open import Tactic.Reflection.Reright

  open import Prelude.Extras.Nat
  open import Prelude.Extras.List
  open import Prelude.Extras.Bool

  x∈fv[𝕧x] : (x : Var) → elem x (fv (𝕧 x)) ≡ true
  x∈fv[𝕧x] x with x == x
  ...| no ¬refl = ⊥-elim (¬refl refl)
  ...| yes refl = refl

  -- This works for all _*'_, _-'_ and _+'_. since fv applied to them reduces to
  -- _++_
  -- x∈fv[a₁]⇒x∈fv[a₁]++fv[a₂] :
  --   ∀ {a} {A : Set a} {{FVA : FV A}}
  --   (x : Var) (a₁ : A) → (a₂ : A) → elem x (fv a₁) ≡ true
  --   → elem x ((fv a₁) ++ (fv a₂)) ≡ true
  -- x∈fv[a₁]⇒x∈fv[a₁]++fv[a₂] x a₁ a₂ x∈fv[a₁] =
  --     elem x ((fv a₁) ++ (fv a₂))
  --   ≡⟨ elem-++-distr x (fv a₁) _ ⟩
  --     elem x (fv a₁) || elem x (fv a₂)
  --   ≡⟨ cong (_|| elem x (fv a₂)) x∈fv[a₁] ⟩
  --     true ∎

  -- x∈fv[a₂]⇒x∈fv[a₁]++fv[a₂] :
  --   ∀ {a} {A : Set a} {{FVA : FV A}}
  --   (x : Var) (a₁ : A) → (a₂ : A) → elem x (fv a₂) ≡ true
  --   → elem x ((fv a₁) ++ (fv a₂)) ≡ true
  -- x∈fv[a₂]⇒x∈fv[a₁]++fv[a₂] x a₁ a₂ x∈fv[a₂] =
  --          (elem-++-distr x (fv a₁) _)
  --      ⟨≡⟩ cong (elem x (fv a₁) ||_) x∈fv[a₂]
  --      ⟨≡⟩ x||true

  lem-1-12 :
    (s s' : State) → (a : Aexp)
    → ((x : Var) → elem x (fv a) ≡ true → get s x ≡ get s' x)
    → 𝓐⟦ a ⟧ s ≡ 𝓐⟦ a ⟧ s'
  lem-1-12 _ _ (𝕟 _) _ = refl
  lem-1-12 s s' (𝕧 x) ctx-equiv = ctx-equiv x  (x∈fv[𝕧x] x)
  lem-1-12 s s' (a₁ +' a₂) ctx-equiv =
       cong₂ (_+_) (lem-1-12 s s' a₁ ctx-equivₗ)
                   (lem-1-12 s s' a₂ ctx-equivᵣ)
     where
       ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
       ctx-equivₗ x x∈fva₁ =
         ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
       ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
       ctx-equivᵣ x x∈fva₂ =
         ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))
  lem-1-12 s s' a@(a₁ *' a₂) ctx-equiv =
       cong₂ (_*_) (lem-1-12 s s' a₁ ctx-equivₗ)
                   (lem-1-12 s s' a₂ ctx-equivᵣ)
     where
       ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
       ctx-equivₗ x x∈fva₁ =
         ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
       ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
       ctx-equivᵣ x x∈fva₂ =
         ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))
  lem-1-12 s s' a@(a₁ -' a₂) ctx-equiv =
       cong₂ (_-_) (lem-1-12 s s' a₁ ctx-equivₗ)
                   (lem-1-12 s s' a₂ ctx-equivᵣ)
     where
       ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
       ctx-equivₗ x x∈fva₁ =
         ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
       ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
       ctx-equivᵣ x x∈fva₂ =
         ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))


  lem-1-13 :
    (s : State) → (s' : State) → (b : Bexp)
    → ((x : Var) → elem x (fv b) ≡ true → get s x ≡ get s' x)
    → ℬ⟦ b ⟧ s ≡ ℬ⟦ b ⟧ s'
  lem-1-13 _ _ true' _ = refl
  lem-1-13 _ _ false' _ = refl
  lem-1-13 s s' (a₁ ==' a₂) ctx-equiv =
    cong₂ (_==?_) (lem-1-12 s s' a₁ ctx-equivₗ)
                   (lem-1-12 s s' a₂ ctx-equivᵣ)
     where
       ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
       ctx-equivₗ x x∈fva₁ =
         ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
       ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
       ctx-equivᵣ x x∈fva₂ =
         ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))
  lem-1-13 s s' (a₁ <=' a₂) ctx-equiv =
    cong₂ (_≤?_) (lem-1-12 s s' a₁ ctx-equivₗ)
                  (lem-1-12 s s' a₂ ctx-equivᵣ)
     where
       ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
       ctx-equivₗ x x∈fva₁ =
         ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
       ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
       ctx-equivᵣ x x∈fva₂ =
         ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))
  lem-1-13 s s' (!' b) ctx-equiv =
    cong not (lem-1-13 s s' b ctx-equiv)
  lem-1-13 s s' (b₁ &&' b₂) ctx-equiv =
    cong₂ (_&&_) (lem-1-13 s s' b₁ ctx-equivₗ)
                  (lem-1-13 s s' b₂ ctx-equivᵣ)
     where
       ctx-equivₗ : (x : Var) → (elem x (fv b₁) ≡ true) → get s x ≡ get s' x
       ctx-equivₗ x x∈fvb₁ =
         ctx-equiv x ((elem-++-left x (fv b₁) (fv b₂) x∈fvb₁))
       ctx-equivᵣ : (x : Var) → (elem x (fv b₂) ≡ true) → get s x ≡ get s' x
       ctx-equivᵣ x x∈fvb₂ =
         ctx-equiv x ((elem-++-right x (fv b₁) (fv b₂) x∈fvb₂))




aexp-subst : Aexp → Var → Aexp → Aexp
aexp-subst e@(𝕟 _) _ _ = e
aexp-subst e@(𝕧 v) v' exp =
  if v ==? v'
  then exp
  else e
aexp-subst (a₁ +' a₂) v' exp =
  aexp-subst a₁ v' exp +' aexp-subst a₂ v' exp
aexp-subst (a₁ *' a₂) v' exp =
  aexp-subst a₁ v' exp *' aexp-subst a₂ v' exp
aexp-subst (a₁ -' a₂) v' exp =
  aexp-subst a₁ v' exp -' aexp-subst a₂ v' exp

instance
  SubstitutableAexpNat : Substitutable Aexp Aexp
  _[_↦_] {{SubstitutableAexpNat}} = aexp-subst

bexp-subst : Bexp → Var → Aexp → Bexp
bexp-subst true' _ _ = true'
bexp-subst false' _ _ = false'
bexp-subst (a₁ ==' a₂) v exp =
  (a₁ [ v ↦ exp ]) ==' (a₂ [ v ↦ exp ])
bexp-subst (a₁ <=' a₂) v exp =
  (a₁ [ v ↦ exp ]) <=' (a₂ [ v ↦ exp ])
bexp-subst (!' b) v exp =
  !' (bexp-subst b v exp)
bexp-subst (b₁ &&' b₂) v exp =
  bexp-subst b₁ v exp &&' bexp-subst b₂ v exp

instance
  SubstitutableBexpNat : Substitutable Bexp Aexp
  _[_↦_] {{SubstitutableBexpNat}} = bexp-subst




module _ where
  open import Tactic.Cong
  lem-1-14 :
    (a : Aexp) → (y : Var) → (a₀ : Aexp) → (s : State)
    → 𝓐⟦ (a [ y ↦ a₀ ]) ⟧ s ≡ 𝓐⟦ a ⟧ (s [ y ↦ 𝓐⟦ a₀ ⟧ s ])
  lem-1-14 (𝕟 n) _ _ _ = refl
  lem-1-14 a@(𝕧 v) y a₀ s
    with v == y
  ...| yes v≡y rewrite v≡y = sym (lookup[x]∘set[x]≡v s y (𝓐⟦ a₀ ⟧ s))
  ...| no v≢y = sym (lookup[y]∘set[x]≡lookup[y] s v y (𝓐⟦ a₀ ⟧ s) (¬sym v≢y))
  lem-1-14 (a₁ +' a₂) y a₀ s =
    cong₂ (_+_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
  lem-1-14 (a₁ *' a₂) y a₀ s =
    cong₂ (_*_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
  lem-1-14 (a₁ -' a₂) y a₀ s =
    cong₂ (_-_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)



  lem-1-15 :
    (b : Bexp) → (y : Var) → (a₀ : Aexp) → (s : State)
    → ℬ⟦ (b [ y ↦ a₀ ]) ⟧ s ≡ ℬ⟦ b ⟧ (s [ y ↦ 𝓐⟦ a₀ ⟧ s ])
  lem-1-15 true' _ _ _ = refl
  lem-1-15 false' _ _ _ = refl
  lem-1-15 (a₁ ==' a₂) y a₀ s =
    cong₂ (_==?_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
  lem-1-15 (a₁ <=' a₂) y a₀ s =
    cong₂ (_≤?_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
  lem-1-15 (!' b) y a₀ s =
    cong not (lem-1-15 b y a₀ s)
  lem-1-15 (b₁ &&' b₂) y a₀ s =
    cong₂ (_&&_) (lem-1-15 b₁ y a₀ s) (lem-1-15 b₂ y a₀ s)

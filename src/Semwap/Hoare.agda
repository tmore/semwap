module Semwap.Hoare where



open import Prelude
open import Prelude.Extras

open import Prelude.Int.Properties
open import Semwap.Expr
open import Semwap.While

data ⟦_⟧_⟦_⟧ {ℓ} : (State → Set ℓ) → While → (State → Set ℓ) → Set (lsuc ℓ) where
  [skipₐₓ] : {P : State → Set ℓ} → ⟦ P ⟧ skip ⟦ P ⟧
  [assₐₓ] : {var : Var} {val : Aexp} {P : State → Set ℓ}
            → ⟦ (P [ var ↦ (𝓐⟦ val ⟧_) ]) ⟧ (var := val) ⟦ P ⟧
  [compₐₓ] : {S₁ S₂ : While} {P Q R : State → Set ℓ}
             → ⟦ P ⟧ S₁ ⟦ Q ⟧ → ⟦ Q ⟧ S₂ ⟦ R ⟧
             → ⟦ P ⟧ (S₁ ∶ S₂) ⟦ R ⟧
  [ifₐₓ] : {S₁ S₂ : While} {b : Bexp} {P Q : State → Set ℓ}
           → ⟦ (λ s → ℬ⟦ b ⟧ s ≡ true × P s) ⟧ S₁ ⟦ Q ⟧
           → ⟦ (λ s → ℬ⟦ b ⟧ s ≡ false × P s) ⟧ S₂ ⟦ Q ⟧
           → ⟦ P ⟧ (if' b then S₁ else S₂) ⟦ Q ⟧
  [whileₐₓ] : {S : While} {b : Bexp} {P : State → Set ℓ}
           → ⟦ (λ s → ℬ⟦ b ⟧ s ≡ true × P s) ⟧ S ⟦ P ⟧
           → ⟦ P ⟧ (while b run S) ⟦ (λ s → ℬ⟦ b ⟧ s ≡ false × P s) ⟧
  [consₐₓ] : {S : While} {P P' Q' Q : State → Set ℓ}
           → ((s : State) → P s → P' s) → ((s : State) → Q' s → Q s)
           → ⟦ P' ⟧ S ⟦ Q' ⟧
           → ⟦ P ⟧ S ⟦ Q ⟧

module _ where
  ex-9-8 : ⟦ const Unit ⟧ skip ⟦ const Unit ⟧
  ex-9-8 = [skipₐₓ]

  ex-9-8' : ⟦ const Unit ⟧ while true' run skip ⟦ const Unit ⟧
  ex-9-8' = [consₐₓ] (λ s p → p) (λ { s (_ , p) → p })
                    ([whileₐₓ] ([consₐₓ] (λ { s p → p }) (λ { s (_ , p) → p })
                                         [skipₐₓ]))

  fac : Nat → Nat
  fac 0 = 1
  fac (suc n ) = (suc n) * fac n


  fac-extend : (n : Nat) → (suc n) * fac n ≡ fac (suc n)
  fac-extend 0 = refl
  fac-extend (suc n) = refl


  -- Non total definition of face for integers
  faci : Int → Int
  faci (pos n) = fromNat $ fac n
  faci (negsuc n) = -1

  x y : Var
  x = 0
  y = 1

  open import Prelude.Int.Properties
  ex-9-9 : {n : Nat}
           → ⟦ (λ s → get s x ≡ fromNat n) ⟧
             (y := 1 ∶ while (!' (𝕧 x ==' 1))
                      run (y := (𝕧 y * 𝕧 x) ∶ x := (𝕧 x - 1)))
             ⟦ (λ s → get s y ≡ fromNat (fac n)) ⟧
  ex-9-9 {n} =
    [compₐₓ] ([consₐₓ] (λ s p → trans (lookup[y]∘set[x]≡lookup[y]
                                        s x y 1 (λ ())) p
                                     , lookup[x]∘set[x]≡v s y 1)
                      (λ s p → p)
                      ([assₐₓ] {P = (λ s → (get s x ≡ fromNat n) × (get s y ≡ 1))})
                      )
            ([consₐₓ] PRE⇒INV
                     INV⇒POST
                     ([whileₐₓ] {P = INV}
                                ([consₐₓ]
                                  WHILE-PRE
                                  (λ s p → p)
                                  ([compₐₓ]
                                  [assₐₓ]
                                  ([assₐₓ] {P = INV})
                                )))
                      )
         where
           open import Prelude.Nat.Properties
           INV : State → Set _
           INV s =
             ((get s x > 0)
             → (((get s y) * faci (get s x)) ≡ fromNat (fac n)))
             × fromNat n ≥ (get s x)

           PRE⇒INV : (s : State) → get s x ≡ pos n × get s y ≡ pos 1 → INV s
           PRE⇒INV s (x≡n , y≡1)
             rewrite x≡n
             rewrite y≡1
             rewrite add-zero-r (fac n)
             = (λ x>0 → refl) , (diff zero refl)
           INV⇒POST : (s : State)
                     → (not (get s x ==? pos 1)) ≡ false × INV s → get s y ≡ pos (fac n)
           INV⇒POST s (≡false , (inv-f , n≥x ))
             with a==?b⇒a≡b $ not[x]≡false⇒x≡true ≡false
           ...| x≡1 rewrite x≡1
             with inv-f (diff zero refl)
           ...| fac-eq rewrite int-mul-one-r (get s 1) = fac-eq

           WHILE-PRE :  (s : State) → (not (get s x ==? pos 1)) ≡ true × INV s →
                                    INV ((s [ y ↦ get s y * get s x ]) [ x ↦ get (s [ y ↦ get s y * get s x ]) x + negsuc 0 ])
           WHILE-PRE s (≡true , (inv-f , n≥x ))
             with ==?≡false⇒≢ $ not[x]≡true⇒x≡false ≡true
           ...| foo
             rewrite lookup[y]∘set[x]≡lookup[y] s x y (get s y * get s x) (λ ())
             rewrite lookup[x]∘set[x]≡v (s [ 1 ↦ get s 1 * get s 0 ]) x ((get s 0) - 1)
             rewrite lookup[y]∘set[x]≡lookup[y] (s [ 1 ↦ get s 1 * get s 0 ]) y x (get s 0 + negsuc 0) (λ ())
             rewrite lookup[x]∘set[x]≡v s y (get s 1 * get s 0)
               = inv-p , (lt-predIntₗ _ _ n≥x)
               where
                 inv-p : 0 < (get s x - 1)  → get s y * get s x * faci (get s x - 1) ≡ pos (fac n)
                 inv-p pred[x]>0 with int-suc (get s x) (lt-predIntᵣ _ _ pred[x]>0)
                 ...| m , get[s,x]≡m
                   rewrite get[s,x]≡m
                   = get s y * pos (suc m) * faci (suc m -NZ 1)
                   ≡⟨ cong (λ z → get s y * pos (suc m) * faci z) (-NZ-spec (suc m) 1) ⟩
                     get s y * pos (suc m) * faci (diffNat (suc m) 1)
                   ≡⟨ refl ⟩
                      (get s y * pos (suc m)) * pos (fac m)
                   ≡⟨ sym (mulInt-assoc (get s y) (pos (suc m)) (pos (fac m))) ⟩
                      get s y * pos (fac (suc m))
                   ≡⟨ inv-f 0<sucm ⟩
                     pos (fac n)
                   ∎
                   where
                     0<sucm : 0 < pos (suc m)
                     0<sucm = lt-predIntᵣ 0 (pos (suc m)) pred[x]>0


module Properties where
  open import Semwap.NatSem

  -- Maybe both proof of sound and complete?
  hoare-sound :
    ∀ {ℓ} → (S : While) → {s s' : State}
    → {P Q : State → Set ℓ}
    → ⟦ P ⟧ S ⟦ Q ⟧ → ⟨ S , s ⟩ ⇾ s'
    → P s → Q s'
  hoare-sound (x₁ := x₂)  [assₐₓ] [assₙₛ] Ps =
    Ps
  hoare-sound (x₁ := x₂) {s} {_} {P} {Q} ([consₐₓ] {P' = P'} {Q' = Q'} P→P' Q'→Q hoare) [assₙₛ] Ps =
    Q'→Q (s [ x₁ ↦ 𝓐⟦ x₂ ⟧ s ]) (hoare-sound (x₁ := x₂) hoare [assₙₛ] (P→P' s Ps))
  hoare-sound skip [skipₐₓ] [skipₙₛ] Ps = Ps
  hoare-sound skip {s} ([consₐₓ] {P' = P'} {Q' = Q'} P→P' Q'→Q hoare) [skipₙₛ] Ps =
    Q'→Q s (hoare-sound skip hoare [skipₙₛ] (P→P' s Ps))
  hoare-sound (S₁ ∶ S₂) ([compₐₓ] {Q = R} hoareₗ hoareᵣ) ([compₙₛ] {s' = s'} nsₗ nsᵣ) Ps =
    hoare-sound S₂ hoareᵣ nsᵣ (hoare-sound S₁ hoareₗ nsₗ Ps)
  hoare-sound (S₁ ∶ S₂) {s} {s'} ([consₐₓ] P→P' Q'→Q hoare) ([compₙₛ] ns ns₁) Ps =
    Q'→Q s' (hoare-sound (S₁ ∶ S₂) hoare ([compₙₛ] ns ns₁) (P→P' s Ps))
  hoare-sound (if' b then S₁ else S₂) ([ifₐₓ] hoareₗ hoareᵣ) ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ns) Ps =
    hoare-sound S₁ hoareₗ ns (ℬ⟦b⟧≡true , Ps)
  hoare-sound (if' b then S₁ else S₂) ([ifₐₓ] hoareₗ hoareᵣ) ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ns) Ps =
    hoare-sound S₂ hoareᵣ ns (ℬ⟦b⟧≡false , Ps)
  hoare-sound (if' b then S₁ else S₂) {s} {s'} ([consₐₓ] P→P' Q'→Q hoare) ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ns) Ps =
    Q'→Q s' (hoare-sound (if' b then S₁ else S₂) hoare ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ns) (P→P' s Ps))
  hoare-sound (if' b then S₁ else S₂) {s} {s'} ([consₐₓ] P→P' Q'→Q hoare) ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ns) Ps =
    Q'→Q s' (hoare-sound (if' b then S₁ else S₂) hoare ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ns) (P→P' s Ps))
  hoare-sound (while b run S) ([whileₐₓ] hoare) ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true ns ns₁) Ps =
    hoare-sound (while b run S) ([whileₐₓ] hoare) ns₁
                (hoare-sound S hoare ns (ℬ⟦b⟧≡true , Ps))
  hoare-sound (while b run S) ([whileₐₓ] hoare) ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false) Ps =
    (ℬ⟦b⟧≡false , Ps)
  hoare-sound (while b run S) {s} {s'} ([consₐₓ] P→P' Q'→Q hoare) ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true ns ns₁) Ps =
    Q'→Q s' (hoare-sound (while b run S) hoare ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true ns ns₁) (P→P' s Ps))
  hoare-sound (while b run S) {s} {s'} ([consₐₓ] P→P' Q'→Q hoare) ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false) Ps =
    Q'→Q s (hoare-sound (while b run S) hoare ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false) (P→P' s Ps))

module Semwap.SemEq where

open import Prelude
open import Prelude.Nat.Properties

open import Prelude.Extras
open import Prelude.Extras.Delay

open import Semwap.Expr
open import Semwap.While

open import Semwap.NatSem
open import Semwap.SOS
import Semwap.DenotationalSem as DS
open import Control.WellFounded

NS⇒SOS : ∀ {S s s'} → (⟨ S , s ⟩ ⇾ s') → ⟨ S , s ⟩ ⇒⁺ left s'
NS⇒SOS {x := x₁} [assₙₛ] = finally [assₛₒₛ]
NS⇒SOS {skip} [skipₙₛ] = finally [skipₛₒₛ]
NS⇒SOS {if' x then S else S₁} ([ifₙₛᵗᵗ] x₁ foo) =
  [ifₛₒₛᵗᵗ] x₁ andThen (NS⇒SOS foo)
NS⇒SOS {if' x then S else S₁} ([ifₙₛᶠᶠ] x₁ foo) =
  [ifₛₒₛᶠᶠ] x₁ andThen (NS⇒SOS foo)
NS⇒SOS {while x run S} {s}  ([whileₙₛᵗᵗ] {s' = s'} {s''} x₁ body-NS loop-NS) =
  [whileₛₒₛ]
  andThen [ifₛₒₛᵗᵗ] x₁
  andThen (Properties.lem-2-19-inv (NS⇒SOS body-NS) (NS⇒SOS loop-NS))
NS⇒SOS {while x run S} ([whileₙₛᶠᶠ] ¬ℬ⟦x⟧) =
  [whileₛₒₛ]
  andThen [ifₛₒₛᶠᶠ] ¬ℬ⟦x⟧
  andThen finally [skipₛₒₛ]
NS⇒SOS {S ∶ S₁} ([compₙₛ] S-NS S₁-NS) =
  (Properties.lem-2-19-inv (NS⇒SOS S-NS) (NS⇒SOS S₁-NS))



-- proof that SOS can be translated to NS but with a proof of termination included
SOS⇒NS-wf : ∀ {S s s'}
       → (nsProof : ⟨ S , s ⟩ ⇒⁺ left s')
       → (@0 _ : Acc _<_ (evalLength nsProof))
       → (⟨ S , s ⟩ ⇾ s')
SOS⇒NS-wf {x := x₁} (finally [assₛₒₛ]) _ = [assₙₛ]
SOS⇒NS-wf {skip} (finally [skipₛₒₛ]) _ = [skipₙₛ]
SOS⇒NS-wf {if' x then S else S₁} ([ifₛₒₛᵗᵗ] x₁ andThen tail) (acc getLt) =
  [ifₙₛᵗᵗ] x₁ (SOS⇒NS-wf {S}
                      tail
                      (getLt (evalLength tail)
                             (diff 0 refl)))
SOS⇒NS-wf {if' x then S else S₁} ([ifₛₒₛᶠᶠ] x₁ andThen tail) (acc getLt) =
  [ifₙₛᶠᶠ] x₁ (SOS⇒NS-wf {S₁}
                      tail
                      (getLt (evalLength tail)
                             (diff 0 refl)))
SOS⇒NS-wf {while x run S} ([whileₛₒₛ] andThen tail) (acc getLt) =
  _≡ₙₛ_.S₂⇒S₁ (lem-2-5 x S)
              _ _
              (SOS⇒NS-wf tail
                      (getLt (evalLength tail)
                             (diff 0 refl)))
SOS⇒NS-wf {S₁ ∶ S₂} {s} {s''} c (acc getLt) =
  let (s' , S₁ₛₒₛ , S₂ₛₒₛ) = Properties.lem-2-19 c
      lhs = (fst (snd (Properties.lem-2-19 c)))
      rhs = (snd (snd (Properties.lem-2-19 c)))
      (n , a) = Properties.evalLength-suc lhs
      (m , b) = Properties.evalLength-suc rhs
  in [compₙₛ] (SOS⇒NS-wf {S₁} S₁ₛₒₛ
                     (getLt (evalLength S₁ₛₒₛ)
                            (diff ((evalLength S₂ₛₒₛ) - 1)
                                  (Properties.lem-2-19-nat c
                                  ⟨≡⟩ add-commute (evalLength lhs) (evalLength rhs)
                                  ⟨≡⟩ cong (_+ (evalLength lhs))
                                           (suc-neg (evalLength rhs) m b))
                            )))
            (SOS⇒NS-wf {S₂} S₂ₛₒₛ
                         (getLt ((evalLength S₂ₛₒₛ))
                                (diff ((evalLength S₁ₛₒₛ) - 1)
                                  (Properties.lem-2-19-nat c
                                  ⟨≡⟩  cong (_+ (evalLength rhs))
                                           (suc-neg (evalLength lhs) n a))
                                )))

SOS⇒NS : ∀ {S s s'}
       → ⟨ S , s ⟩ ⇒⁺ left s'
       → ⟨ S , s ⟩ ⇾ s'
SOS⇒NS nsProof =
  SOS⇒NS-wf nsProof (wfNat (evalLength nsProof))



module _ where
  open import Semwap.AM
  module AMProp = Semwap.AM.Properties
--  import Semwap.AM.Properties as AMProp
  NS⇒AM :
    { S : While} {s s' : State}
    → ⟨ S , s ⟩ ⇾ s'
    → ( CS⟦ S ⟧ , [] , s ) ⊳⁺ ( [] , [] , s' )
  NS⇒AM { v := a } [assₙₛ] =
    AMProp.prepend-terminating (AMProp.airth-behavior a) (finally [STORE])
  NS⇒AM [skipₙₛ] =
    finally [NOOP]
  NS⇒AM ([compₙₛ] ⟨S,s⟩⇾s' ⟨S,s⟩⇾s'') =
    AMProp.prepend-terminating (NS⇒AM ⟨S,s⟩⇾s') (NS⇒AM ⟨S,s⟩⇾s'')
  NS⇒AM ([ifₙₛᵗᵗ]  {S₁} {S₂} {s} {s'} { b = b } ℬ⟦b⟧≡true ⟨S₁,s⟩⇾s')
    with (AMProp.bool-behavior {[]} {s} b)
  ...| guard-p rewrite ℬ⟦b⟧≡true =
   (AMProp.prepend-terminating
     guard-p
     ([BRANCH] andThen (transport (λ z → ( z , [] , s) ⊳⁺ ([] , [] , s'))
                                  (sym $ right-ident CS⟦ S₁ ⟧)
                                  (NS⇒AM ⟨S₁,s⟩⇾s'))))
  NS⇒AM ([ifₙₛᶠᶠ] {S₁} {S₂} {s} {s'} { b = b } ℬ⟦b⟧≡false ⟨S₂,s⟩⇾s')
    with (AMProp.bool-behavior {[]} {s} b)
  ...| guard-p rewrite ℬ⟦b⟧≡false =
   (AMProp.prepend-terminating
     guard-p
     ([BRANCH] andThen (transport (λ z → ( z , [] , s) ⊳⁺ ([] , [] , s'))
                                  (sym $ right-ident CS⟦ S₂ ⟧)
                                  (NS⇒AM ⟨S₂,s⟩⇾s'))))
  NS⇒AM ([whileₙₛᵗᵗ] {b} {S} {s} {s'} {s''} ℬ⟦b⟧≡true ⟨S,s⟩⇾s' ⟨whilebdoS,s⟩⇾s'')
    with (AMProp.bool-behavior {[]} {s} b)
  ...| guard-p rewrite ℬ⟦b⟧≡true =
    [LOOP] andThen (AMProp.prepend-terminating
                       guard-p
                       ([BRANCH] andThen
                         (transport
                           (λ z → (z , [] , s) ⊳⁺
                                  ([] , [] , s''))
                           (sym $ right-ident (CS⟦ S ⟧ ++ [ LOOP CB⟦ b ⟧ CS⟦ S ⟧ ]))
                           (AMProp.prepend-terminating (NS⇒AM ⟨S,s⟩⇾s') (NS⇒AM ⟨whilebdoS,s⟩⇾s''))
                         )))
  NS⇒AM ([whileₙₛᶠᶠ] {S} {s} {b} ℬ⟦b⟧≡false)
    with (AMProp.bool-behavior {[]} {s} b)
  ...| guard-p rewrite ℬ⟦b⟧≡false =
       [LOOP] andThen (AMProp.prepend-terminating guard-p ([BRANCH] andThen (finally [NOOP])))


module _ where
  NS⇒DS : (S : While) {s s' : State} → ⟨ S , s ⟩ ⇾ s' → ⟨ S , s ⟩ DS.↠ s'
  NS⇒DS skip [skipₙₛ] = DS.[skip]
  NS⇒DS (x := a) [assₙₛ] = DS.[ass]
  NS⇒DS (S₁ ∶ S₂) ([compₙₛ] ⟨S₁,s⟩⇾s₁ ⟨S₂,s₁⟩⇾s₂)  =
    DS.[comp] (NS⇒DS S₁ ⟨S₁,s⟩⇾s₁) (NS⇒DS S₂ ⟨S₂,s₁⟩⇾s₂)
  NS⇒DS (if' b then S₁ else S₂) ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ⟨S₁,s⟩⇾s₁)  =
    DS.[ifᵗᵗ] ℬ⟦b⟧≡true (NS⇒DS S₁ ⟨S₁,s⟩⇾s₁)
  NS⇒DS (if' b then S₁ else S₂) ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ⟨S₂,s⟩⇾s₁)  =
    DS.[ifᶠᶠ] ℬ⟦b⟧≡false (NS⇒DS S₂ ⟨S₂,s⟩⇾s₁)
  NS⇒DS stm@(while b run S) ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true ⟨S,s⟩⇾s₁ ⟨while,s₁⟩⇾s₂)  =
    DS.[whileᵗᵗ] ℬ⟦b⟧≡true
                 (NS⇒DS S ⟨S,s⟩⇾s₁)
                 (NS⇒DS stm ⟨while,s₁⟩⇾s₂)
  NS⇒DS stm@(while b run S) ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false )  =
    DS.[whileᶠᶠ] ℬ⟦b⟧≡false


  open import Codata.Delay
  -- Shows that the termination of denotational semantics implies a natural
  -- semantics proof. This version does induction over the size of the sum of
  -- proof and syntax tree size. The proof without it is defined below
  DS⇒NS' : (S : While) {s s' : State}
         → (ds : ⟨ S , s ⟩ DS.↠ s')
         → (@0 _ : Acc _<_ ((size ds) + (size S)))
         → ⟨ S , s ⟩ ⇾ s'
  DS⇒NS' (x := x₁) (DS.semProof {s = s} {.(s [ x ↦ 𝓐⟦ x₁ ⟧ s ])} (now .(s [ x ↦ 𝓐⟦ x₁ ⟧ s ])) refl) _ =
    [assₙₛ]
  DS⇒NS' skip (DS.semProof (now _) refl) _ = [skipₙₛ]
  DS⇒NS' (S₁ ∶ S₂) p@(DS.semProof d x) (acc getLt)
    with DS.[comp]-destr-and-size p
  ...| (s' , (⟨S₁,s⟩⇾s' , ⟨S₂,s'⟩⇾s'') ,  size-proof)
    rewrite size-proof =
    [compₙₛ] (DS⇒NS' S₁ ⟨S₁,s⟩⇾s'
                     (getLt ((size ⟨S₁,s⟩⇾s') + size S₁)
                            (diff (size ⟨S₂,s'⟩⇾s'' + size S₂) auto)))
            (DS⇒NS' S₂ ⟨S₂,s'⟩⇾s'' (getLt ((size ⟨S₂,s'⟩⇾s'') + size S₂)
                            (diff (size ⟨S₁,s⟩⇾s' + size S₁) auto)))
    where
      open import Tactic.Nat.Auto
  DS⇒NS' (if' x then S₁ else S₂) {s = s} {s' = s'} (DS.semProof d x₁) (acc getLt)
    with inspect (ℬ⟦ x ⟧ s)
  ...| (true , ingraph ℬ⟦x⟧s≡true) rewrite ℬ⟦x⟧s≡true =
       [ifₙₛᵗᵗ] ℬ⟦x⟧s≡true (DS⇒NS' S₁ (DS.semProof {S₁} {s} {s'}  d x₁)
                                   (getLt (size (DS.semProof {S₁} {s} {s'} d x₁) + size S₁)
                                          (diff (size S₂) auto)))
       where
         open import Tactic.Nat.Auto
  ...| (false , ingraph ℬ⟦x⟧s≡false) rewrite ℬ⟦x⟧s≡false =
       [ifₙₛᶠᶠ] ℬ⟦x⟧s≡false (DS⇒NS' S₂ (DS.semProof {S₂} {s} {s'}  d x₁)
                                   (getLt (size (DS.semProof {S₂} {s} {s'} d x₁) + size S₂)
                                          (diff (size S₁) auto)))
       where
         open import Tactic.Nat.Auto
  DS⇒NS' (while x run S) (DS.semProof {s = s} {s' = s''} d x₁) (acc getLt)
    with inspect (ℬ⟦ x ⟧ s)
  ...| true , ingraph ℬ⟦x⟧s≡true
    with DS.[whileᵗᵗ]-destr-and-size (DS.semProof {(while x run S)} {s} {s''} d x₁) ℬ⟦x⟧s≡true
  ...| (s' , (body-proof , loop-proof) , size-proof)
    rewrite size-proof =
    [whileₙₛᵗᵗ] ℬ⟦x⟧s≡true
              (DS⇒NS' S body-proof
                        (getLt (size body-proof + size S)
                               (diff (suc (size loop-proof)) auto)))
              (DS⇒NS' (while x run S)
                       loop-proof
                       (getLt (size loop-proof + suc (size S))
                              (diff (size body-proof) auto)
                              ))
    where
      open import Tactic.Nat.Auto
  DS⇒NS' (while x run S) (DS.semProof {(while x run S)} {s} {s''} d x₁) (acc getLt)
    | false , ingraph ℬ⟦x⟧s≡false
      rewrite ℬ⟦x⟧s≡false
      rewrite extract-now d
      rewrite sym x₁ =
      [whileₙₛᶠᶠ] ℬ⟦x⟧s≡false

  DS⇒NS : (S : While) {s s' : State}
         → (⟨ S , s ⟩ DS.↠ s')
         → ⟨ S , s ⟩ ⇾ s'
  DS⇒NS S wp = DS⇒NS' S wp (wfNat (size wp + size S))

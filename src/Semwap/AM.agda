{-
Author: Tomas Möre 2020
-}
module Semwap.AM where

open import Prelude
open import Prelude.Extras

open import Semwap.Expr
open import Semwap.While

-- Defines every instruction in table 4.1
data Inst : Set where
  PUSH : Nat → Inst
  ADD : Inst
  MULT : Inst
  SUB : Inst
  TRUE : Inst
  FALSE : Inst
  EQ : Inst
  LE : Inst
  AND : Inst
  NEG : Inst
  FETCH : Var → Inst
  STORE : Var → Inst
  NOOP : Inst
  BRANCH : List Inst → List Inst → Inst
  LOOP : List Inst → List Inst → Inst

-- Type alias
Code : Set
Code = List Inst

AMStack : Set
AMStack = List (Either Bool Val)
AMState : Set
AMState = List Inst × AMStack × State

-- Translation of arithmetic expressions to code
CA⟦_⟧ : Aexp → Code
CA⟦ 𝕟 n ⟧ = [ PUSH n ]
CA⟦ 𝕧 v ⟧ = [ FETCH v ]
CA⟦ a₁ +' a₂ ⟧ = CA⟦ a₂ ⟧ ++ CA⟦ a₁ ⟧ ++ [ ADD ]
CA⟦ a₁ *' a₂ ⟧ = CA⟦ a₂ ⟧ ++ CA⟦ a₁ ⟧ ++ [ MULT ]
CA⟦ a₁ -' a₂ ⟧ = CA⟦ a₂  ⟧ ++ CA⟦ a₁ ⟧ ++ [ SUB ]

-- Translation of boolean expressions to code
CB⟦_⟧ : Bexp → Code
CB⟦ true' ⟧ = [ TRUE ]
CB⟦ false' ⟧ = [ FALSE ]
CB⟦ a₁ ==' a₂ ⟧ = CA⟦ a₂ ⟧ ++ CA⟦ a₁ ⟧ ++ [ EQ ]
CB⟦ a₁ <=' a₂ ⟧ = CA⟦ a₂ ⟧ ++ CA⟦ a₁ ⟧ ++ [ LE ]
CB⟦ !' b ⟧ = CB⟦ b ⟧ ++ [ NEG ]
CB⟦ b₁ &&' b₂ ⟧ = CB⟦ b₂ ⟧ ++ CB⟦ b₁ ⟧ ++ [ AND ]


-- Translation of While to Code
CS⟦_⟧ : While → Code
CS⟦ v := a ⟧ = CA⟦ a ⟧ ++ [ STORE v ]
CS⟦ skip ⟧ = [ NOOP ]
CS⟦ c₁ ∶ c₂ ⟧ = CS⟦ c₁ ⟧ ++ CS⟦ c₂ ⟧
CS⟦ if' b then c₁ else c₂ ⟧ = CB⟦ b ⟧ ++ [ BRANCH CS⟦ c₁ ⟧ CS⟦ c₂ ⟧ ]
CS⟦ while b run c ⟧ = [ LOOP CB⟦ b ⟧ CS⟦ c ⟧ ]

-- Step semantics of the abstract machine as defined in table 4.1
data _⊳_ : AMState → AMState → Set where
  [PUSH] : ∀ {n c e s}
         → ((PUSH n ∷ c) , e , s) ⊳
           (c , (right (fromNat n) ∷ e) , s)
  [ADD] : ∀ {z₁ z₂ c e s}
        → ((ADD ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
          (c , ( right (z₁ + z₂) ∷ e) , s)
  [MULT] : ∀ {z₁ z₂ c e s}
         → ((MULT ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
           (c , ( right (z₁ * z₂) ∷ e) , s)
  [SUB] : ∀ {z₁ z₂ c e s}
        → ((SUB ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
          (c , ( right (z₁ - z₂) ∷ e) , s)
  [TRUE] : ∀ {c e s}
         → ((TRUE ∷ c) , e , s) ⊳
           (c , ( left true ∷ e) , s)
  [FALSE] : ∀ {c e s}
          → ((FALSE ∷ c) , e , s) ⊳
            (c , ( left false ∷ e) , s)
  [EQ] : ∀ {z₁ z₂ c e s}
       → ((EQ ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
         (c , ( left (z₁ ==? z₂) ∷ e) , s)
  [LE] : ∀ {z₁ z₂ c e s}
       → ((LE ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
         (c , ( left (z₁ ≤? z₂) ∷ e) , s)
  [AND] : ∀ {t₁ t₂ c e s}
        → ((AND ∷ c) , (left t₁ ∷ left t₂ ∷ e) , s) ⊳
          (c , ( left (t₁ && t₂) ∷ e) , s)
  [NEG] : ∀ {t c e s}
        → ((NEG ∷ c) , (left t ∷ e) , s) ⊳
          (c , (left (not t) ∷ e) , s)
  [FETCH] : ∀ {x c e s}
          → ((FETCH x ∷ c) , e , s) ⊳
            (c , (right (get s x) ∷ e) , s)
  [STORE] : ∀ {x z c e s}
          → ((STORE x ∷ c) , (right z ∷ e) , s) ⊳
            (c , e , s [ x ↦ z ])
  [NOOP] : ∀ {c e s}
         → ((NOOP ∷ c) , e , s) ⊳  (c , e , s)
  [BRANCH] : ∀ {c₁ c₂ t c e s}
           → (((BRANCH c₁ c₂) ∷ c) , (left t ∷ e) , s) ⊳
             ((if t then c₁ else c₂) ++  c , e , s)
  [LOOP] : ∀ {c₁ c₂ c e s}
           → ((LOOP c₁ c₂ ∷ c) , e , s) ⊳
             (c₁ ++ ((BRANCH (c₂ ++ [ (LOOP c₁ c₂) ] ) [ NOOP ]) ∷ c) , e , s)

-- Proof that a sequence of instructions terminates to some state
infixr 4 _andThen_
data _⊳⁺_ : AMState → AMState → Set where
  finally : ∀ {c e s c' e' s'}
          → (c , e , s) ⊳ (c' , e' , s')
          → (c , e , s) ⊳⁺ (c' , e' , s')
  _andThen_ : ∀ { c e s c' e' s' c'' e'' s''}
          → (c , e , s) ⊳ (c' , e' , s')
          → (c' , e' , s') ⊳⁺ (c'' , e'' , s'')
          → (c , e , s) ⊳⁺ (c'' , e'' , s'')

derivLength : ∀ {pre post} → (pre ⊳⁺ post) → Nat
derivLength (finally _) = 0
derivLength (_ andThen tail) = 1 + derivLength tail

instance
  Sized⊳⁺ : ∀ {pre post} → Sized (pre ⊳⁺ post)
  size {{Sized⊳⁺}} = derivLength


IsFinally : ∀ {pre post} → pre ⊳⁺ post → Set
IsFinally (finally _) = ⊤
IsFinally (_ andThen _) = ⊥

IsAndThen : ∀ {pre post} → pre ⊳⁺ post → Set
IsAndThen (finally _) = ⊥
IsAndThen (_ andThen _) = ⊤

module Examples where
  private
    x : Var
    x = 0

  ex-4-1 : (PUSH 1 ∷ FETCH x ∷ ADD ∷ STORE x ∷ [] , [] , ε [ x ↦ 3 ]) ⊳⁺
           ([] , [] , ε [ x ↦ 4 ])
  ex-4-1 = [PUSH]
           andThen [FETCH]
           andThen [ADD]
           andThen (finally [STORE])


module Properties where

  no-step-from-empty-code :
    ∀ {e s c' e' s'}
    → ([] , e , s) ⊳ (c' , e' , s')
    → ⊥
  no-step-from-empty-code ()

  no-deriv-seq-from-empty-code :
    ∀ {e s c' e' s'}
    → ([] , e , s) ⊳⁺ (c' , e' , s')
    → ⊥
  no-deriv-seq-from-empty-code (finally ())
  no-deriv-seq-from-empty-code (() andThen _)

  AM-step-deterministic :
    ∀ {c e s c' e' s' c'' e'' s''}
    → (c , e , s) ⊳ (c' , e' , s')
    → (c , e , s) ⊳ (c'' , e'' , s'')
    → (c' , e' , s') ≡ (c'' , e'' , s'')
  AM-step-deterministic [PUSH] [PUSH] = refl
  AM-step-deterministic [ADD] [ADD] = refl
  AM-step-deterministic [MULT] [MULT] = refl
  AM-step-deterministic [SUB] [SUB] = refl
  AM-step-deterministic [TRUE] [TRUE] = refl
  AM-step-deterministic [FALSE] [FALSE] = refl
  AM-step-deterministic [EQ] [EQ] = refl
  AM-step-deterministic [LE] [LE] = refl
  AM-step-deterministic [AND] [AND] = refl
  AM-step-deterministic [NEG] [NEG] = refl
  AM-step-deterministic [FETCH] [FETCH] = refl
  AM-step-deterministic [STORE] [STORE] = refl
  AM-step-deterministic [NOOP] [NOOP] = refl
  AM-step-deterministic [BRANCH] [BRANCH] = refl
  AM-step-deterministic [LOOP] [LOOP] = refl


  step-terms-tail-empty :
    ∀ {i c e s e' s'}
    → (i ∷ c , e , s) ⊳ ([] , e' , s')
    → c ≡ []
  step-terms-tail-empty {PUSH x} [PUSH] = refl
  step-terms-tail-empty {ADD} [ADD] = refl
  step-terms-tail-empty {MULT} [MULT] = refl
  step-terms-tail-empty {SUB} [SUB] = refl
  step-terms-tail-empty {TRUE} [TRUE] = refl
  step-terms-tail-empty {FALSE} [FALSE] = refl
  step-terms-tail-empty {EQ} [EQ] = refl
  step-terms-tail-empty {LE} [LE] = refl
  step-terms-tail-empty {AND} [AND] = refl
  step-terms-tail-empty {NEG} [NEG] = refl
  step-terms-tail-empty {FETCH x} [FETCH] = refl
  step-terms-tail-empty {STORE x} [STORE] = refl
  step-terms-tail-empty {NOOP} [NOOP] = refl
  step-terms-tail-empty {BRANCH [] x₁} {c} {e = left true ∷ e} {s} p₁ =
     fst $ pair-inj (AM-step-deterministic [BRANCH] p₁)
  step-terms-tail-empty {BRANCH x@(_ ∷ _) x₁} {c} {e = left true ∷ e} {s} p₁
    with fst $ pair-inj $ AM-step-deterministic [BRANCH] p₁
  ...| ()
  step-terms-tail-empty {BRANCH x []} {e = left false ∷ e} p₁ =
     fst $ pair-inj (AM-step-deterministic [BRANCH] p₁)
  step-terms-tail-empty {BRANCH x x₁@(_ ∷ _)} {e = left false ∷ e} p₁
    with fst $ pair-inj $ AM-step-deterministic [BRANCH] p₁
  ...| ()
  step-terms-tail-empty {LOOP x x₁} {c} {e} {s} p₁ =
    ⊥-elim (xs++y∷ys≢[]
              _ _ _
              (fst $ pair-inj $ AM-step-deterministic [LOOP] p₁))


  ex-4-4-single : ∀ {c₁ e₁ s c' e' s' c₂ e₂}
         → (c₁ , e₁ , s) ⊳ (c' , e' , s')
         → (c₁ ++ c₂ , e₁ ++ e₂ , s) ⊳ (c' ++ c₂ , e' ++ e₂ , s')
  ex-4-4-single [PUSH] = [PUSH]
  ex-4-4-single [ADD] = [ADD]
  ex-4-4-single [MULT] = [MULT]
  ex-4-4-single [SUB] = [SUB]
  ex-4-4-single [TRUE] = [TRUE]
  ex-4-4-single [FALSE] = [FALSE]
  ex-4-4-single [EQ] = [EQ]
  ex-4-4-single [LE] = [LE]
  ex-4-4-single [AND] = [AND]
  ex-4-4-single [NEG] = [NEG]
  ex-4-4-single [FETCH] = [FETCH]
  ex-4-4-single [STORE] = [STORE]
  ex-4-4-single [NOOP] = [NOOP]
  ex-4-4-single { ((BRANCH tc fc) ∷ c) } {e₁} {s} {c'} {e'} {s'} {c₂} {e₂}
           ([BRANCH] {_} {_} {t} {c} {e} {s})
    rewrite ++-commute (if t then tc else fc) c c₂ =
      [BRANCH]
  ex-4-4-single { ((LOOP g b) ∷ c) } {e₁} {s} {c'} {e'} {s'} {c₂} {e₂} [LOOP]
    rewrite ++-commute g
                       (BRANCH (b ++ singleton (LOOP g b)) (singleton NOOP) ∷ c)
                            c₂ =
    [LOOP]

  ex-4-4 : ∀ {c₁ e₁ s c' e' s' c₂ e₂}
         → (c₁ , e₁ , s) ⊳⁺ (c' , e' , s')
         → (c₁ ++ c₂ , e₁ ++ e₂ , s) ⊳⁺ (c' ++ c₂ , e' ++ e₂ , s')
  ex-4-4 (finally p) = finally (ex-4-4-single p)
  ex-4-4 ([PUSH] andThen tail) = [PUSH] andThen ex-4-4 tail
  ex-4-4 ([ADD] andThen tail) = [ADD] andThen ex-4-4 tail
  ex-4-4 ([MULT] andThen tail) = [MULT] andThen ex-4-4 tail
  ex-4-4 ([SUB] andThen tail) = [SUB] andThen ex-4-4 tail
  ex-4-4 ([TRUE] andThen tail) = [TRUE] andThen ex-4-4 tail
  ex-4-4 ([FALSE] andThen tail) = [FALSE] andThen ex-4-4 tail
  ex-4-4 ([EQ] andThen tail) = [EQ] andThen ex-4-4 tail
  ex-4-4 ([LE] andThen tail) = [LE] andThen ex-4-4 tail
  ex-4-4 ([AND] andThen tail) = [AND] andThen ex-4-4 tail
  ex-4-4 ([NEG] andThen tail) = [NEG] andThen ex-4-4 tail
  ex-4-4 ([FETCH] andThen tail) = [FETCH] andThen ex-4-4 tail
  ex-4-4 ([STORE] andThen tail) = [STORE] andThen ex-4-4 tail
  ex-4-4 ([NOOP] andThen tail) = [NOOP] andThen ex-4-4 tail
  ex-4-4 { ((BRANCH tc fc) ∷ c) } {e₁ = left t ∷ e₁} {s} {c'} {e'} {s'} {c₂} {e₂}
           (([BRANCH] .{tc} .{fc} {t} {c} .{e₁} {s}) andThen tail)
           with (ex-4-4 {(if t then tc else fc) ++  c} {e₁} {s} {c'} {e'} {s'} {c₂} {e₂} tail)
  ...| p rewrite ++-commute (if t then tc else fc) c c₂ =
      [BRANCH] andThen p
  ex-4-4 { ((LOOP g b) ∷ c) } {e₁} {s} {c'} {e'} {s'} {c₂} {e₂}
         ([LOOP] andThen tail)
           with (ex-4-4 {g ++ BRANCH (b ++ [ LOOP g b ]) [ NOOP ] ∷ c}
                        {e₁} {s} {c'} {e'} {s'} {c₂} {e₂} tail)
  ...| p rewrite ++-commute g
                            (BRANCH (b ++ singleton (LOOP g b)) (singleton NOOP) ∷ c)
                            c₂ =
       [LOOP] andThen p




  two-instr-step-term-absurd :
    ∀ {i₁ i₂ c e s e'' s''}
    → (i₁ ∷ i₂ ∷ c , e , s) ⊳ ([] , e'' , s'')
    → ⊥
  two-instr-step-term-absurd {BRANCH x x₁} {i₂} {c} {e = left b ∷ e} p =
    ⊥-elim (≢-sym (right-append-non-empty-nonempty res≢[])
                    (fst $ pair-inj $ AM-step-deterministic p [BRANCH]))
    where
      res≢[] : ¬ (_≡_ {A = List Inst} (i₂ ∷ c) [])
      res≢[] ()
  two-instr-step-term-absurd {LOOP x x₁} {i₂} {c} p =
    ⊥-elim (≢-sym (right-append-non-empty-nonempty res≢[])
                    (fst $ pair-inj $ AM-step-deterministic p [LOOP]))
    where
      res≢[] : ¬ (_≡_ {A = List Inst} (_ ∷ i₂ ∷ c) [])
      res≢[] ()

  ex-4-5 : ∀ {c₁ c₂ e s e'' s''}
         → (c₁ ≢ []) → (c₂ ≢ [])
         → (c₁ ++ c₂ , e , s) ⊳⁺ ([] , e'' , s'')
         → Σ (AMStack × State) (λ ams → (c₁ , e , s) ⊳⁺ ([] , ams)
                                      × (c₂ , ams) ⊳⁺ ([] , e'' , s''))
  ex-4-5 {[]} c₁≢[] c₂≢[] (finally x) = ⊥-elim (c₁≢[] refl)
  ex-4-5 {i₁ ∷ []} {c₂ = []} c₁≢[] c₂≢[] = ⊥-elim ( c₂≢[] refl)
  ex-4-5 {i₁ ∷ []} {i₂ ∷ c₂} c₁≢[] c₂≢[] (finally x) =
    ⊥-elim $ two-instr-step-term-absurd x
  ex-4-5 { i₁ ∷ i₂ ∷ c₁ } {c₂} c₁≢[] c₂≢[] (finally x) =
    ⊥-elim $ two-instr-step-term-absurd x
  ex-4-5 { [] } c₁≢[] _ (x andThen tail) =
    ⊥-elim $ (c₁≢[] refl)
  ex-4-5 {(PUSH n ∷ c₁)} {c₂} {e} {s} {e''} {s''} _ c₂≢[] ([PUSH] andThen tail)
    with c₁
  ...| [] =
    (right (fromNat n) ∷ e , s)
    , (finally ([PUSH] {n} {[]} {e} {s}) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([PUSH] andThen c₁-terms) , c₂-terms)
  ex-4-5 { ADD ∷ c₁ } {c₂} {(right z₁ ∷ right z₂ ∷ e)} {s} _ c₂≢[] ([ADD] andThen tail)
    with c₁
  ...| [] =
    (right (z₁ + z₂) ∷ e , s)
    , (finally ([ADD]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([ADD] andThen c₁-terms) , c₂-terms)
  ex-4-5 { MULT  ∷ c₁} {c₂} {(right z₁ ∷ right z₂ ∷ e)} {s} _ c₂≢[] ([MULT] andThen tail)
    with c₁
  ...| [] =
    (right (z₁ * z₂) ∷ e , s)
    , (finally ([MULT]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([MULT] andThen c₁-terms) , c₂-terms)
  ex-4-5 {SUB ∷ c₁} {c₂} {(right z₁ ∷ right z₂ ∷ e)} {s} _ c₂≢[] ([SUB] andThen tail)
    with c₁
  ...| [] =
    (right (z₁ - z₂) ∷ e , s)
    , (finally ([SUB]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([SUB] andThen c₁-terms) , c₂-terms)
  ex-4-5 {TRUE ∷ c₁} {c₂} {e} {s} _ c₂≢[] ([TRUE] andThen tail)
    with c₁
  ...| [] =
    (left true ∷ e , s)
    , (finally ([TRUE]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([TRUE] andThen c₁-terms) , c₂-terms)
  ex-4-5 {FALSE ∷ c₁} {c₂} {e} {s} _ c₂≢[] ([FALSE] andThen tail)
    with c₁
  ...| [] =
    (left false ∷ e , s)
    , (finally ([FALSE]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([FALSE] andThen c₁-terms) , c₂-terms)
  ex-4-5 {EQ ∷ c₁} {c₂} {(right z₁ ∷ right z₂ ∷ e)} {s} _ c₂≢[] ([EQ] andThen tail)
    with c₁
  ...| [] =
    (left (z₁ ==? z₂) ∷ e , s)
    , (finally ([EQ]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([EQ] andThen c₁-terms) , c₂-terms)
  ex-4-5 {LE ∷ c₁} {c₂} {(right z₁ ∷ right z₂ ∷ e)} {s} _ c₂≢[] ([LE] andThen tail)
    with c₁
  ...| [] =
    (left (z₁ ≤? z₂) ∷ e , s)
    , (finally ([LE]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([LE] andThen c₁-terms) , c₂-terms)
  ex-4-5 {AND ∷ c₁} {c₂} {(left b₁ ∷ left b₂ ∷ e)} {s} _ c₂≢[] ([AND] andThen tail)
    with c₁
  ...| [] =
    (left (b₁ && b₂) ∷ e , s)
    , (finally ([AND]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , ( ([AND] andThen c₁-terms) , c₂-terms)
  ex-4-5 {NEG ∷ c₁} {c₂} {(left b ∷ e)} {s} _ c₂≢[] ([NEG] andThen tail)
    with c₁
  ...| [] =
    (left (not b) ∷ e , s) , (finally ([NEG]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , (([NEG] andThen c₁-terms) , c₂-terms)
  ex-4-5 {FETCH v ∷ c₁} {c₂} {e} {s} _ c₂≢[] ([FETCH] andThen tail)
    with c₁
  ...| [] =
    (right (get s v) ∷ e , s) , (finally ([FETCH]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , (([FETCH] andThen c₁-terms) , c₂-terms)
  ex-4-5 {STORE v ∷ c₁} {c₂} {(right z ∷ e)} {s} _ c₂≢[] ([STORE] andThen tail)
    with c₁
  ...| [] =
    (e , (s [ v ↦ z ] ))
    , (finally ([STORE]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , (([STORE] andThen c₁-terms) , c₂-terms)
  ex-4-5 {NOOP ∷ c₁} {c₂} {e} {s} _ c₂≢[] ([NOOP] andThen tail)
    with c₁
  ...| [] =
    (e , s)
    , (finally ([NOOP]) , tail)
  ...| (x ∷ xs) with ex-4-5 (λ ()) c₂≢[] tail
  ...| (ams , (c₁-terms , c₂-terms)) =
    ams , (([NOOP] andThen c₁-terms) , c₂-terms)
  ex-4-5 { BRANCH tc fc ∷ []} {c₂} {(left true ∷ e)} {s} _ c₂≢[] (r@[BRANCH] andThen tail)
    with tc
  ...| [] = (e , s ) , (finally [BRANCH] , tail)
  ...| tc'@(_ ∷ _) with ex-4-5 (λ ()) c₂≢[] tail
  ...| ( ams , (if-terms , rest-terms)) =
       (fst ams , snd ams)
       , ( ([BRANCH] andThen if-terms')  , rest-terms)
       where
         if-terms' = transport (λ z → (z , e , s) ⊳⁺ ([] , fst ams , snd ams))
                               (sym (right-ident tc')) if-terms
  ex-4-5 {BRANCH tc fc ∷ c₁@(_ ∷ _) } {c₂} {(left true ∷ e)} {s} _ c₂≢[] ([BRANCH] andThen tail)
    with tc
  ...| []  = let (ams , (if-terms , c₂-terms)) = ex-4-5 (λ ()) c₂≢[] tail
             in ams , (([BRANCH] andThen if-terms), c₂-terms)
  ...| tc'@(_ ∷ _) rewrite sym (monoid-comm tc' c₁ c₂) =
       let (ams , (if-terms , c₂-terms)) = ex-4-5 {c₁ = tc' ++ c₁} (λ ()) c₂≢[] tail
       in ams , (([BRANCH] andThen if-terms), c₂-terms)
  ex-4-5 { BRANCH tc fc ∷ []} {c₂} {(left false ∷ e)} {s} _ c₂≢[] (r@[BRANCH] andThen tail)
    with fc
  ...| [] = (e , s ) , (finally [BRANCH] , tail)
  ...| fc'@(_ ∷ _) with ex-4-5 (λ ()) c₂≢[] tail
  ...| ( ams , (if-terms , rest-terms)) =
       ams
       , ( ([BRANCH] andThen if-terms')  , rest-terms)
       where
         if-terms' = transport (λ z → (z , e , s) ⊳⁺ ([] , ams))
                               (sym (right-ident fc')) if-terms
  ex-4-5 {BRANCH tc fc ∷ c₁@(_ ∷ _) } {c₂} {(left false ∷ e)} {s} _ c₂≢[] ([BRANCH] andThen tail)
    with fc
  ...| []  = let (ams , (if-terms , c₂-terms)) = ex-4-5 (λ ()) c₂≢[] tail
             in ams , (([BRANCH] andThen if-terms), c₂-terms)
  ...| fc'@(_ ∷ _) rewrite sym (monoid-comm fc' c₁ c₂) =
       let (ams , (if-terms , c₂-terms)) = ex-4-5 {c₁ = fc' ++ c₁} (λ ()) c₂≢[] tail
       in ams , (([BRANCH] andThen if-terms), c₂-terms)
  ex-4-5 {LOOP x x₁ ∷ c₁ } {c₂} {e} {s} {e''} {s''} _ c₂≢[] ([LOOP] andThen tail)
--    rewrite xs++y∷ys≡xs++[y]++ys x (BRANCH (x₁ ++ [ LOOP x x₁ ]) [ NOOP ]) c₂
    rewrite sym (monoid-comm x ((BRANCH (x₁ ++ [ LOOP x x₁ ]) [ NOOP ]) ∷ c₁) c₂)
    with ex-4-5 (xs++y∷ys≢[] _ _ _) c₂≢[] tail
  ...| ams , (c₁-terms , c₂-terms) = ams , (([LOOP] andThen c₁-terms) , c₂-terms)


  deriv-append :
    ∀ {c e s c' e' s' c'' e'' s''}
    → (c , e , s) ⊳⁺ (c' , e' , s')
    → (c' , e' , s') ⊳⁺ (c'' , e'' , s'')
    → (c , e , s) ⊳⁺ (c'' , e'' , s'')
  deriv-append (finally x) p₂ =
    x andThen p₂
  deriv-append (x andThen p₁) p₂ =
    x andThen (deriv-append p₁ p₂)

  -- Could be defined such that the step code is a non singleton list, but any
  -- such proof is absurd
  cons-terminating :
    ∀ {i e s c' e' s' c'' e'' s''}
    → ( [ i ] , e , s) ⊳ ([] , e' , s')
    → ( c' , e' , s') ⊳⁺ ( c'' , e'' , s'')
    → ( i ∷ c' , e , s) ⊳⁺ ( c'' , e'' , s'')
  cons-terminating {PUSH x}  [PUSH]  am = [PUSH] andThen am
  cons-terminating {ADD}     [ADD]   am = [ADD] andThen am
  cons-terminating {MULT}    [MULT]  am = [MULT] andThen am
  cons-terminating {SUB}     [SUB]   am = [SUB] andThen am
  cons-terminating {TRUE}    [TRUE]  am = [TRUE] andThen am
  cons-terminating {FALSE}   [FALSE] am = [FALSE] andThen am
  cons-terminating {EQ}      [EQ]    am = [EQ] andThen am
  cons-terminating {LE}      [LE]    am = [LE] andThen am
  cons-terminating {AND}     [AND]   am = [AND] andThen am
  cons-terminating {NEG}     [NEG]   am = [NEG] andThen am
  cons-terminating {FETCH x} [FETCH] am = [FETCH] andThen am
  cons-terminating {STORE x} [STORE] am = [STORE] andThen am
  cons-terminating {NOOP}    [NOOP]  am = [NOOP] andThen am
  cons-terminating {BRANCH [] _} {e = left true ∷ e} step am
    with tripple-inj $ AM-step-deterministic [BRANCH] step
  ...| _ , e≡e' , s≡s'  rewrite e≡e' rewrite s≡s' =
     [BRANCH] andThen am
  cons-terminating {BRANCH (_ ∷ _) _} {e = left true ∷ e} step am
    with tripple-inj $ AM-step-deterministic [BRANCH] step
  ...| () , e≡e' , s≡s'
  cons-terminating {BRANCH _ [] } {e = left false ∷ e} step am
    with tripple-inj $ AM-step-deterministic [BRANCH] step
  ...| _ , e≡e' , s≡s'  rewrite e≡e' rewrite s≡s' =
     [BRANCH] andThen am
  cons-terminating {BRANCH _ (_ ∷ _)} {e = left false ∷ e} step am
    with tripple-inj $ AM-step-deterministic [BRANCH] step
  ...| () , e≡e' , s≡s'
  cons-terminating {LOOP x x₁} step am =
    ⊥-elim (xs++y∷ys≢[]
              _ _ _
              (fst $ pair-inj $ AM-step-deterministic [LOOP] step))

  -- inverse of ex-4-5 but has slightly larger domain
  prepend-terminating :
    ∀ {c₁ c₂ e s e' s' c'' e'' s''}
    → (c₁ , e , s) ⊳⁺ ([] , e' , s')
    → (c₂ , e' , s') ⊳⁺ (c'' , e'' , s'')
    → (c₁ ++ c₂ , e , s) ⊳⁺ (c'' , e'' , s'')
  prepend-terminating {c₁ = []} am₁ am₂ =
    ⊥-elim $ no-deriv-seq-from-empty-code am₁
  prepend-terminating {c₁ = i₁ ∷ c₁} (finally step) am
    with step-terms-tail-empty step
  ...| c₁≡[] rewrite c₁≡[] = cons-terminating step am
  prepend-terminating ([PUSH] andThen am₁) am₂ =
    [PUSH] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([ADD] andThen am₁) am₂ =
    [ADD] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([MULT] andThen am₁) am₂ =
    [MULT] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([SUB] andThen am₁) am₂ =
    [SUB] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([TRUE] andThen am₁) am₂ =
    [TRUE] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([FALSE] andThen am₁) am₂ =
    [FALSE] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([EQ] andThen am₁) am₂ =
    [EQ] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([LE] andThen am₁) am₂ =
    [LE] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([AND] andThen am₁) am₂ =
    [AND] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([NEG] andThen am₁) am₂ =
    [NEG] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([FETCH] andThen am₁) am₂ =
    [FETCH] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([STORE] andThen am₁) am₂ =
    [STORE] andThen (prepend-terminating am₁ am₂)
  prepend-terminating ([NOOP] andThen am₁) am₂ =
    [NOOP] andThen (prepend-terminating am₁ am₂)
  prepend-terminating {c₁ = BRANCH xs _ ∷ c₁}
                      {c₂ = c₂}
                      {e = left true ∷ e}
                      ([BRANCH] andThen am₁) am₂
    with (prepend-terminating am₁ am₂)
  ...| tail-proof rewrite (monoid-comm xs c₁ c₂) =
       [BRANCH] andThen tail-proof

  prepend-terminating {c₁ = BRANCH _ xs ∷ c₁}
                      {c₂ = c₂}
                      {e = left false ∷ e}
                      ([BRANCH] andThen am₁) am₂
    with (prepend-terminating am₁ am₂)
  ...| tail-proof rewrite (monoid-comm xs c₁ c₂) =
       [BRANCH] andThen tail-proof
  prepend-terminating {c₁ = LOOP guard body ∷ c₁} {c₂ = c₂}
                      ([LOOP] andThen am₁) am₂
    with (prepend-terminating am₁ am₂)
  ...| tail-proof rewrite (monoid-comm
                            guard
                            (BRANCH (body ++ [ LOOP guard body ])
                                    [ NOOP ] ∷ c₁)
                            c₂) =
       [LOOP] andThen tail-proof




  -- All terminating derivations result in the same stack and heap
  ex-4-6 : ∀ {c e s e' s' e'' s''}
         → (c , e , s) ⊳⁺ ([] , e' , s')
         → (c , e , s) ⊳⁺ ([] , e'' , s'')
         → (e' , s') ≡ (e'' , s'')
  ex-4-6 {c = c} (finally x) (finally x₁) =
    snd $ pair-inj $ AM-step-deterministic x x₁
  ex-4-6 {c = c} (finally x) (x₁ andThen p₂)
    with pair-inj (AM-step-deterministic x x₁)
  ...| []≡c''' , _ rewrite sym ([]≡c''') =
    ⊥-elim $ no-deriv-seq-from-empty-code p₂
  ex-4-6 {c = c} (x₁ andThen p) (finally x)
    with pair-inj (AM-step-deterministic x x₁)
  ...| []≡c''' , _ rewrite sym ([]≡c''') =
    ⊥-elim $ no-deriv-seq-from-empty-code p
  ex-4-6 {c = c} (x andThen p₁) (x₁ andThen p₂)
    rewrite AM-step-deterministic x x₁ =
      ex-4-6 p₁ p₂

  -- All derivations of the same size result is deterministic
  ex-4-6' : ∀ {c e s c' e' s' c'' e'' s''}
         → (p₁ : (c , e , s) ⊳⁺ (c' , e' , s'))
         → (p₂ : (c , e , s) ⊳⁺ (c'' , e'' , s''))
         → size p₁ ≡ size p₂
         → (c' , e' , s') ≡ (c'' , e'' , s'')
  ex-4-6' {c = c} (finally x) (finally x₁) _ =
    AM-step-deterministic x x₁
  ex-4-6' {c = c} (x andThen p₁) (x₁ andThen p₂) size-eq
    rewrite AM-step-deterministic x x₁ =
      ex-4-6' p₁ p₂ (suc-inj _ _ size-eq)

  ex-4-10 : ∀ {x} → CA⟦ 𝕧 x + 1 ⟧ ≡ PUSH 1 ∷ FETCH x ∷ ADD ∷ []
  ex-4-10 = refl

  -- lemma 4.18
  airth-behavior :
    ∀ {e s} (a : Aexp)
    → ( CA⟦ a ⟧ , e , s) ⊳⁺ ( [] , right (𝓐⟦ a ⟧ s) ∷ e , s)
  airth-behavior {e} {s} (𝕟 x) = finally [PUSH]
  airth-behavior {e} {s} (𝕧 x) = finally [FETCH]
  airth-behavior (a₁ +' a₂) =
    prepend-terminating (airth-behavior a₂)
      (prepend-terminating (airth-behavior a₁)
                           (finally [ADD]))
  airth-behavior (a₁ *' a₂) =
    prepend-terminating (airth-behavior a₂)
      (prepend-terminating (airth-behavior a₁)
                           (finally [MULT]))
  airth-behavior (a₁ -' a₂) =
    prepend-terminating (airth-behavior a₂)
      (prepend-terminating (airth-behavior a₁)
                           (finally [SUB]))

  -- exercise 4.19
  bool-behavior :
    ∀ {e s} (b : Bexp)
    → ( CB⟦ b ⟧ , e , s) ⊳⁺ ( [] , left (ℬ⟦ b ⟧ s) ∷ e , s)
  bool-behavior true' = finally [TRUE]
  bool-behavior false' = finally [FALSE]
  bool-behavior (a₁ ==' a₂) =
    prepend-terminating (airth-behavior a₂)
      (prepend-terminating (airth-behavior a₁)
                           (finally [EQ]))
  bool-behavior (a₁ <=' a₂) =
    prepend-terminating (airth-behavior a₂)
      (prepend-terminating (airth-behavior a₁)
                           (finally [LE]))
  bool-behavior (!' b) =
    prepend-terminating (bool-behavior b) (finally [NEG])
  bool-behavior (b₁ &&' b₂) =
    prepend-terminating (bool-behavior b₂)
      (prepend-terminating (bool-behavior b₁)
                           (finally [AND]))

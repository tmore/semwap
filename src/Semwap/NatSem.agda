{-
Author: Tomas Möre 2020
-}
module Semwap.NatSem where




open import Prelude hiding (force)
open import Semwap.Expr
open import Semwap.While



-- big-ste or / natural semantic relation definition given as a datatype. Has no
-- meaning by itself, later we can show that it imples proofs about actual
-- semantic functions
data _⇾_ : (While × State) → State → Set where
  [assₙₛ] : ∀ {x a s} → ⟨ (x := a) , s ⟩ ⇾ (s [ x ↦ 𝓐⟦ a ⟧ s ])
  [skipₙₛ] : ∀ {s} → ⟨ skip , s ⟩ ⇾ s
  [compₙₛ] :  ∀ {S₁ S₂ s s' s''}
           → (⟨ S₁ , s ⟩ ⇾ s') → (⟨ S₂ , s' ⟩ ⇾ s'')
           → ⟨ S₁ ∶ S₂ , s ⟩ ⇾ s''
  [ifₙₛᵗᵗ] : ∀ {S₁ S₂ s s' b}
           → (ℬ⟦ b ⟧ s ≡ true) → ⟨ S₁ , s ⟩ ⇾ s'
           → ⟨ if' b then S₁ else S₂ , s ⟩ ⇾ s'
  [ifₙₛᶠᶠ] : ∀ {S₁ S₂ s s' b}
           → (ℬ⟦ b ⟧ s ≡ false)
           → ⟨ S₂ , s ⟩ ⇾ s'
           → ⟨ if' b then S₁ else S₂ , s ⟩ ⇾ s'

  [whileₙₛᵗᵗ] :
    ∀ {b S s s' s''}
    → (ℬ⟦ b ⟧ s ≡ true)
    → ⟨ S , s ⟩ ⇾ s'
    → ⟨ while b run S , s' ⟩ ⇾ s''
    → ⟨ while b run S , s ⟩ ⇾ s''
  [whileₙₛᶠᶠ] :
    ∀ {S s b}
    → (ℬ⟦ b ⟧ s ≡ false)
    → ⟨ while b run S , s ⟩ ⇾ s



private
  x y z : Var
  x = 0
  y = 1
  z = 2

_ : ⟨ (x := 𝕟 1) , ε ⟩ ⇾ (ε [ x ↦ 1 ])
_ = [assₙₛ]

module _ where
  private
    s₀ s₁ s₂ s₃ : State
    s₀ = (ε [ x ↦ 5 ]) [ y ↦ 7 ]
    s₁ = s₀ [ z ↦ 5 ]
    s₂ = s₁ [ x ↦ 7 ]
    s₃ = s₂ [ y ↦ 5 ]

  ex-2-1 : ⟨ ((z := 𝕧 x ∶ x := 𝕧 y) ∶ y := 𝕧 z) , s₀ ⟩ ⇾ s₃
  ex-2-1 = [compₙₛ] ([compₙₛ] [assₙₛ] [assₙₛ]) [assₙₛ]


module _ where
  private
    s₀  : State
    s₀ = ε [ x ↦ 3 ]

  ex-2-2 : ⟨ (y := 𝕟 1 ∶ while !' (𝕧 x ==' 𝕟 1) run (y := 𝕧 y *' 𝕧 x ∶ x := 𝕧 x -' 𝕟 1)) , s₀ ⟩ ⇾ (s₀ [ y ↦ 6 ] [ x ↦ 1 ])
  ex-2-2 = [compₙₛ] [assₙₛ]
                   ([whileₙₛᵗᵗ] refl
                              ([compₙₛ] [assₙₛ] [assₙₛ])
                              ([whileₙₛᵗᵗ] refl
                                         ([compₙₛ] [assₙₛ] [assₙₛ])
                                         ([whileₙₛᶠᶠ] refl)))

module _ where
  private
    s₀ : State
    s₀ = ε [ x ↦ 17 ] [ y ↦ 5 ]

  ex-2-3 : ⟨ (z := 𝕟 0 ∶ while 𝕧 y <=' 𝕧 x run (z := 𝕧 z +' 𝕟 1 ∶ x := 𝕧 x -' 𝕧 y)) , s₀ ⟩ ⇾ (s₀ [ x ↦ 2 ] [ y ↦ 5 ] [ z ↦ 3 ])
  ex-2-3 = [compₙₛ] [assₙₛ]
                    ([whileₙₛᵗᵗ]
                      refl
                      ([compₙₛ] [assₙₛ] [assₙₛ])
                      ([whileₙₛᵗᵗ]
                        refl
                        ([compₙₛ] [assₙₛ] [assₙₛ])
                        ([whileₙₛᵗᵗ]
                          refl
                          ([compₙₛ] [assₙₛ] [assₙₛ])
                          ([whileₙₛᶠᶠ] refl))))

infix 4 _≌_
record _≌_ {ℓ} (A B : Set ℓ) : Set ℓ where
 constructor sem-eq
 field
   from : A → B
   to : B → A

≌-refl : ∀ {ℓ} → {A : Set ℓ} → A ≌ A
≌-refl = sem-eq id id

≌-sym : ∀ {ℓ} {A B : Set ℓ} → A ≌ B → B ≌ A
≌-sym (sem-eq a→b b→a) = (sem-eq b→a a→b)

≌-trans :  ∀ {ℓ} {A B C : Set ℓ} →  A ≌ B → B ≌ C → A ≌ C
≌-trans (sem-eq a→b b→a) (sem-eq b→c c→b) = sem-eq (b→c ∘ a→b) (b→a ∘ c→b)

_≌ₙₛ_ : (S₁ S₂ : While) → Set
_≌ₙₛ_ S₁ S₂ = {s s' : State} → ⟨ S₁ , s' ⟩ ⇾ s' ≌  ⟨ S₂ , s' ⟩ ⇾ s'


record _≡ₙₛ_ (S₁ S₂ : While) : Set where
  constructor ns-eq
  field
    S₁⇒S₂ : (s s' : State) → ⟨ S₁ , s ⟩ ⇾ s' → ⟨ S₂ , s ⟩ ⇾ s'
    S₂⇒S₁ : (s s' : State) → ⟨ S₂ , s ⟩ ⇾ s' → ⟨ S₁ , s ⟩ ⇾ s'


≡ₙₛ-refl : {S : While} → (S ≡ₙₛ S)
≡ₙₛ-refl =
  ns-eq (λ s s' t → t) (λ s s' t → t)

≡ₙₛ-sym : {S₁ S₂ : While} → S₁ ≡ₙₛ S₂ → S₂ ≡ₙₛ S₁
≡ₙₛ-sym (ns-eq to from) = ns-eq from to

≡ₙₛ-trans : {S₁ S₂ S₃ : While} → S₁ ≡ₙₛ S₂ → S₂ ≡ₙₛ S₃ → S₁ ≡ₙₛ S₃
≡ₙₛ-trans (ns-eq to from) (ns-eq to' from') =
  ns-eq (λ s s' S₁ → to' s s' (to s s' S₁))
        (λ s s' S₃ → from s s' (from' s s' S₃))



no-inf-loop : ∀ {s s'} → ¬ ( ⟨ while true' run skip , s ⟩ ⇾ s')
no-inf-loop ([whileₙₛᵗᵗ] x₁ [skipₙₛ] p₁) =
  no-inf-loop p₁

if-comp-right-dist :
  ∀ {S₁ S₂ S₃ b}
  → ((if' b then S₁ else S₂) ∶ S₃) ≡ₙₛ (if' b then (S₁ ∶ S₃) else (S₂ ∶ S₃))
if-comp-right-dist {S₁} {S₂} {S₃} {b} =
  ns-eq outer-to-inner inner-to-outer
  where
    lhs = (if' b then S₁ else S₂) ∶ S₃
    rhs = if' b then (S₁ ∶ S₃) else (S₂ ∶ S₃)
    outer-to-inner : (s s'' : State) → ⟨ lhs , s ⟩ ⇾ s'' → ⟨ rhs , s ⟩ ⇾ s''
    outer-to-inner s s'' ([compₙₛ] ([ifₙₛᵗᵗ] ℬ⟦b⟧ S₁ₙₛ) S₃ₙₛ) =
      [ifₙₛᵗᵗ] ℬ⟦b⟧ ([compₙₛ] S₁ₙₛ S₃ₙₛ)
    outer-to-inner s s'' ([compₙₛ] ([ifₙₛᶠᶠ] ¬ℬ⟦b⟧ S₂ₙₛ) S₃ₙₛ) =
      [ifₙₛᶠᶠ] ¬ℬ⟦b⟧ ([compₙₛ] S₂ₙₛ S₃ₙₛ)
    inner-to-outer : (s s'' : State) → ⟨ rhs , s ⟩ ⇾ s'' → ⟨ lhs , s ⟩ ⇾ s''
    inner-to-outer s s'' ([ifₙₛᵗᵗ] ℬ⟦b⟧ ([compₙₛ] S₁ₙₛ S₃ₙₛ)) =
      [compₙₛ] ([ifₙₛᵗᵗ] ℬ⟦b⟧ S₁ₙₛ) S₃ₙₛ
    inner-to-outer s s'' ([ifₙₛᶠᶠ] ¬ℬ⟦b⟧ ([compₙₛ] S₂ₙₛ S₃ₙₛ)) =
      [compₙₛ] ([ifₙₛᶠᶠ] ¬ℬ⟦b⟧ S₂ₙₛ) S₃ₙₛ

lem-2-4c : ∀ {s s'} → ¬ (⟨ while true' run skip , s ⟩ ⇾ s')
lem-2-4c ([whileₙₛᶠᶠ] ())
lem-2-4c ([whileₙₛᵗᵗ] _ _ rec)  = lem-2-4c rec

lem-2-5 : (b : Bexp) (S : While)
        → (while b run S) ≡ₙₛ (if' b then (S ∶ while b run S) else skip)
lem-2-5 b S =
   ns-eq while-to-unfold unfold-to-while
  where
    lhs = while b run S
    rhs = if' b then (S ∶ while b run S) else skip
    while-to-unfold : (s s'' : State) → ⟨ lhs , s ⟩ ⇾ s'' → ⟨ rhs , s ⟩ ⇾ s''
    while-to-unfold s s''
      ([whileₙₛᵗᵗ] ℬ⟦b⟧s≡true ⟨S,s⟩⇾s' ⟨whilebrunS,s'⟩⇾s'') =
        [ifₙₛᵗᵗ] ℬ⟦b⟧s≡true ([compₙₛ] ⟨S,s⟩⇾s' ⟨whilebrunS,s'⟩⇾s'')
    while-to-unfold s s''
      ([whileₙₛᶠᶠ] ℬ⟦b⟧s≡false) =
        [ifₙₛᶠᶠ] ℬ⟦b⟧s≡false [skipₙₛ]

    unfold-to-while : (s s'' : State) → ⟨ rhs , s ⟩ ⇾ s'' → ⟨ lhs , s ⟩ ⇾ s''
    unfold-to-while s s''
      ([ifₙₛᵗᵗ] ℬ⟦b⟧s≡true ([compₙₛ] ⟨S,s⟩⇾s' ⟨whilebrunS,s'⟩⇾s'')) =
        [whileₙₛᵗᵗ] ℬ⟦b⟧s≡true ⟨S,s⟩⇾s' ⟨whilebrunS,s'⟩⇾s''
    unfold-to-while s s''
      ([ifₙₛᶠᶠ] ℬ⟦b⟧s≡false [skipₙₛ]) =
        [whileₙₛᶠᶠ] ℬ⟦b⟧s≡false


ex-2-6 : {S₁ S₂ S₃ : While} → (S₁ ∶ S₂ ∶ S₃) ≡ₙₛ ((S₁ ∶ S₂) ∶ S₃)
ex-2-6 {S₁} {S₂} {S₃} =
  ns-eq to from
  where
    to : (s s₃ : State) → ⟨ S₁ ∶ S₂ ∶ S₃ , s ⟩ ⇾ s₃ → ⟨ (S₁ ∶ S₂) ∶ S₃ , s ⟩ ⇾ s₃
    to  s s₃ ([compₙₛ] ⟨S₁,s⟩⇾s₁ ([compₙₛ] ⟨S₂,s₁⟩⇾s₂ ⟨S₃,s₂⟩⇾s₃)) =
      [compₙₛ] ([compₙₛ] ⟨S₁,s⟩⇾s₁ ⟨S₂,s₁⟩⇾s₂) ⟨S₃,s₂⟩⇾s₃
    from : (s s₃ : State) → ⟨ (S₁ ∶ S₂) ∶ S₃ , s ⟩ ⇾ s₃ → ⟨ S₁ ∶ S₂ ∶ S₃ , s ⟩ ⇾ s₃
    from  s s₃ ([compₙₛ] ([compₙₛ] ⟨S₁,s⟩⇾s₁ ⟨S₂,s₁⟩⇾s₂) ⟨S₃,s₂⟩⇾s₃) =
      [compₙₛ] ⟨S₁,s⟩⇾s₁ ([compₙₛ]  ⟨S₂,s₁⟩⇾s₂ ⟨S₃,s₂⟩⇾s₃)


thm-2-9 : {S : While} → {s s' s'' : State}
        → ⟨ S , s ⟩ ⇾ s' → ⟨ S , s ⟩ ⇾ s''
        → s' ≡ s''
thm-2-9 [skipₙₛ] [skipₙₛ] = refl
thm-2-9 [assₙₛ] [assₙₛ] = refl
thm-2-9 ([compₙₛ] ⟨S₁,s⟩⇾s³ ⟨S₂,s³⟩⇾s') ([compₙₛ] ⟨S₁,s⟩⇾s⁴ ⟨S₁,s⁴⟩⇾s'')
  rewrite thm-2-9 ⟨S₁,s⟩⇾s³ ⟨S₁,s⟩⇾s⁴ =
   thm-2-9 ⟨S₂,s³⟩⇾s' ⟨S₁,s⁴⟩⇾s''
thm-2-9 ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ⟨S₁,s⟩⇾s') ([ifₙₛᵗᵗ] _ ⟨S₁,s⟩⇾s'') =
  thm-2-9 ⟨S₁,s⟩⇾s' ⟨S₁,s⟩⇾s''
thm-2-9 ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ⟨S₂,s⟩⇾s') ([ifₙₛᶠᶠ] _ ⟨S₂,s⟩⇾s'') =
  thm-2-9 ⟨S₂,s⟩⇾s' ⟨S₂,s⟩⇾s''
thm-2-9 ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true _) ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false _)
  with (trans (sym ℬ⟦b⟧≡true) ℬ⟦b⟧≡false)
...| ()
thm-2-9 ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false _) ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true _)
  with (trans (sym ℬ⟦b⟧≡true) ℬ⟦b⟧≡false)
...| ()
thm-2-9 ([whileₙₛᵗᵗ] _ ⟨S,s⟩⇾s³ ⟨while,s³⟩⇾s')
        ([whileₙₛᵗᵗ] _ ⟨S,s⟩⇾s⁴ ⟨while,s⁴⟩⇾s'')
  rewrite thm-2-9 ⟨S,s⟩⇾s³ ⟨S,s⟩⇾s⁴ =
  thm-2-9 ⟨while,s³⟩⇾s' ⟨while,s⁴⟩⇾s''
thm-2-9 ([whileₙₛᶠᶠ] _) ([whileₙₛᶠᶠ] _) =
  refl
thm-2-9 ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true _ _) ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false)
  with (trans (sym ℬ⟦b⟧≡true) ℬ⟦b⟧≡false)
...| ()
thm-2-9 ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false) ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true _ _)
  with (trans (sym ℬ⟦b⟧≡true) ℬ⟦b⟧≡false)
...| ()



module ComputeTactic where
  open import Tactic.Reflection
  -- Tactic for computing the derivation tree of a fully computable term.
  compute-ns' : Nat → While → State → Maybe (Term × State)
  compute-ns' 0 _ _ = nothing
  compute-ns' (suc n) skip s = just ((con (quote [skipₙₛ]) []), s)
  compute-ns' (suc n) (x := a) s = just ((con (quote [assₙₛ]) []) , s [ x ↦ 𝓐⟦ a ⟧ s ])
  compute-ns' (suc n) (S₁ ∶ S₂) s = do
    (S₁-p , s' ) ← compute-ns' n S₁ s
    (S₂-p , s'' ) ← compute-ns' n S₂ s'
    just ((con₂ (quote [compₙₛ]) S₁-p S₂-p) , s'')
  compute-ns' (suc n) (if' b then S₁ else S₂) s
    with inspect (ℬ⟦ b ⟧ s)
  ...| true with≡ ℬ⟦b⟧s≡true = do
     (S₁-p , s') ← compute-ns' n S₁ s
     just ( (con₂ (quote [ifₙₛᵗᵗ]) (con₀ (quote refl)) S₁-p) , s')
  ...| false with≡ ℬ⟦b⟧s≡false = do
     (S₂-p , s') ← compute-ns' n S₂ s
     just ( (con₂ (quote [ifₙₛᶠᶠ]) (con₀ (quote refl)) S₂-p) , s')
  compute-ns' (suc n) (while b run S) s
    with inspect (ℬ⟦ b ⟧ s)
  ...| true with≡ ℬ⟦b⟧s≡true = do
     (S-p , s') ← compute-ns' n S s
     (while-p , s'') ← compute-ns' n (while b run S) s'
     just ( (con₃ (quote [whileₙₛᵗᵗ]) (con₀ (quote refl)) S-p while-p) , s'')
  ...| false with≡ ℬ⟦b⟧s≡false = do
     just (con₁ (quote [whileₙₛᶠᶠ]) (con₀ (quote refl)) , s)


  macro
    compute-ns : Nat → Tactic
    compute-ns n goal = do
      goalTerm <- inferType goal
      goalTerm' <- normalise goalTerm
      case goalTerm' of
       λ { (def (quote _⇾_)
                ((arg _ (con (quote _,_)
                        (_ ∷ _ ∷ _ ∷ _ ∷ arg _ S' ∷ arg _ s' ∷ []))) ∷ _)) → do
                        S ← unquoteTC S'
                        s ← unquoteTC s'
                        case (compute-ns' n S s) of
                           λ { (just (p , _)) → unify goal p
                             ; nothing → typeErrorS "Exeeded execution depth"
                             }
         ;  _ -> typeErrorS "Expected natural semantic goal"
         }

  module _ where
   private
     s₀ : State
     s₀ = ε [ x ↦ 17 ] [ y ↦ 5 ]
   _ : ⟨ (z := 𝕟 0 ∶ while 𝕧 y <=' 𝕧 x run (z := 𝕧 z +' 𝕟 1 ∶ x := 𝕧 x -' 𝕧 y)) , s₀ ⟩ ⇾ (s₀ [ x ↦ 2 ] [ y ↦ 5 ] [ z ↦ 3 ])
   _ = compute-ns 10

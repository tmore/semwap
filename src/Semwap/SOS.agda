module Semwap.SOS where

open import Prelude hiding (guard )

open import Prelude.Extras
open import Prelude.Extras.Bool

open import Semwap.While
open import Semwap.Expr




data _⇒_ : (While × State) → (Either State (While × State)) → Set where
  [assₛₒₛ] : ∀ {x a s} → ⟨ x := a , s ⟩ ⇒ left (s [ x ↦ 𝓐⟦ a ⟧ s ])
  [skipₛₒₛ] : ∀ {s} → ⟨ skip , s ⟩ ⇒ left s
  [comp₁ₛₒₛ] : ∀ {S₁ S₁' S₂ s s'}
             → ⟨ S₁ , s ⟩ ⇒ right ⟨ S₁' , s' ⟩
             → ⟨ S₁ ∶ S₂ , s ⟩ ⇒ right ⟨ S₁' ∶ S₂ , s' ⟩
  [comp₂ₛₒₛ] : ∀ {S₁ S₂ s s'}
             → ⟨ S₁ , s ⟩ ⇒ left s'
             → ⟨ S₁ ∶ S₂ , s ⟩ ⇒ right ⟨ S₂ , s' ⟩
  [ifₛₒₛᵗᵗ] : ∀ {b S₁ S₂ s}
            → ℬ⟦ b ⟧ s ≡ true
            → ⟨ if' b then S₁ else S₂ , s ⟩ ⇒ right ⟨ S₁ , s ⟩
  [ifₛₒₛᶠᶠ] : ∀ {b S₁ S₂ s}
            → ℬ⟦ b ⟧ s ≡ false
            → ⟨ if' b then S₁ else S₂ , s ⟩ ⇒ right ⟨ S₂ , s ⟩
  [whileₛₒₛ] : ∀ {b S s}
            → ⟨ while b run S , s ⟩ ⇒ right ⟨ if' b then (S ∶ while b run  S) else skip , s ⟩


infixr 4 _andThen_
data _⇒⁺_ : (While × State) → (Either State (While × State)) → Set where
  finally : ∀ {S s a}
         → ⟨ S , s ⟩ ⇒ a
         → ⟨ S , s ⟩ ⇒⁺ a
  _andThen_ : ∀ {S S' s s' a}
            → ⟨ S , s ⟩ ⇒ right ⟨ S' , s' ⟩
            → ⟨ S' , s' ⟩ ⇒⁺ a
            → ⟨ S , s ⟩ ⇒⁺ a

evalLength : ∀ {From To} → From ⇒⁺ To → Nat
evalLength (finally _) = 1
evalLength (_ andThen rest) = suc (evalLength rest)

evalLength-not-zero : ∀ {S s a} → (seq : ⟨ S , s ⟩ ⇒⁺ a) → evalLength seq ≢ 0
evalLength-not-zero (finally _) ()
evalLength-not-zero (_ andThen _) ()

append : ∀ {S s S' s' a}
                    → ⟨ S , s ⟩ ⇒⁺ right ⟨ S' , s' ⟩
                    → ⟨ S' , s' ⟩ ⇒⁺ a
                    → ⟨ S , s ⟩ ⇒⁺ a
append (finally x) t = x andThen t
append (prev andThen x) t = prev andThen (append x  t)

private
  x y z : Var
  x = 0
  y = 1
  z = 2
module _ where

  private
    s₀ s₁ s₂ s₃ : State
    s₀ = (ε [ x ↦ 5 ]) [ y ↦ 7 ]
    s₁ = s₀ [ z ↦ 5 ]
    s₂ = s₁ [ x ↦ 7 ]
    s₃ = s₂ [ y ↦ 5 ]

  ex-2-14 : ⟨ ((z := 𝕧 x ∶ x := 𝕧 y) ∶ y := 𝕧 z) , s₀ ⟩ ⇒ right ⟨ x := 𝕧 y ∶ y := 𝕧 z , s₁ ⟩
  ex-2-14 = [comp₁ₛₒₛ] ([comp₂ₛₒₛ] [assₛₒₛ])

module _ where

  private
    s₀ : State
    s₀ = (ε [ x ↦ 3 ])

    body : While
    body = y := 𝕧 y * 𝕧 x ∶ x := 𝕧 x - 1
    guard : Bexp
    guard = !' (𝕧 x ==' 1)
    whileStm : While
    whileStm = while guard run body

  ex-2-15-1 : ⟨ y := 1 ∶ whileStm , s₀ ⟩
          ⇒ right ⟨ whileStm , (s₀ [ y ↦ 1 ]) ⟩
  ex-2-15-1 = [comp₂ₛₒₛ] [assₛₒₛ]

  ex-2-15-2 : ⟨ whileStm , (s₀ [ y ↦ 1 ]) ⟩
            ⇒ right ⟨ if' guard then (body ∶ whileStm) else skip ,  (s₀ [ y ↦ 1 ]) ⟩
  ex-2-15-2 = [whileₛₒₛ]

  ex-2-15-3 : ⟨ if' guard then (body ∶ whileStm) else skip ,  (s₀ [ y ↦ 1 ]) ⟩
             ⇒ right ⟨ body ∶ whileStm , (s₀ [ y ↦ 1 ]) ⟩
  ex-2-15-3 = [ifₛₒₛᵗᵗ] refl

  ex-2-15-4 : ⟨  body ∶ whileStm , (s₀ [ y ↦ 1 ]) ⟩
             ⇒ right ⟨ x := 𝕧 x - 1 ∶ whileStm , (s₀ [ y ↦ 3 ]) ⟩
  ex-2-15-4 = [comp₁ₛₒₛ] ([comp₂ₛₒₛ] [assₛₒₛ])

  ex-2-15-5 : ⟨  x := 𝕧 x - 1 ∶ whileStm , (s₀ [ y ↦ 3 ]) ⟩
             ⇒ right ⟨  whileStm , (s₀ [ y ↦ 3 ] [ x ↦ 2 ]) ⟩
  ex-2-15-5 = [comp₂ₛₒₛ] [assₛₒₛ]


  ex-2-15 : ⟨ y := 1 ∶ whileStm , s₀ ⟩ ⇒⁺ right ⟨  whileStm , (s₀ [ y ↦ 3 ] [ x ↦ 2 ]) ⟩
  ex-2-15 = ex-2-15-1
            andThen ex-2-15-2
            andThen ex-2-15-3
            andThen ex-2-15-4
            andThen (finally ex-2-15-5)

-- ex-2-15-1 (evalStep ex-2-15-2 {!!}) -- derivCons₁ (derivCons₁ (derivCons₁ (derivCons₁ {!!} {!!}) {!!}) {!!}) ([comp₂ₛₒₛ] {!!})


module Properties where


  evalLength-suc : ∀ {S s a}
                 → (seq : ⟨ S , s ⟩ ⇒⁺ a)
                 → Σ Nat (λ n → evalLength seq ≡ suc n)
  evalLength-suc (finally _) = 0 , refl
  evalLength-suc (_ andThen seq) =
    suc (fst (evalLength-suc seq))
    , cong suc (snd (evalLength-suc seq))


  step-deterministic :
    ∀ {S s a a'}
    → ⟨ S , s ⟩ ⇒ a
    → ⟨ S , s ⟩ ⇒ a'
    → a ≡ a'
  step-deterministic {x₁ := x₂} [assₛₒₛ] [assₛₒₛ] = refl
  step-deterministic {skip} [skipₛₒₛ] [skipₛₒₛ] = refl
  step-deterministic {if' x₁ then S₁ else S₂} ([ifₛₒₛᵗᵗ] x₂) ([ifₛₒₛᵗᵗ] x₃) = refl
  step-deterministic {if' x₁ then S₁ else S₂} ([ifₛₒₛᵗᵗ] x₂) ([ifₛₒₛᶠᶠ] x₃) =
    ⊥-elim (true≢false (trans (sym x₂) x₃))
  step-deterministic {if' x₁ then S₁ else S₂} ([ifₛₒₛᶠᶠ] x₂) ([ifₛₒₛᵗᵗ] x₃) =
    ⊥-elim (false≢true (trans (sym x₂) x₃))
  step-deterministic {if' x₁ then S₁ else S₂} ([ifₛₒₛᶠᶠ] x₂) ([ifₛₒₛᶠᶠ] x₃) = refl
  step-deterministic {while x₁ run S₁} [whileₛₒₛ] [whileₛₒₛ] = refl
  step-deterministic {x₁ := x₂ ∶ S₂} ([comp₂ₛₒₛ] a) ([comp₂ₛₒₛ] b)
    rewrite left-inj (step-deterministic a b) = refl
  step-deterministic {skip ∶ S₂} ([comp₂ₛₒₛ] a) ([comp₂ₛₒₛ] b)
    rewrite left-inj (step-deterministic a b) = refl
  step-deterministic {(S₁ ∶ S₃) ∶ S₂} ([comp₁ₛₒₛ] a) ([comp₁ₛₒₛ] b)
    rewrite fst (pair-inj (right-inj (step-deterministic a b)))
    rewrite snd (pair-inj (right-inj (step-deterministic a b)))
    = refl
  step-deterministic {(if' x₁ then S₁ else S₃) ∶ S₂} ([comp₁ₛₒₛ] a) ([comp₁ₛₒₛ] b)
    rewrite fst (pair-inj (right-inj (step-deterministic a b)))
    rewrite snd (pair-inj (right-inj (step-deterministic a b)))
    = refl
  step-deterministic {(while x₁ run S₁) ∶ S₂} ([comp₁ₛₒₛ] a) ([comp₁ₛₒₛ] b)
    rewrite fst (pair-inj (right-inj (step-deterministic a b)))
    rewrite snd (pair-inj (right-inj (step-deterministic a b)))
    = refl


  halt-deterministic :
    ∀ {S s s' s''}
    → ⟨ S , s ⟩ ⇒⁺ left s'
    → ⟨ S , s ⟩ ⇒⁺ left s''
    → s' ≡ s''
  halt-deterministic {S = x₁ := x₂} (finally [assₛₒₛ]) (finally [assₛₒₛ]) = refl
  halt-deterministic {S = skip} (finally [skipₛₒₛ]) (finally [skipₛₒₛ]) = refl
  halt-deterministic {S = if' x₁ then S else S₁} (x₂ andThen a) (x₃ andThen b)
    rewrite fst (pair-inj (right-inj (step-deterministic x₂ x₃)))
    rewrite snd (pair-inj (right-inj (step-deterministic x₂ x₃)))
    rewrite halt-deterministic a b
    = refl
  halt-deterministic {S = while x₁ run S} (x₂ andThen a) (x₃ andThen b)
    rewrite fst (pair-inj (right-inj (step-deterministic x₂ x₃)))
    rewrite snd (pair-inj (right-inj (step-deterministic x₂ x₃)))
    rewrite halt-deterministic a b
    = refl
  halt-deterministic {S = S ∶ S₁} (x₃ andThen a) (x₄ andThen b)
    rewrite fst (pair-inj (right-inj (step-deterministic x₃ x₄)))
    rewrite snd (pair-inj (right-inj (step-deterministic x₃ x₄)))
    rewrite halt-deterministic a b
    = refl

  lem-2-19 : ∀ {S₁ S₂ s s''}
           → (proofChain : ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ left s'')
           → Σ State (λ s' → ⟨ S₁ , s ⟩ ⇒⁺ left s' ×  ⟨ S₂ , s' ⟩ ⇒⁺ left s'')
  lem-2-19 (finally ())
  lem-2-19 (([comp₂ₛₒₛ] {s' = s'} r) andThen t) = s' , (finally r , t)
  lem-2-19 (([comp₁ₛₒₛ] {s' = s'} r) andThen t) =
    let (s' , r' , t') = lem-2-19 t
    in s' , (r andThen r') , t'

  lem-2-19-nat : ∀ {S₁ S₂ s s''}
               → (seq : ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ left s'')
               → evalLength seq ≡ evalLength (fst (snd (lem-2-19 seq))) + evalLength (snd (snd (lem-2-19 seq)))
  lem-2-19-nat (finally ())
  lem-2-19-nat (([comp₂ₛₒₛ] r) andThen t) = refl
  lem-2-19-nat (([comp₁ₛₒₛ] r) andThen t) = cong suc (lem-2-19-nat t)

  lem-2-19-inv : ∀ {S₁ S₂ s s' s''}
               → ⟨ S₁ , s ⟩ ⇒⁺ left s'
               → ⟨ S₂ , s' ⟩ ⇒⁺ left s''
               → ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ left s''
  lem-2-19-inv (finally x₁) x₂ = [comp₂ₛₒₛ] x₁ andThen x₂
  lem-2-19-inv (x₁ andThen x₃) x₂ = ([comp₁ₛₒₛ] x₁) andThen (lem-2-19-inv x₃ x₂)



  ex-2-20 : ¬ (∀ {S₁ S₂ s s'} → ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ right ⟨ S₂ , s' ⟩ → ⟨ S₁ , s ⟩ ⇒⁺ left s')
  ex-2-20 f = absurd (halt-deterministic contr (f p))
    where
      S₁ = skip
      S₂ = while true' run (y := 𝕧 y + 1)
      s = ε
      s' = ε [ y ↦ 1 ]
      p : ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ right ⟨ S₂ , s' ⟩
      p = [comp₂ₛₒₛ] [skipₛₒₛ] andThen ([whileₛₒₛ] andThen ([ifₛₒₛᵗᵗ] refl) andThen (finally ([comp₂ₛₒₛ] [assₛₒₛ])))
      contr : ⟨ S₁ , s ⟩ ⇒⁺ left s
      contr = finally [skipₛₒₛ]
      absurd : ¬ ([] ≡ singleton (1 , pos 1))
      absurd ()


  ex-2-21 : ∀ {S₁ S₂ s s'}
          → ⟨ S₁ , s ⟩ ⇒⁺ left s'
          → ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ right ⟨ S₂ , s' ⟩
  ex-2-21 (finally x₁) = finally ([comp₂ₛₒₛ] x₁)
  ex-2-21 (x₁ andThen foo) = ([comp₁ₛₒₛ] x₁) andThen (ex-2-21 foo)

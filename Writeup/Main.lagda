\documentclass[a4paper]{article}

\usepackage[a4paper, total={6in, 8in}]{geometry}

\usepackage[backend=bibtex]{biblatex}
\addbibresource{references.bib}

%\usepackage[mathletters]{ucs}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{hyperref}
\usepackage{ stmaryrd }

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{ stmaryrd }
\usepackage{ amssymb }
\usepackage{ textcomp }
\usepackage{ebproof}
\usepackage{xifthen}
\usepackage{graphicx}


\usepackage{agda}

\usepackage{dot2texi}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}

\usepackage{mathrsfs}
\usepackage{fontspec}
\usepackage{unicode-math}
\usepackage{cprotect}
\setmathfont{Cambria Math}
\setmathfont{XITS Math}


\usepackage{listings}
\lstset{ basicstyle=\ttfamily, mathescape }

\newcommand{\myparagraph}[1]{\paragraph{#1}\mbox{}\\}

\usepackage{newunicodechar}
\newunicodechar{λ}{\ensuremath{\mathnormal\lambda}}
\newunicodechar{←}{\ensuremath{\mathnormal\shortleftarrow}}
\newunicodechar{→}{\ensuremath{\mathnormal\shortrightarrow}}
\newunicodechar{⇾}{\ensuremath{\mathnormal\rightarrowtriangle}}
\newunicodechar{⇒}{\ensuremath{\Rightarrow}}
\newunicodechar{↠}{\ensuremath{\twoheadrightarrow}}

\newunicodechar{⇓}{\ensuremath{\Downarrow}}
\newunicodechar{↦}{\ensuremath{\mathnormal\mapsto}}
\newunicodechar{∀}{\ensuremath{\mathnormal\forall}}
\newunicodechar{ℕ}{\ensuremath{\mathbb{N}}}
\newunicodechar{𝕟}{\ensuremath{\mathbb{n}}}
\newunicodechar{𝔸}{\ensuremath{\mathbb{A}}}
\newunicodechar{𝕒}{\ensuremath{\mathbb{a}}}
\newunicodechar{𝔹}{\ensuremath{\mathbb{B}}}
\newunicodechar{ℍ}{\ensuremath{\mathbb{H}}}
\newunicodechar{𝕙}{\ensuremath{\mathbb{h}}}
\newunicodechar{𝕍}{\ensuremath{\mathbb{V}}}
\newunicodechar{𝕧}{\ensuremath{\mathbb{v}}}

\newunicodechar{⊳}{\ensuremath{\vartriangleright}}


\newunicodechar{ℤ}{\ensuremath{\mathbb{Z}}}
\newunicodechar{𝕫}{\ensuremath{\mathbb{z}}}
\newunicodechar{𝔽}{\ensuremath{\mathbb{F}}}
\newunicodechar{ℙ}{\ensuremath{\mathbb{P}}}
\newunicodechar{𝕎}{\ensuremath{\mathbb{W}}}

\newunicodechar{ϕ}{\ensuremath{\mathbb{\phi}}}
\newunicodechar{ψ}{\ensuremath{\mathbb{\psi}}}
\newunicodechar{γ}{\ensuremath{\gamma}}

\newunicodechar{≟}{\ensuremath{\stackrel{?}{=}}}
\newunicodechar{≤}{\ensuremath{\le}}
\newunicodechar{≡}{\ensuremath{\equiv}}
\newunicodechar{≔}{\ensuremath{\coloneq}}
\newunicodechar{∷}{\ensuremath{::}}
\newunicodechar{∶}{\ensuremath{:}}
\newunicodechar{𝓐}{\ensuremath{\mathcal{A}}}
\newunicodechar{ℬ}{\ensuremath{\mathcal{B}}}
\newunicodechar{𝓢}{\ensuremath{\mathcal{S}}}
\newunicodechar{ε}{\ensuremath{\varepsilon}}
\newunicodechar{∈}{\ensuremath{\in}}

\newunicodechar{∧}{\ensuremath{\land}}
\newunicodechar{∨}{\ensuremath{\lor}}
\newunicodechar{⦇}{\ensuremath{\llparenthesis}}
\newunicodechar{⟨}{\ensuremath{\langle}}
\newunicodechar{⟩}{\ensuremath{\rangle}}
\newunicodechar{⦃}{\ensuremath{\lBrace}}
\newunicodechar{⦄}{\ensuremath{\rBrace}}
\newunicodechar{≥}{\ensuremath{\geq}}
\newunicodechar{∎}{\ensuremath{\blacksquare}}


\newunicodechar{∘}{\ensuremath{\circ}}
\newunicodechar{⊥}{\ensuremath{\bot}}
\newunicodechar{⊤}{\ensuremath{\top}}
\newunicodechar{≢}{\ensuremath{\not\equiv}}
\newunicodechar{≌}{\ensuremath{\cong}}
\newunicodechar{₀}{\textsubscript{0}}
\newunicodechar{₁}{\textsubscript{1}}
\newunicodechar{₂}{\textsubscript{2}}
\newunicodechar{₃}{\textsubscript{3}}
\newunicodechar{₄}{\textsubscript{4}}

\newunicodechar{ℓ}{\ensuremath{\ell}}

\newunicodechar{ₗ}{\textsubscript{l}}
\newunicodechar{ᵣ}{\textsubscript{r}}
\newunicodechar{ₙ}{\textsubscript{n}}
\newunicodechar{ₛ}{\textsubscript{s}}
\newunicodechar{ₒ}{\textsubscript{o}}
\newunicodechar{ₐ}{\textsubscript{a}}
\newunicodechar{ₓ}{\textsubscript{x}}

\newunicodechar{ᵗ}{\textsuperscript{t}}
\newunicodechar{ᶠ}{\textsuperscript{f}}
\newunicodechar{⁺}{\textsuperscript{+}}

\newunicodechar{¹}{\textsuperscript{1}}
\newunicodechar{²}{\textsuperscript{2}}
\newunicodechar{³}{\textsuperscript{3}}
\newunicodechar{⁴}{\textsuperscript{4}}

% For some classical style semantic stuff

\newcommand{\ruleref}[1]{\text{   }\{#1\}}
\newcommand{\refp}[1]{(\ref{#1})}
% Sets a number on the end of align* reows
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

\newcommand{\letC}[0]{\textit{let }}
\newcommand{\Assume}[0]{\textit{Assume }}

\newcommand{\Natural}[1]{\mathbb{N} \llbracket #1 \rrbracket}
\newcommand{\Bool}[1]{\mathbb{B}\llbracket #1 \rrbracket}

\newcommand{\arithSem}[2]{\mathcal{A}\llbracket #1 \rrbracket #2 }
\newcommand{\boolSem}[2]{\mathcal{B}\llbracket #1 \rrbracket #2 }
\newcommand{\Aexp}[0]{\textit{Aexp}}
\newcommand{\Bexp}[0]{\textit{Bexp}}
\newcommand{\State}[0]{\textit{State}}



\newcommand{\ruleEqv}[1]{\stackrel{\mathclap{\tiny\mbox{\text{   #1   }}\normalfont}}{\Leftrightarrow}}
\newcommand{\defeqv}[0]{\ruleEqv{def}}

\newcommand{\ruleRightarrow}[1]{\stackrel{\mathclap{\tiny\mbox{\text{   #1   }}\normalfont}}{\Longrightarrow}}

% True and false in 'while'
\newcommand{\trueW}[0]{\text{true}}
\newcommand{\falseW}[0]{\text{false}}

% True and false in 'logig'
\newcommand{\trueL}[0]{\textbf{tt}}
\newcommand{\falseL}[0]{\textbf{ff}}

% Free variables functio
\newcommand{\FV}[1]{\text{FV}(#1)}

% While
\newcommand{\eqW}[2]{#1 = #2}
\newcommand{\leqW}[2]{#1 \leq #2}
\newcommand{\subW}[2]{#1 - #2}
\newcommand{\assignW}[2]{#1 \text{:=} #2}
\newcommand{\skipW}[0]{\textit{skip}}
\newcommand{\composeW}[2]{#1 \text{;} #2}
\newcommand{\ifW}[3]{\textit{if } #1 \textit{ then } #2 \textit{ else } #3}
\newcommand{\whileW}[2]{\textit{while } #1 \textit{ do } #2}


% while extention
\newcommand{\readW}[1]{\textit{read } #1}
\newcommand{\writeW}[1]{\textit{write } #1}
\newcommand{\abortW}{\textit{abort}}
\newcommand{\threadW}[1]{\textit{thread } #1 \textit{ end}}
\newcommand{\untilW}[2]{ \textit{repeat } #1 \textit{ until } #2}


% Semantic pair
\newcommand{\SemPair}[2]{\langle #1 \text{, } #2 \rangle}
%Natural semantics
\newcommand{\NatSem}[3]{\langle #1 \text{, } #2 \rangle \rightarrow #3}
\newcommand{\StateMap}[2]{#1[#2]}

\newcommand{\ift}{\textit{, if }}


% Agda shortcuts


\newcommand{\AIC}[1]{\AgdaInductiveConstructor{#1}}
\newcommand{\AD}[1]{\AgdaDatatype{#1}}
\newcommand{\AB}[1]{\AgdaBound{#1}}
\newcommand{\AN}[1]{\AgdaNumber{#1}}
\newcommand{\AFn}[1]{\AgdaFunction{#1}}
\newcommand{\AFi}[1]{\AgdaField{#1}}
\newcommand{\AS}[1]{\AgdaSymbol{#1}}
\newcommand{\NSSimple}[3]{\AFn{⟨} \AB{#1} \AFn{,} \AB{#2} \AFn{⟩} \AFn{⇾} \AB{#3}}

\newcommand{\AST}[2]{\AFn{⟨} #1 \AFn{,} #2 \AFn{⟩}}

\newcommand{\ANS}[3]{\AST{#1}{#2} \AFn{⇾} #3}
\newcommand{\ASOS}[3]{\AST{#1}{#2} \AFn{⇒} #3}
\newcommand{\ADS}[3]{\AST{#1}{#2} \AFn{↠} #3}
\newcommand{\ADSF}[2]{\AFn{S⟦} #1 \AFn{⟧} #2}
\newcommand{\ASOSD}[3]{\AST{#1}{#2} \AFn{⇒⁺} #3}
\newcommand{\Awhile}[2]{\AIC{while} #1 \AIC{run} #2}
\newcommand{\AwhileS}[2]{\AIC{while} \AB{#1} \AIC{run} \AB{#2}}
\newcommand{\Aif}[3]{\AIC{if'} #1 \AIC{then} #2 \AIC{else} #3}


\newcommand{\ABoolSem}[2]{\AFn{ℬ⟦} #1 \AFn{⟧} #2}
\newcommand{\ABoolSemT}[2]{\AFn{ℬ⟦} #1 \AFn{⟧} #2 \AFn{≡} \AIC{true}}
\newcommand{\ABoolSemF}[2]{\AFn{ℬ⟦} #1 \AFn{⟧} #2 \AFn{≡} \AIC{false}}


\title{Formalized semantics of a toy imperative language in Agda \\ (draft)}

\author{Tomas Möre, tomas.o.more@gmail.com}

\begin{document}

\maketitle
\tableofcontents
\newpage

\begin{code}[hide]
{-# OPTIONS --sized-types #-}
module Main where
open import Prelude hiding (guard ; force)
open import Prelude.Extras
\end{code}



\section{Introduction}

Today there exists many tools to for formal semantic analysis, but many of these
tools utilize classical logic in systems that are foremost intended as tools for
verification. Agda (and similar languages) have become of great interest to me
lately as the approach they take have an appealing nature to the programmer side
of me. However, today there is a severe lack of "real" work done in Agda for the
purposes of formal analysis of imperative programming languages. Since I was
curious about the experience of writing programming analysis in a language like
Agda I decided to start a hobby project of formalizing the concepts of a book
called \citetitle{nielson1992semantics} by \citeauthor{nielson1992semantics}
\cite{nielson1992semantics} which informally describes and reasons about the
semantics and properties of a small toy imperative language by the name of
\textit{While}.

After finishing the project I got a request to do a write-up about the
experience, this document is intended to be such a write-up. Since it is hard to
abstractly share the experience I decided to do a walk through of the
formalization step by step such that those interested can follow my though
process and draw their own conclusions. Consequently this may be considered
somewhat of a tutorial for writing similar projects in Agda or similar
languages.

Before going into the work I wish to give the reader a few disclaimers.
\begin{itemize}

\item This is not an academic paper I will not give any introduction to Agda
  or the kind of logic. Before reading this you should be familiar with Agda.

\item While I will be detailed, I will not explain every utility lemma and
logical step. That said, I will initially give full explanations of some
definitions and proofs where I saw fit.

\item Even though the work here is based on a book and I will reference it
regularly I do not believe that anyone will need to have a copy of the book to
understand the presented semantics. I only base the work on the abstract ideas
from the book.

\item The source code for the work in this thesis can be found
at \url{https://gitlab.com/tmore/semwap/-/tree/master/} and the content here is
an adaptation of that code.
\end{itemize}

If you find errors, believe that something is poorly explained or have other questions, please feel free to send me message!


\subsection{Structure of the document}

The document is into sections corresponding to concepts. Section
\ref{sec:memory-state}, \ref{sec:expressions} and \ref{sec:while} should be read by all. The other
sections are more or less conceptually free standing except for proofs of
semantical equality, and occasional reference.


\subsection{Acknowledgment}

I would like to give a thanks to the people at irc \#\#agda and \#\#dependent for
answering many small questions about Agda and type theory. Special thanks to
Paolo G. Giarusso for helping me with some particularities related to the
definition of the denotational semantics.

\subsection{Overview of While}

The \textit{While} language that we will be conserved with in this document is
an extremely simple imperative language originally intended as a toy language
for learning formal analysis. While the book does define extensions to the
language, the base language we will consider here consists of variable
assignment, if-statements, while-loops and a single no-op operator and
side-effect free boolean and arithmetic languages. The entire syntax tree is
described by the following BNF taken directly from the book.

\begin{verbatim}
  a ::= n | x | a1 + a2 | a1 * a2 | a1 - a2
  b ::= true | false | a1 == a2 | a1 <= a2 | ! b | b1 && b2
  S ::= x := a | skip | S1 ; S2 | if b then S1 else S2 | while b do S
\end{verbatim}

Here the arithmetic expression works on the domain of the integers (\AB{ℤ}).

It should be clear that the language is Turing complete but lacks the features
considered needed for "real" languages.


\section{Memory State}\label{sec:memory-state}

The definition of the memory state is the most fundamental definition when
reasoning about imperative languages as they are, by definition, sequences of
instructions that modifies some memory state. In practice, machine states can be
complicated by many factors, (e.g. limited memory, memory modifications,
processor registers, memory mappings e.tc.) but the While language that we will
focus on is only concerned with a simple state that is capable of storing and
retrieving values of some kind. The book defines the state as a function $State
= Var → ℤ$ where $ℤ$ is the integers, the definition of $Var$ is left
ambiguous. The exact representation of a Var could be many things, e.g. string,
but for simplicity we will here simply define variables to be the natural
integers. For syntactical clarity, we define type synonyms for variables and
values.

\begin{code}
Var : Set
Var = Nat

Val : Set
Val = Int
\end{code}
As for the definition of state itself, it should favorably be implemented as a
kind of type class with an object, two functions (get and set) and the properties
one would expect from a state. Such a definition would allow the following
definitions to be more generic, however such an abstract representation would
make all of the reasoning slightly more cluttered and it was therefore
dropped. Instead, for the sake of it I decided to encode the concept of a state
as a list of pairs of variables and values. This definition slightly complicates
the following definitions but will have no consequence later on.
\begin{code}
State : Set
State = List (Var × Val)
\end{code}

Having a concrete datatype for state makes some of the later proof simpler as we
may utilize Agdas computational properties.

We will utilize the fact that variables have a well defined order to create a
set like definition. Naturally we have the empty state (\AgdaFunction{ε})
represented by an empty list.
\begin{code}
ε : State
ε = []
\end{code}
We define a state setting function \AFn{stateSet} that modifies the
state by associating a given variable with a given value. The insertion process
is essentially an insertion, a la insertion sort on the variable value.
\begin{code}
stateSet : State → Var → Val → State
stateSet [] var val  = [ (var , val) ]
stateSet (t@(var' , _) ∷ ss) var val =
  case (compare var' var) of
    λ { (equal eq) → (var , val) ∷ ss
      ; (less lt) →  (t ∷ stateSet ss var val)
      ; (greater gt) → (var , val) ∷ t ∷ ss
      }
\end{code}
We also define a lookup method \AFn{lookupDefault} for retrieving
values from the list. Instead of using a default value one could of course
utilize the \AD{Maybe} type, however, such a definition if of little use for our case
and would only complicate the code (more then I already have).
\begin{code}
lookupDefault : State → Var → Val → Val
lookupDefault [] _ def = def
lookupDefault ((var' , val) ∷ ss) var def =
  if var' ==? var
  then val
  else lookupDefault ss var def
\end{code}
Now we are ready to define the main two properties of the state, namely that
first setting a variable to some value and then retrieving that value returns
the set value, and that variables disjoint from some variable being set does
not modify the original value.
\begin{code}
lookup[x]∘set[x]≡v : (s : State) (x : Var) (v : Val)
                    → lookupDefault (stateSet s x v) x 0 ≡ v
lookup[x]∘set[x]≡v [] x v
  with x == x
...| yes _ =  refl
...| no ¬eq = ⊥-elim (¬eq refl)
lookup[x]∘set[x]≡v ((x' , v') ∷ ss) x v
  with compare x' x
...| (equal eq) rewrite (==?-reflexive x) = refl
...| less lt rewrite (≢⇒==? (<⇒≢ {A = Nat} lt))
             rewrite lookup[x]∘set[x]≡v ss x v = refl
...| greater gt rewrite (==?-reflexive x) = refl

lookup[y]∘set[x]≡lookup[y] :
  (s : State) (y : Var) (x : Var) (v : Val)
  → (¬ (x ≡ y))
  → lookupDefault (stateSet s x v) y 0 ≡ lookupDefault s y 0
lookup[y]∘set[x]≡lookup[y] [] y x v ¬eq
  rewrite (≢⇒==? ¬eq) = refl
lookup[y]∘set[x]≡lookup[y] (t@(x' , v') ∷ ss) y x v ¬eq
  rewrite (≢⇒==? ¬eq)
  with compare x' x
...| equal eq rewrite eq rewrite (≢⇒==? ¬eq) = refl
...| greater gt rewrite (≢⇒==? (¬sym (<⇒≢ {A = Nat} gt)))
                rewrite (≢⇒==? ¬eq) = sub-cases
     where
       sub-cases : _ ≡ _
       sub-cases with x' ==? y
       ...| true = refl
       ...| false = refl
...| less lt rewrite (≢⇒==? (<⇒≢ {A = Nat} lt))
             rewrite lookup[y]∘set[x]≡lookup[y] ss y x v ¬eq =
     sub-cases
     where
       sub-cases : _ ≡ _
       sub-cases with x' ==? y
       ...| true = refl
       ...| false = refl


\end{code}
As a slight generalization of the concept of ``getting'', ``setting'' values, but also
allowing the syntax to be closer to that of the book, we define two unconstrained
type classes for getting and setting/substituting values
\begin{code}
record TotalIndexedContainer {a b} (A : Set a) (B : Set b) : Set (a ⊔ b) where
  field get : A → Var → B
open TotalIndexedContainer {{...}} public

record Substitutable {a b} (A : Set a) (B : Set b) : Set (a ⊔ b) where
   field _[_↦_] : A → Var → B → A
open Substitutable {{...}} public

instance
   SubstitutableStateInt : Substitutable State Int
   _[_↦_] {{SubstitutableStateInt}} state var val = stateSet state var val

   TotalIndexedContainerStateVal : TotalIndexedContainer State Int
   get {{TotalIndexedContainerStateVal}} state var = lookupDefault state var 0
\end{code}
These synonyms will be used from this point on.

\section{Arithmetic \& boolean syntax and semantics}\label{sec:expressions}
The book introduces a fairly simple form of arithmetic and boolean expression
languages without side effects. The book does not explicitly define any syntax
tree structure for these languages but here, we define the languages as such.
Since the common arithmetic and boolean symbols (\lstinline{+,-,*, true, false,&&} e.tc.) are used by agda-prelude, we define each clashing symbol with a
single quote to denote it as such.

\begin{code}
data Aexp : Set where
   𝕟 : Nat → Aexp
   𝕧 : Var → Aexp
   _+'_ : Aexp → Aexp → Aexp
   _*'_ : Aexp → Aexp → Aexp
   _-'_ : Aexp → Aexp → Aexp

\end{code}
To make the usage of the arithmetic type more pleasant we can define a few type
class instances of arithmetic expressions in agda-preludes Number, Semiring and
Subtractive type classes. The addition to the number class means that raw integer
literals can be used instead of having to write \AIC{𝕟} every time. Adding the
\AgdaDatatype{Semiring} and \AgdaDatatype{Substractive} instances means we can use the symbols for addition,
subtraction and multiplication without the single quotes.
\begin{code}
instance
  NumberAexp : Number Aexp
  Number.Constraint NumberAexp _ = ⊤
  Number.fromNat NumberAexp n = 𝕟 n

  SemiringAexp : Semiring Aexp
  zro {{SemiringAexp}} = 0
  one {{SemiringAexp}} = 1
  _+_ {{SemiringAexp}} = _+'_
  _*_ {{SemiringAexp}} = _*'_

  SubtractiveAexp : Subtractive Aexp
  _-_    {{SubtractiveAexp}}   = _-'_
  negate {{SubtractiveAexp}} n = 0 - n
\end{code}
We are now ready to define the semantic function of arithmetic expressions, this
definition is the same as table 1.1:
\begin{code}
𝓐⟦_⟧_ : Aexp → State → Val
𝓐⟦ 𝕟 n ⟧ _ = fromNat n
𝓐⟦ 𝕧 v ⟧ s = get s v
𝓐⟦ a₁ +' a₂ ⟧ s = (𝓐⟦ a₁ ⟧ s) +  (𝓐⟦ a₂ ⟧ s)
𝓐⟦ a₁ *' a₂ ⟧ s = (𝓐⟦ a₁ ⟧ s) *  (𝓐⟦ a₂ ⟧ s)
𝓐⟦ a₁ -' a₂ ⟧ s = (𝓐⟦ a₁ ⟧ s) -  (𝓐⟦ a₂ ⟧ s)
\end{code}
With the definition of arithmetic expressions and its semantic function we are
ready to define the boolean expressions and its semantic function in a similar
way as above. The following definitions are derived from table 1.2 in the
book.
\begin{code}
data Bexp : Set where
  true' : Bexp
  false' : Bexp
  _=='_ : Aexp → Aexp → Bexp
  _<='_ : Aexp → Aexp → Bexp
  !' : Bexp → Bexp
  _&&'_ : Bexp → Bexp → Bexp

ℬ⟦_⟧_ : Bexp → State → Bool
ℬ⟦ true' ⟧ _ = true
ℬ⟦ false' ⟧ _ = false
ℬ⟦ (a₁ ==' a₂) ⟧ s = ((𝓐⟦ a₁ ⟧ s) ==? (𝓐⟦ a₂ ⟧ s))
ℬ⟦ a₁ <=' a₂ ⟧ s = (𝓐⟦ a₁ ⟧ s) ≤? (𝓐⟦ a₂ ⟧ s)
ℬ⟦ !' b ⟧ s = not (ℬ⟦ b ⟧ s)
ℬ⟦ b₁ &&' b₂ ⟧ s = (ℬ⟦ b₁ ⟧ s) && (ℬ⟦ b₂ ⟧ s)
\end{code}
The book uses the mathematical symbols for the boolean expressions, I here
decided to go with the more ``traditional'' programming language style to
clearly separate code from other definitions.

The book goes on with an exercise (1.8) of proving that \AgdaFunction{ℬ⟦\_⟧\_} and \AgdaFunction{𝓐⟦\_⟧\_} are
total functions. However, this property comes for free in Agda as non-total
functions are not allowed without introducing unsafe cheats, (or potential
bugs in the termination checker).


\subsection{Properties of the expression semantics}

The book goes on with proving some properties on the semantics of arithmetic and
boolean expressions. The lemmas can be useful for some reasoning, I included
these here mostly for completeness.


\subsubsection{Free variables}
We can now define the concept of \textit{free variables}. Free variables of an
expression is defined as the set of variables present in the given
expression. In our case, we can retrieve all free variables of arithmetic and
boolean expressions by the means of depth first recursion on the syntax tree,
adding an element to the result every time a \AIC{𝕧} element is encountered, while a
set may be preferred for some, we will here utilize lists instead.
\begin{code}
aexp-fv : Aexp → List Var
aexp-fv (𝕟 n) = []
aexp-fv (𝕧 v) = [ v ]
aexp-fv (a₁ +' a₂) = (aexp-fv a₁) ++ (aexp-fv a₂)
aexp-fv (a₁ *' a₂) = (aexp-fv a₁) ++ (aexp-fv a₂)
aexp-fv (a₁ -' a₂) = (aexp-fv a₁) ++ (aexp-fv a₂)

bexp-fv : Bexp → List Var
bexp-fv true' = empty
bexp-fv false' = empty
bexp-fv (a₁ ==' a₂) = aexp-fv a₁ ++ aexp-fv a₂
bexp-fv (a₁ <=' a₂) = aexp-fv a₁ ++ aexp-fv a₂
bexp-fv (!' b) = bexp-fv b
bexp-fv (a₁ &&' a₂) = bexp-fv a₁ ++ bexp-fv a₂
\end{code}
In the book, the free variable function ``FV'' is not specified over any
particular domain. To mimic this behavior in Agda, and for programming
simplicity we define a type class ``FV'' with one function field \lstinline{fv}
that is supposed to extract the free variables of the expressions. We define two
instances of this class on the \AgdaDatatype{Aexp} and \AgdaDatatype{Bexp} types
respectively.
\begin{code}
record FV {a} (A : Set a) : Set a where
  field fv : A → List Var
open FV {{...}} public

instance
  FVAexp : FV Aexp
  fv {{FVAexp}} aexp = aexp-fv aexp

  FVBexp : FV Bexp
  fv {{FVBexp}} bexp = bexp-fv bexp
\end{code}
To reason about the free variables, it helps to define some utility properties of
the \AgdaFunction{fv} function. First we prove that the result of \AgdaFunction{fv}\AgdaSpace (\AgdaInductiveConstructor{𝕧}\AgdaSpace\AgdaBound{x}) contains \AgdaBound{x}.
\begin{code}
x∈fv[𝕧x] : (x : Var) → elem x (fv (𝕧 x)) ≡ true
x∈fv[𝕧x] x with x == x
...| yes refl = refl
...| no ¬refl = ⊥-elim (¬refl refl)
\end{code}

Here we have to pattern match on the result of \AB{x} \AFi{==} \AB{x}
\cprotect\footnote{\AFi{\_==\_} : (\AB{x} \AB{y} : \AD{A}) \AS{→} \AD{Dec}
(\AB{x} \AD{≡} \AB{y})} function comes from agda-prelude, returning an instance
of the ``decidable'' datatype that either returns a proof of equality or a proof
that equality would be absurd since Agda needs this information to normalize the
equality term.

\subsubsection{Free variables}{Exercise 1.8 \& 1.10}
The book has two exercises that the reader should verify that the above semantic functions are total, these properties are for free in Agda.

\myparagraph{Lemma 1.12}
The book states ``Let $s$ and $s'$ be two states satisfying that $s\ x = s'\ x$
for all $x$ in $FV(a)$. Then $𝓐⟦a⟧ s = 𝓐⟦a⟧ s'$.''. The Agda equivalent for this proposition would look as follows:

\begin{code}
lem-1-12 :
  (s s' : State) → (a : Aexp)
  → ((x : Var) → elem x (fv a) ≡ true → get s x ≡ get s' x)
  → 𝓐⟦ a ⟧ s ≡ 𝓐⟦ a ⟧ s'
\end{code}
Like the book, we prove this property of the arithmetic semantic by structural induction on the arithmetic expression.
\begin{code}
lem-1-12 _ _ (𝕟 _) _ = refl
\end{code}
For the case when the arithmetic expression is some constant, then the semantic
is trivially independent of the state and can is proven \AIC{refl} since \AFn{𝓐⟦} (\AIC{𝕟} \AB{n}) \AFn{⟧} \AB{s} \AD{≡} \AFn{𝓐⟦} (\AIC{𝕟} \AB{n}) \AFn{⟧} \AB{s'} normalizes to \AFi{fromNat} \AB{n} \AD{≡} \AFi{fromNat} \AB{n}.
\begin{code}
lem-1-12 _ _ (𝕧 x) ctx-equiv = ctx-equiv x  (x∈fv[𝕧x] x)
\end{code}
For the case where the arithmetic expression is (\AIC{𝕧} \AB{x}) we can utilize
the assumption that all variables \AgdaBound{x} that is elements of the free
variables of the expression have the same associated value in the states. We
only need to provide the element \AgdaBound{x} and proof that it is part of the
free variables, the proof is constructed by the previous lemma.
\begin{code}
lem-1-12 s s' (a₁ +' a₂) ctx-equiv =
   cong₂ (_+_) (lem-1-12 s s' a₁ ctx-equivₗ)
               (lem-1-12 s s' a₂ ctx-equivᵣ)
 where
   ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
   ctx-equivₗ x x∈fva₁ =
     ctx-equiv x ((elem-++-left x (aexp-fv a₁) (aexp-fv a₂) x∈fva₁))
   ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
   ctx-equivᵣ x x∈fva₂ =
     ctx-equiv x ((elem-++-right x (aexp-fv a₁) (aexp-fv a₂) x∈fva₂))
\end{code}

The case for addition is proved by induction and congruence. The goal \AFn{𝓐⟦}
\AB{a₁} \AIC{+'} \AB{a₂} \AFn{⟧} \AB{s} \AD{≡} \AFn{𝓐⟦} \AB{a₁} \AIC{+'} \AB{a₂}
\AFn{⟧} \AB{s'} normalizes to \AFn{𝓐⟦} \AB{a₁} \AFn{⟧} \AB{s} \AFi{+} \AFn{𝓐⟦}
\AB{a₂} \AFn{⟧} \AB{s} \AD{≡} \AFn{𝓐⟦} \AB{a₁} \AFn{⟧} \AB{s'} \AFi{+} \AFn{𝓐⟦}
\AB{a₂} \AFn{⟧} \AB{s'} by the definition of
\AgdaFunction{𝓐⟦\_⟧\_}. \AgdaFunction{cong₂} \cprotect\footnote{\AFn{cong₂}
\AS{:} (\AB{f} \AS{:} \AB{A} \AS{→} \AB{B} \AS{→} \AB{C}) \AS{→} \AB{x₁} \AD{≡}
\AB{x₂} \AS{→} \AB{y₁} \AD{≡} \AB{y₂} \AS{→} \AB{f} \AB{x₁} \AB{y₁} \AD{≡}
\AB{f} \AB{x₂} \AB{y₂}} can construct a proof that the additions are equivalent
given proofs that both arguments are equivalent. By implicit structural
induction we can show that this is the case, given that we prove that all free
variables of the sub-expressions are associated with the same values in both
states. These properties are given by \AgdaFunction{ctx-equivₗ} and
\AgdaFunction{ctx-equivᵣ} that produces such functions by utilizing the original
assumption, the original assumption requires a proof that the variable is part
of the parent expression. Since \AFi{fv} (\AB{a₁} \AIC{+'} \AIC{a₂}) \AD{≡}
\AFi{fv} \AB{a₁} \AFn{++} \AFi{fv} \AB{a₂} by definition we can utilize the fact
that if something is an element of either the left or right hand side of
concatenation it is an element of the concatenated list. The properties required
are proven in agda-prelude-extras as \AgdaFunction{elem-++-left}
\cprotect\footnote{\AgdaDatatype{elem-++-left : (x : A) → (l r : List A) → elem
x l ≡ true → elem x (l ++ r) ≡ true}} and \AgdaFunction{elem-++-right}
\cprotect\footnote{\AgdaDatatype{elem-++-right : (x : A) → (l r : List A) → elem
x r ≡ true → elem x (l ++ r) ≡ true}}

The cases for \AgdaInductiveConstructor{\_*'\_} and
\AgdaInductiveConstructor{\_-'\_} are nearly identical to the \AgdaInductiveConstructor{\_+'\_} case and
are left out for brevity but can be found in the source code. The only syntactic
difference is the function passed as first argument to \AgdaFunction{cong₂}

\begin{code}[hide]
lem-1-12 s s' (a₁ *' a₂) ctx-equiv =
     cong₂ (_*_) (lem-1-12 s s' a₁ ctx-equivₗ)
                 (lem-1-12 s s' a₂ ctx-equivᵣ)
   where
     ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
     ctx-equivₗ x x∈fva₁ rewrite x∈fva₁  =
       ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
     ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
     ctx-equivᵣ x x∈fva₂ =
       ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))
lem-1-12 s s' (a₁ -' a₂) ctx-equiv =
     cong₂ (_-_) (lem-1-12 s s' a₁ ctx-equivₗ)
                 (lem-1-12 s s' a₂ ctx-equivᵣ)
   where
     ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
     ctx-equivₗ x x∈fva₁ rewrite x∈fva₁  =
       ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
     ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
     ctx-equivᵣ x x∈fva₂ =
       ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))
\end{code}


\myparagraph{Lemma / Exercise 1.13}

The book gives an exercise to show the same property as lemma 1.12 for the
boolean expressions: ``Let $s$ and $s'$ be two states satisfying
that $s\ x = s'\ x$ for all $x$ in $FV(b)$. Prove that $ℬ⟦b⟧s = ℬ⟦b⟧s'$.  The
proposition and proof are similar to that of \AgdaFunction{lem-1-2}
therefore the proof is left out. But the signature looks as follows:
\begin{code}
lem-1-13 :
  (s s' : State) → (b : Bexp)
  → ((x : Var) → elem x (fv b) ≡ true → get s x ≡ get s' x)
  → ℬ⟦ b ⟧ s ≡ ℬ⟦ b ⟧ s'
\end{code}
\begin{code}[hide]
lem-1-13 _ _ true' _ = refl
lem-1-13 _ _ false' _ = refl
lem-1-13 s s' (a₁ ==' a₂) ctx-equiv =
  cong₂ (_==?_) (lem-1-12 s s' a₁ ctx-equivₗ)
                 (lem-1-12 s s' a₂ ctx-equivᵣ)
   where
     ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
     ctx-equivₗ x x∈fva₁ =
       ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
     ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
     ctx-equivᵣ x x∈fva₂ =
       ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))
lem-1-13 s s' (a₁ <=' a₂) ctx-equiv =
  cong₂ (_≤?_) (lem-1-12 s s' a₁ ctx-equivₗ)
               (lem-1-12 s s' a₂ ctx-equivᵣ)
   where
     ctx-equivₗ : (x : Var) → (elem x (fv a₁) ≡ true) → get s x ≡ get s' x
     ctx-equivₗ x x∈fva₁ =
       ctx-equiv x ((elem-++-left x (fv a₁) (fv a₂) x∈fva₁))
     ctx-equivᵣ : (x : Var) → (elem x (fv a₂) ≡ true) → get s x ≡ get s' x
     ctx-equivᵣ x x∈fva₂ =
       ctx-equiv x ((elem-++-right x (fv a₁) (fv a₂) x∈fva₂))
lem-1-13 s s' (!' b) ctx-equiv =
  cong not (lem-1-13 s s' b ctx-equiv)
lem-1-13 s s' (b₁ &&' b₂) ctx-equiv =
  cong₂ (_&&_) (lem-1-13 s s' b₁ ctx-equivₗ)
                (lem-1-13 s s' b₂ ctx-equivᵣ)
   where
     ctx-equivₗ : (x : Var) → (elem x (fv b₁) ≡ true) → get s x ≡ get s' x
     ctx-equivₗ x x∈fvb₁ =
       ctx-equiv x ((elem-++-left x (fv b₁) (fv b₂) x∈fvb₁))
     ctx-equivᵣ : (x : Var) → (elem x (fv b₂) ≡ true) → get s x ≡ get s' x
     ctx-equivᵣ x x∈fvb₂ =
       ctx-equiv x ((elem-++-right x (fv b₁) (fv b₂) x∈fvb₂))
\end{code}

\subsubsection{Substitutions}
The book defines variable substitutions of the expression syntax trees a s a
function $e [ y ↦ a]$ such that the variable $y$ in the syntax tree $e$ is
replaced by the arithmetic expression $a$. We may define the same function for
the arithmetic as follows:
\begin{code}
aexp-subst : Aexp → Var → Aexp → Aexp
aexp-subst (𝕟 n) _ _ = (𝕟 n)
aexp-subst (𝕧 v) v' exp =
  if v ==? v' then exp else (𝕧 v)
aexp-subst (a₁ +' a₂) v' exp =
  aexp-subst a₁ v' exp +' aexp-subst a₂ v' exp
aexp-subst (a₁ *' a₂) v' exp =
  aexp-subst a₁ v' exp *' aexp-subst a₂ v' exp
aexp-subst (a₁ -' a₂) v' exp =
  aexp-subst a₁ v' exp -' aexp-subst a₂ v' exp
\end{code}
To comply with the books syntax we may create an instance of \AgdaDatatype{Substitutable Aexp Aexp} as follows
\begin{code}
instance
  SubstitutableAexpNat : Substitutable Aexp Aexp
  _[_↦_] {{SubstitutableAexpNat}} = aexp-subst
\end{code}
We may do something similar to substitutions in boolean expressions. However,
the substitution is still an arithmetic expression.

\begin{code}
bexp-subst : Bexp → Var → Aexp → Bexp
bexp-subst true' _ _ = true'
bexp-subst false' _ _ = false'
bexp-subst (a₁ ==' a₂) v exp =
  (a₁ [ v ↦ exp ]) ==' (a₂ [ v ↦ exp ])
bexp-subst (a₁ <=' a₂) v exp =
  (a₁ [ v ↦ exp ]) <=' (a₂ [ v ↦ exp ])
bexp-subst (!' b) v exp =
  !' (bexp-subst b v exp)
bexp-subst (b₁ &&' b₂) v exp =
  bexp-subst b₁ v exp &&' bexp-subst b₂ v exp
\end{code}
Again, we define ans instance of \AgdaDatatype{Substitutable Bexp Aexp} for
syntactix simplicity.
\begin{code}
instance
  SubstitutableBexpNat : Substitutable Bexp Aexp
  _[_↦_] {{SubstitutableBexpNat}} = bexp-subst
\end{code}


\myparagraph{Exercise 1.14 \& Exercise 1.15}

The book gives an exercise to show the proposition ``$𝓐⟦ a [ y ↦ a₀] ⟧ s = 𝓐⟦ a
⟧ (s [ y ↦ 𝓐⟦ a₀ ⟧ s])$''. Both lemmas can be shown easily by structural
induction on the expression in a fashion similar to that of
\AgdaFunction{lem-1-12} and \AgdaFunction{lem-1-13}.

\begin{code}
lem-1-14 :
  (a : Aexp) → (y : Var) → (a₀ : Aexp) → (s : State)
  → 𝓐⟦ (a [ y ↦ a₀ ]) ⟧ s ≡ 𝓐⟦ a ⟧ (s [ y ↦ 𝓐⟦ a₀ ⟧ s ])
\end{code}

\begin{code}
lem-1-14 (𝕟 n) _ _ _ = refl
\end{code}
For the case where \AgdaBound{a} is some natural \AgdaBound{n} it is trivially
proved by \AgdaInductiveConstructor{refl}
\begin{code}
lem-1-14 (𝕧 v) y a₀ s
  with v == y
...| yes v≡y rewrite v≡y = sym (lookup[x]∘set[x]≡v s y (𝓐⟦ a₀ ⟧ s))
...| no v≢y = sym (lookup[y]∘set[x]≡lookup[y] s v y (𝓐⟦ a₀ ⟧ s) (¬sym v≢y))
\end{code}
When the expression is of type variable we simply pattern match on the result of
\AgdaBound{v} \AgdaFunction{==} \AgdaBound{y} to retrieve the two cases where
the variables are equal or not. For the case where they are equal we may
substitute \AgdaBound{v} with \AgdaBound{y} using the rewrite construct to retrieve the goal
\AgdaDatatype{𝓐⟦ a₀ ⟧ s ≡ get (stateSet s y (𝓐⟦ a₀ ⟧ s)) y} and
apply the \AgdaFunction{lookup[x]∘set[x]≡v} lemma defined earler. If they are
not equal we may apply the \AgdaFunction{lookup[y]∘set[x]≡lookup[y]} lemma.
\begin{code}
lem-1-14 (a₁ +' a₂) y a₀ s =
  cong₂ (_+_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
lem-1-14 (a₁ *' a₂) y a₀ s =
  cong₂ (_*_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
lem-1-14 (a₁ -' a₂) y a₀ s =
  cong₂ (_-_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
\end{code}
The \AgdaInductiveConstructor{\_+'\_}, \AgdaInductiveConstructor{\_*'\_} and
\AgdaInductiveConstructor{\_-'\_} are all proved similarily by structural
induction on each subterm using the \AgdaFunction{cong₂} function.

\begin{code}
lem-1-15 :
  (b : Bexp) → (y : Var) → (a₀ : Aexp) → (s : State)
  → ℬ⟦ (b [ y ↦ a₀ ]) ⟧ s ≡ ℬ⟦ b ⟧ (s [ y ↦ 𝓐⟦ a₀ ⟧ s ])
lem-1-15 true' _ _ _ = refl
lem-1-15 false' _ _ _ = refl
lem-1-15 (a₁ ==' a₂) y a₀ s =
  cong₂ (_==?_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
lem-1-15 (a₁ <=' a₂) y a₀ s =
  cong₂ (_≤?_) (lem-1-14 a₁ y a₀ s) (lem-1-14 a₂ y a₀ s)
lem-1-15 (!' b) y a₀ s =
  cong not (lem-1-15 b y a₀ s)
lem-1-15 (b₁ &&' b₂) y a₀ s =
  cong₂ (_&&_) (lem-1-15 b₁ y a₀ s) (lem-1-15 b₂ y a₀ s)
\end{code}


\section{The While language syntax}\label{sec:while}
The While language is described by the following BNF taken from section 1.2:
\begin{lstlisting}
  $S$ ::= $x$ := $a$ | skip | $S_1$ : $S_2$ | if $b$ then $S_1$ else $S_2$ | while $b$ do $S$
\end{lstlisting}
A kind of nifty side effect of Agdas mixfix notation is that we can encode the
entire syntax tree in a way such that the Agda constructors and corresponding
written code is identical. Unfortunately, some of the keywords used in the books
notation is taken by common functions in Agda libraries or are keyword, namely
``\AgdaKeyword{do}'' and ``:'' are keywords and \AgdaFunction{if\_then\_else}
are commonly defined as the if expression. We therefore conduct the following changes.
\begin{itemize}
\item The composition operator ``:'' is replaced with the utf-8 character ``\AgdaInductiveConstructor{∶}'',
\item The if-then-else expressions are written with a single apostrophe after the
  ``if'' keyword (\AgdaInductiveConstructor{if'\_then\_else\_})
\item The while expressions are written with ``run'' instead of ``do'' (\AgdaInductiveConstructor{while\_run\_})
\end{itemize}
\begin{code}
data While : Set where
  _:=_ : Var → Aexp → While
  skip : While
  _∶_ : While → While → While
  if'_then_else_ : Bexp → While → While → While
  while_run_ : Bexp → While → While

infixr 1 _∶_
infix 2 _:=_
\end{code}
For syntactic clarity we have to set the fixity levels of the expressions, to be
frank, I put an abosolute minimum effort into deciding these values can be set
to better values. Note that \AgdaKeyword{infixr} makes the
\AgdaInductiveConstructor{\_∶\_} operator right associative.


For termination reasons that will be made clear later we will require a
measurement of the ``size'' of the syntax tree. The agda-prelude-extras library
defines such a type class \AgdaDatatype{Sized} in order to de-clutter the name
space. The size of the While language syntax tree is defined as follows:
\begin{code}
instance
  SizedWhile : Sized While
  size {{SizedWhile}} (_ := _) = 1
  size {{SizedWhile}} (skip) = 1
  size {{SizedWhile}} (l ∶ r) = 1 + size l + size r
  size {{SizedWhile}} (if' _ then l else r) = 1 + size l + size r
  size {{SizedWhile}} (while _ run r) = 1 + size r
\end{code}
We say that the size of any constructor of the While syntax tree is the size of
any children plus one.

\myparagraph{Alternative extendible definition}

In the work here we will
strictly only focus on the core syntax and semantics of the while
language. However, the book has exercises and chapter that either adds
expressions to the syntax tree and / or changes the arithemtic and boolean
expressions within. In order to allow for such changes it could be possible to
encode the syntax tree with the following datatype instead.
\begin{code}
module WhileExt where
  data WhileExt (Ext : Set) (ExpA : Set) (ExpB : Set) : Set where
    _:=_ : Var → ExpA →  WhileExt Ext ExpA ExpB
    skip : WhileExt Ext ExpA ExpB
    _∶_ : WhileExt Ext ExpA ExpB
        → WhileExt Ext ExpA ExpB
        → WhileExt Ext ExpA ExpB
    if'_then_else_ : ExpB
                   → WhileExt Ext ExpA ExpB
                   → WhileExt Ext ExpA ExpB
                   → WhileExt Ext ExpA ExpB
    while_run_ : ExpB
               → WhileExt Ext ExpA ExpB
               → WhileExt Ext ExpA ExpB
    ext : Ext → WhileExt Ext ExpA ExpB
\end{code}
In such a definition \AgdaBound{Ext} is the type that should extend the syntax, \AgdaBound{ExpA}
is the type acting as arithemetic expressions and \AgdaBound{ExpB} is the type acting as
boolean expressions. The basic While language could then be encoded as:
\begin{code}
  WhileAlt : Set
  WhileAlt = WhileExt ⊥ Aexp Bexp
\end{code}




\section{Natural semantics}

Natural semantics is a class of semantics ment to emphasize the result of the
evaluation of programming language. In this section we will show the original
definition of the natural semantics of the While language, the chosen encoding
of the semantics in Agda, some concrete examples to show that the semantic rules
are viable and some lemmas and their proofs that be derived from the semantics.

\begin{alignat*}{2}
  & [ass_{ns}] && \SemPair{\assignW{x}{a}}{s} \rightarrow s[ x \mapsto \arithSem{a}{s}] \\
  & [skip_{ns}] && \SemPair{\skipW}{s} \rightarrow s \\
  & [comp] && \frac{\SemPair{S_1}{s} \rightarrow s' \qquad \SemPair{S_2}{s'} \rightarrow s''}{\SemPair{\composeW{S_1}{S_2}}{s} \rightarrow s''} \\
  & [if^\trueL_{ns}] && \frac{ \SemPair{S_1}{s} \rightarrow s'}{\SemPair{\ifW{b}{S_1}{S_2}}{s} \rightarrow s''}  \ift \boolSem{b}{s} = \trueL \\
  & [if^\falseL_{ns}] && \frac{ \SemPair{S_2}{s} \rightarrow s'}{\SemPair{\ifW{b}{S_1}{S_2}}{s} \rightarrow s''}  \ift \boolSem{b}{s} = \falseL \\
  & [while^\trueL_{ns}] && \frac{\SemPair{S}{s} \rightarrow s' \SemPair{\whileW{b}{S}}{s'} \rightarrow s''}{ \SemPair{while b do S}{s} \rightarrow s''} \ift \boolSem{b}{s} = \trueL \\
  & [while^\falseL_{ns}] && \SemPair{\whileW{b}{S}}{s} \rightarrow s \ift \boolSem{b}{s} = \falseL
\end{alignat*}

The book defines the natural semantics of While to be a transition relation
between a \textit{semantic tuple} of a \AgdaDatatype{While} statement and a
state to a terminal state. The so-called \textit{semantic tuple} is in fact just
a tuple \AgdaDatatype{While × State}, In order to make the Agda definitions
syntactically closer to that of the book I define a type synonym
\AgdaFunction{⟨\_,\_⟩} for that of tuples.
\begin{code}
⟨_,_⟩ : ∀ {n m} {A : Set n} {B : Set m} → A → B → A × B
⟨_,_⟩ A B = A , B
\end{code}
Formally the closest matching structure corresponding
would be one or a set of predicates on the same structure. However, a problem
with predicates is that they are "flat" structures that disallows induction on
the proof statement, neither do they neccisarily store information much needed
for some proofs. I have here instead taken the libery of encoding the Natural
semantic of \AgdaDatatype{While} as an inductive indexed datatype.
\begin{code}
data _⇾_ : (While × State) → State → Set where
  [assₙₛ] : ∀ {x a s} → ⟨ (x := a) , s ⟩ ⇾ (s [ x ↦ 𝓐⟦ a ⟧ s ])
  [skipₙₛ] : ∀ {s} → ⟨ skip , s ⟩ ⇾ s
  [compₙₛ] :  ∀ {S₁ S₂ s s' s''}
           → (⟨ S₁ , s ⟩ ⇾ s') → (⟨ S₂ , s' ⟩ ⇾ s'')
           → ⟨ S₁ ∶ S₂ , s ⟩ ⇾ s''
  [ifₙₛᵗᵗ] : ∀ {S₁ S₂ s s' b}
           → (ℬ⟦ b ⟧ s ≡ true) → ⟨ S₁ , s ⟩ ⇾ s'
           → ⟨ if' b then S₁ else S₂ , s ⟩ ⇾ s'
  [ifₙₛᶠᶠ] : ∀ {S₁ S₂ s s' b}
           → (ℬ⟦ b ⟧ s ≡ false)
           → ⟨ S₂ , s ⟩ ⇾ s'
           → ⟨ if' b then S₁ else S₂ , s ⟩ ⇾ s'
  [whileₙₛᵗᵗ] :
    ∀ {b S s s' s''}
    → (ℬ⟦ b ⟧ s ≡ true)
    → ⟨ S , s ⟩ ⇾ s'
    → ⟨ while b run S , s' ⟩ ⇾ s''
    → ⟨ while b run S , s ⟩ ⇾ s''
  [whileₙₛᶠᶠ] :
    ∀ {S s b}
    → (ℬ⟦ b ⟧ s ≡ false)
    → ⟨ while b run S , s ⟩ ⇾ s
\end{code}

Here each constructor are named after each rule in the transision table
above. In Agda we are free to define datatypes like this in almost whatever way
we want. If they would be unsound we simply will not be able to create an
instance of such a type.

Note that due to the inductive nature of the definition the size of a derivation
tree constructed using this type is finite, consequently, a proof
\NSSimple{S}{s}{s'} implies that the program will terminate. We can quite verify
part of this statement by showing that there exists no derivation sequence for
\AgdaInductiveConstructor{while true run skip} statements.

\begin{code}
no-inf-loopₙₛ : ∀ {s s'} → ¬ ( ⟨ while true' run skip , s ⟩ ⇾ s')
no-inf-loopₙₛ ([whileₙₛᵗᵗ] _ [skipₙₛ] p₁) = no-inf-loopₙₛ p₁
\end{code}

To somewhat ensure that the definition of natural semantics can be use to prove
some simple statements we shall solve some of the basic examples given in the
book.

\myparagraph{Example 2.1}
\quote{
  Consider the statement (\AgdaBound{z} \AgdaInductiveConstructor{:=} \AgdaBound{x} \AgdaInductiveConstructor{∶} \AgdaBound{x} \AgdaInductiveConstructor{:=} \AgdaBound{y}) \AgdaInductiveConstructor{∶} \AgdaBound{y} \AgdaInductiveConstructor{:=} \AgdaBound{z} Let $s_0$ be the state that maps all variables except $x$ and $y$ to 0 and has $s_0 x = 5$, $s_0 y = 7$.
}
We define the following abbreviations for variable and states:
\begin{code}
x y z : Var
x = 0
y = 1
z = 2
\end{code}
\begin{code}[hide]
module _ where
\end{code}
\begin{code}
  private
    s₀ s₁  : State
    s₀ = (ε [ x ↦ 5 ]) [ y ↦ 7 ]
    s₁ = s₀ [ z ↦ 5 ] [ x ↦ 7 ] [ y ↦ 5 ]
\end{code}
The proposition and proof is easily encoded with the following definition:
\begin{code}
  ex-2-1 : ⟨ ((z := 𝕧 x ∶ x := 𝕧 y) ∶ y := 𝕧 z) , s₀ ⟩ ⇾ s₁
  ex-2-1 = [compₙₛ] ([compₙₛ] [assₙₛ] [assₙₛ]) [assₙₛ]
\end{code}


\myparagraph{Exercise 2.3}
\quote{
 Consider the statement \AgdaBound{z} \AgdaInductiveConstructor{:=} \AgdaInductiveConstructor{∶} \AgdaInductiveConstructor{while} \AgdaBound{y} \AgdaInductiveConstructor{<='} \AgdaBound{x} \AgdaInductiveConstructor{run} (\AgdaBound{z} \AgdaInductiveConstructor{:=} \AgdaBound{z} \AgdaField{+} \AgdaNumber{1} \AgdaInductiveConstructor{∶} \AgdaBound{x} \AgdaInductiveConstructor{:=} \AgdaBound{x} \AgdaField{-} \AgdaNumber{y}). Construct a derivation tree for this statement when executed in a state where $x$ has the value $17$ and $y$ has the value $5$
}


\begin{code}[hide]
module _ where
\end{code}
\begin{code}
  private
    s₀ : State
    s₀ = ε [ x ↦ 17 ] [ y ↦ 5 ]

  ex-2-3 : ⟨ (z := 0 ∶ while 𝕧 y <=' 𝕧 x run (z := 𝕧 z + 1 ∶ x := 𝕧 x - 𝕧 y)) , s₀ ⟩ ⇾
            (s₀ [ x ↦ 2 ] [ y ↦ 5 ] [ z ↦ 3 ])
  ex-2-3 = [compₙₛ] [assₙₛ]
                    ([whileₙₛᵗᵗ]
                      refl
                      ([compₙₛ] [assₙₛ] [assₙₛ])
                      ([whileₙₛᵗᵗ]
                        refl
                        ([compₙₛ] [assₙₛ] [assₙₛ])
                        ([whileₙₛᵗᵗ]
                          refl
                          ([compₙₛ] [assₙₛ] [assₙₛ])
                          ([whileₙₛᶠᶠ] refl))))
\end{code}

\subsection{Properties of natural semantics}

It may be useful to reason about semantic equality of statements. The book
defines two statements \AgdaBound{S₁}, \AgdaBound{S₂} to be semantically
equivalent in the classical sense if \hbox{\NSSimple{S₁}{s}{s'}} holds if and
only if \NSSimple{S₂}{s}{s'}. Since we are not working with Cubical Agda we may
not utilize the standard \AgdaFunction{\_≡\_} equivalence definition to reason
about this kind of equivalence. I therefore define a kind of "proof equivalence"
\AgdaFunction{\_≌\_} to reason about the semantic equivalence. (Sorry, I don't
know a better name)
\begin{code}[hide]
infix 3.5 _≌_
\end{code}
\begin{code}
record _≌_ {ℓ} (A B : Set ℓ) : Set ℓ where
 constructor sem-eq
 field
   from : A → B
   to : B → A
\end{code}
A proof equivalence is simply a pair of function which is mutual inverses in the
\textit{kind} level. It is easy to see that it forms an equivalence relation.
\begin{code}
≌-refl : ∀ {ℓ} → {A : Set ℓ} → A ≌ A
≌-refl = sem-eq id id

≌-sym : ∀ {ℓ} {A B : Set ℓ} → A ≌ B → B ≌ A
≌-sym (sem-eq a→b b→a) = (sem-eq b→a a→b)

≌-trans :  ∀ {ℓ} {A B C : Set ℓ} →  A ≌ B → B ≌ C → A ≌ C
≌-trans (sem-eq a→b b→a) (sem-eq b→c c→b) = sem-eq (b→c ∘ a→b) (b→a ∘ c→b)
\end{code}
This definition of "proof" equivalence is more general needed is this
chapter. However, the more general definition will be usefull later when we wish
to show the equivalences between diffrent kinds of semantics.

Using this equivalence type we can define semantic equivalence of While expressions under natural semantics as a type synonym. \AgdaFunction{\_≌ₙₛ\_}
\begin{code}
_≌ₙₛ_ : (S₁ S₂ : While) → Set
_≌ₙₛ_ S₁ S₂ = {s s' : State} → ⟨ S₁ , s ⟩ ⇾ s' ≌  ⟨ S₂ , s ⟩ ⇾ s'
\end{code}

\myparagraph{Lemma 2.5 - Unrolling loops}

Using this lemma we can now prove that statements on the form of \AIC{while}
\AB{b} \AIC{run} \AB{S} is semantically equivalent to \AIC{if'} \AB{b}
\AIC{then} \AB{S} \AIC{∶} \AIC{while} \AB{b} \AIC{run} \AB{S} else \AIC{skip}.

\begin{code}
lem-2-5 : (b : Bexp) (S : While)
        → (while b run S) ≌ₙₛ (if' b then (S ∶ while b run S) else skip)
lem-2-5 b S =
   sem-eq
\end{code}
To show this property we have to construct the two matching the constructors \AFi{from} and \AFi{to}.
\begin{code}
      (λ { ([whileₙₛᵗᵗ] ℬ⟦b⟧s≡true ⟨S,s⟩⇾s' ⟨while[b]run[S],s'⟩⇾s'') →
             [ifₙₛᵗᵗ] ℬ⟦b⟧s≡true ([compₙₛ] ⟨S,s⟩⇾s' ⟨while[b]run[S],s'⟩⇾s'')
         ; ([whileₙₛᶠᶠ] ℬ⟦b⟧s≡false) →
             [ifₙₛᶠᶠ] ℬ⟦b⟧s≡false [skipₙₛ]
         })
\end{code}
For the first of these functions we have to do a case split on the possible
proof cases, i.e either the \AIC{[whileₙₛᵗᵗ]} or \AIC{[whileₙₛᶠᶠ]} proof
constructors. For the former we can construct the target proof by constructing
an element with the \AIC{[ifₙₛᵗᵗ]} rule utilizing the locally known property
that the boolean expression evaluates to \AIC{true} in the context by
\AB{ℬ⟦b⟧s≡true} and by the knowledge that \NSSimple{S}{s}{s'}
\ANS{\AwhileS{b}{S}}{s'}{s''} we can show that the composition and body of the
rest of the loop results in the same state as the original expression with the \AIC{[compₙₛ]} rule.
\begin{code}
      (λ { ([ifₙₛᵗᵗ] ℬ⟦b⟧s≡true ([compₙₛ] ⟨S,s⟩⇾s' ⟨while[b]run[S],s'⟩⇾s'')) →
                [whileₙₛᵗᵗ] ℬ⟦b⟧s≡true ⟨S,s⟩⇾s' ⟨while[b]run[S],s'⟩⇾s''
          ;  ([ifₙₛᶠᶠ] ℬ⟦b⟧s≡false [skipₙₛ]) →
                [whileₙₛᶠᶠ] ℬ⟦b⟧s≡false
          })
\end{code}
The second equivalence function is simply the inverse of the first.

\myparagraph{Exercise 2.6 - Composision associativity}
On a similar note to lemma 2.5 its is useful to show that the composition operator commutes under semantic equivalence.
\begin{code}
lem-2-6 : ∀ {S₁ S₂ S₃}
        → ((S₁ ∶ S₂) ∶ S₃) ≌ₙₛ (S₁ ∶ S₂ ∶ S₃)
lem-2-6 {S₁} {S₂} {S₃} =
   sem-eq (λ { ([compₙₛ] ([compₙₛ] ⟨S₁,s⟩⇾s₁ ⟨S₂,s₁⟩⇾s₂) ⟨S₃,s₂⟩⇾s₃) →
                 [compₙₛ] ⟨S₁,s⟩⇾s₁ ([compₙₛ]  ⟨S₂,s₁⟩⇾s₂ ⟨S₃,s₂⟩⇾s₃) })
          (λ { ([compₙₛ] ⟨S₁,s⟩⇾s₁ ([compₙₛ] ⟨S₂,s₁⟩⇾s₂ ⟨S₃,s₂⟩⇾s₃)) →
                 [compₙₛ] ([compₙₛ] ⟨S₁,s⟩⇾s₁ ⟨S₂,s₁⟩⇾s₂) ⟨S₃,s₂⟩⇾s₃})
\end{code}
The proof is quite trivial as it simply consists of moving the poisition of the
\AIC{[compₙₛ]} rules in the proof using the information we already have.


\myparagraph{Theorem 2-9 - Determinism of Natural semantics}

The book goes on to show that the natural semantics is deterministic, i.e For
all statement and initial states if we have two proofs \NSSimple{S}{s}{s'} and
\NSSimple{S}{s}{s''} then \AB{s} \AFn{≡} \AB{s'}.
\begin{code}
thm-2-9 : {S : While} → {s s' s'' : State}
        → ⟨ S , s ⟩ ⇾ s' → ⟨ S , s ⟩ ⇾ s''
        → s' ≡ s''
\end{code}

The book takes this moment to deeply explain the concept of structural induction
over the proof structure. We will likewise conduct the proof on the proof structure.
\begin{code}
thm-2-9 [skipₙₛ] [skipₙₛ] = refl
thm-2-9 [assₙₛ] [assₙₛ] = refl
\end{code}

The rules for \AIC{skip} and \AIC{\_:=\_} are the two base cases for the
induction as both rules has no children. Here the booth proofs are trivially
shown by \AIC{refl} as Agda is able to infer the type of the final state for us.

\begin{code}
thm-2-9 ([compₙₛ] ⟨S₁,s⟩⇾s³ ⟨S₂,s³⟩⇾s') ([compₙₛ] ⟨S₁,s⟩⇾s⁴ ⟨S₁,s⁴⟩⇾s'')
  rewrite thm-2-9 ⟨S₁,s⟩⇾s³ ⟨S₁,s⟩⇾s⁴ =
   thm-2-9 ⟨S₂,s³⟩⇾s' ⟨S₁,s⁴⟩⇾s''
\end{code}
In case for composision we gain the knowledge of an intermediate state (here called s₃/s₄) since \AB{S₁} and \AB{s} are equivalent for both cases then  we can utilize induction on the left hand argument of both proofs to retrieve a proof that \AB{s₃} \AFn{≡} \AB{s₄}. Utilising Agdas rewrite abstraction we can then induct using the right hand side of the \AIC{[compₙₛ]} proofs.
\begin{code}
thm-2-9 ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ⟨S₁,s⟩⇾s') ([ifₙₛᵗᵗ] _ ⟨S₁,s⟩⇾s'') =
  thm-2-9 ⟨S₁,s⟩⇾s' ⟨S₁,s⟩⇾s''
thm-2-9 ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ⟨S₂,s⟩⇾s') ([ifₙₛᶠᶠ] _ ⟨S₂,s⟩⇾s'') =
  thm-2-9 ⟨S₂,s⟩⇾s' ⟨S₂,s⟩⇾s''
\end{code}
Both of the \AIC{if} cases are solveable by induction on the respective child
proof-element.
\begin{code}
thm-2-9 ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true _) ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false _)
  with (trans (sym ℬ⟦b⟧≡true) ℬ⟦b⟧≡false)
...| ()
thm-2-9 ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false _) ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true _)
  with (trans (sym ℬ⟦b⟧≡true) ℬ⟦b⟧≡false)
...| ()
\end{code}
Agda is however unable to infer that both proofs must be of the same boolean
evaluation result. We may however, disregard these cases by showning that if
both \ABoolSemT{ \AB{b} } {\AB{s}} and \ABoolSemF{ \AB{b} } {\AB{s}}, then
\AIC{true} \AFn{≡} \AIC{false} which is absurd (and we may conclude anything)

\begin{code}
thm-2-9 ([whileₙₛᵗᵗ] _ ⟨S,s⟩⇾s³ ⟨while,s³⟩⇾s')
        ([whileₙₛᵗᵗ] _ ⟨S,s⟩⇾s⁴ ⟨while,s⁴⟩⇾s'')
  rewrite thm-2-9 ⟨S,s⟩⇾s³ ⟨S,s⟩⇾s⁴ =
  thm-2-9 ⟨while,s³⟩⇾s' ⟨while,s⁴⟩⇾s''
\end{code}
The case for \AIC{while} where both proofs concerns \ABoolSemT{\AB{b}} is
similar to the one for compositsion. Induction on the proof of the body gives an
equivalence of the intermediate final states of evaluating the body. Utilizing
this fact we can then induct on the proof of the remainder of the evaluation.
\begin{code}
thm-2-9 ([whileₙₛᶠᶠ] _) ([whileₙₛᶠᶠ] _) = refl
\end{code}
The case for when a loop halts is equivant is a base case and trivially provable
by \AIC{refl} as the type can be inferred by agda.
\begin{code}
thm-2-9 ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true _ _) ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false)
  with (trans (sym ℬ⟦b⟧≡true) ℬ⟦b⟧≡false)
...| ()
thm-2-9 ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false) ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true _ _)
  with (trans (sym ℬ⟦b⟧≡true) ℬ⟦b⟧≡false)
...| ()
\end{code}
Like the \AIC{if} agda can not trivially disregard the case where we are given
both \AIC{[whileₙₛᶠᶠ]} and \AIC{[whileₙₛᵗᵗ]} rules, however, we can show that
such a case is absurd by proving that this implies that \AIC{true} \AFn{≡}
\AIC{false}.


\myparagraph{The (disregarded) Semantic function $Sₙₛ$}

At this point the book defines a s semantic function \AFn{Sₙₛ} \AB{:} \AD{While}
\AB{→} (\AD{State} \AS{↪} \AD{State}) where \AS{↪} signifies a \textit{partial
function} from State to State. Such that if \NSSimple{S}{s}{s'} then \AFn{Sₙₛ}
\AB{S} \AB{s} \AFn{≡} \AB{s'} else undefined. The problem with such a definition
is that is, by nature, unformal non-constructive. While it could be possible to
define an Agda predicate enforcing a similar constraint on functions, there
really are no point in doing so in a constructive context. Later when we define
denotational semantics we will construct a concrete function that follows this
constraint.


\myparagraph{Automation of semantic proofs}

Going on a tangent to the presentation of the semantics; To some it may be
interesting to see that it is possible to create proof automation tools inside
of Agda that is capable of automatically finding proofs for many easy but
bothersome cases. As an example of this I have created a small While interpreter
capable of working as a \textit{macro} inside of Agda that is capable of
constructing natural semantic derivations for any (terminating)
configuration where the initial program and state is fully known. The function
defined here is in its core a simple While inerpreter that constructs the
necessary derivation rule for every expression. In order to make the tactic
itself terminating we perform recursion on a given natural number giving the
maximal possible evaluation depth. In case this number ever reaches zero the
tactic will fail.

\begin{code}[hide]
module ComputeTactic where
  open import Tactic.Reflection
\end{code}

\begin{code}
  compute-ns' : Nat → While → State → Maybe (Term × State)
  compute-ns' 0 _ _ = nothing
  compute-ns' (suc n) skip s = just ((con (quote [skipₙₛ]) []), s)
  compute-ns' (suc n) (x := a) s = just ((con (quote [assₙₛ]) []) , s [ x ↦ 𝓐⟦ a ⟧ s ])
  compute-ns' (suc n) (S₁ ∶ S₂) s = do
    (S₁-p , s' ) ← compute-ns' n S₁ s
    (S₂-p , s'' ) ← compute-ns' n S₂ s'
    just ((con₂ (quote [compₙₛ]) S₁-p S₂-p) , s'')
  compute-ns' (suc n) (if' b then S₁ else S₂) s
    with inspect (ℬ⟦ b ⟧ s)
  ...| true with≡ ℬ⟦b⟧s≡true = do
     (S₁-p , s') ← compute-ns' n S₁ s
     just ( (con₂ (quote [ifₙₛᵗᵗ]) (con₀ (quote refl)) S₁-p) , s')
  ...| false with≡ ℬ⟦b⟧s≡false = do
     (S₂-p , s') ← compute-ns' n S₂ s
     just ( (con₂ (quote [ifₙₛᶠᶠ]) (con₀ (quote refl)) S₂-p) , s')
  compute-ns' (suc n) (while b run S) s
    with inspect (ℬ⟦ b ⟧ s)
  ...| true with≡ ℬ⟦b⟧s≡true = do
     (S-p , s') ← compute-ns' n S s
     (while-p , s'') ← compute-ns' n (while b run S) s'
     just ( (con₃ (quote [whileₙₛᵗᵗ]) (con₀ (quote refl)) S-p while-p) , s'')
  ...| false with≡ ℬ⟦b⟧s≡false = do
     just (con₁ (quote [whileₙₛᶠᶠ]) (con₀ (quote refl)) , s)
\end{code}
In order to utilize this function during compile time all we have to do is to
define a \AgdaKeyword{macro} that interprets the goal and forwards it to the
\AFn{compute-ns'} function, if possible.
\begin{code}
  macro
    compute-ns : Nat → Tactic
    compute-ns n goal = do
      goalTerm <- inferType goal
      goalTerm' <- normalise goalTerm
      case goalTerm' of
       λ { (def (quote _⇾_)
                ((arg _ (con (quote _,_)
                        (_ ∷ _ ∷ _ ∷ _ ∷ arg _ S' ∷ arg _ s' ∷ []))) ∷ _)) → do
                        S ← unquoteTC S'
                        s ← unquoteTC s'
                        case (compute-ns' n S s) of
                           λ { (just (p , _)) → unify goal p
                             ; nothing → typeErrorS "Exeeded execution depth"
                             }
         ;  _ -> typeErrorS "Expected natural semantic goal"
         }
\end{code}

To examplify that it works we use the \AgdaMacro{compute-ns} to prove
exercise 2.3 again.
\begin{code}[hide]
  private
    s₀ : State
    s₀ = ε [ x ↦ 17 ] [ y ↦ 5 ]
\end{code}
\begin{code}
  ex-2-3' : ⟨ (z := 0 ∶ while 𝕧 y <=' 𝕧 x run (z := 𝕧 z + 1 ∶ x := 𝕧 x - 𝕧 y)) , s₀ ⟩ ⇾
            (s₀ [ x ↦ 2 ] [ y ↦ 5 ] [ z ↦ 3 ])
  ex-2-3' = compute-ns 10
\end{code}
While this exact tactic is only a toy example of interest for simple examples
useful similar \textit{tactics} is not much harder to define.



\section{Structural operational semantics}

\textit{Structural operational semantics} or sometimes called \textit{small-step
semantics} are definition of semantics that focus on the every step along the
way of a program evaluation instead of just focusing on the goal The book
defines the semantics two relations: One transition relation $\SemPair{S}{s}
\Rightarrow \gamma$ where $\gamma ∈ (While \times State) \cup State$ meant to
describe the smallest step in the evaluation sequence and a relation $\gamma_0
\Rightarrow^i \gamma_i = \gamma_0 \Rightarrow \gamma_1 \Rightarrow^(i - 1)
\gamma_i$ meant to capture repeated application of the evaluation on some
semantic tuple (possibly infinite).

\begin{alignat*}{2}
  & [ass_{sos}] && \SemPair{\assignW{x}{a}}{s} \Rightarrow s[ x \mapsto \arithSem{a}{s}] \\
  & [skip_{sos}] && \SemPair{\skipW}{s} \Rightarrow s \\
  & [comp^1_{sos}] && \frac{\SemPair{S_1}{s} \Rightarrow \SemPair{S'_1}{s'}}{\SemPair{\composeW{S_1}{S_2}}{s} \Rightarrow \SemPair{\composeW{S'_1}{S_2}}{s'}} \\
  & [comp^2_{sos}] && \frac{\SemPair{S_1}{s} \Rightarrow s'}{\SemPair{\composeW{S_1}{S_2}}{s} \Rightarrow \SemPair{S_2}{s'}} \\
  & [if^\trueL_{sos}] && \SemPair{\ifW{b}{S_1}{S_2}}{s} \Rightarrow \SemPair{S_1}{s} \ift \boolSem{b}{s} = \trueL \\
  & [if^\falseL_{sos}] && \SemPair{\ifW{b}{S_1}{S_2}}{s} \Rightarrow \SemPair{S_2}{s} \ift \boolSem{b}{s} = \falseL \\
  & [while_{sos}] && \SemPair{\whileW{b}{S}}{s} \Rightarrow \SemPair{\ifW{b}{\composeW{S}{\whileW{b}{S}}}{\skipW}}{s}
\end{alignat*}

Similar to the natural semantic we can encode the step semantic by the means of
an inductive indexed data type. In the book the result of $\Rightarrow$ is the
disjoint union of the semantic tuples and state, represented by the \AD{Either}
\AB{A} \AB{B} type that has two constructors \AIC{left} and \AIC{right}. We say
that the \textit{left} type \AB{A} is that of \AD{State} and the \textit{right}
is of type \AD{While} \AFn{×} \AD{State}. Such that the \AIC{left} signifies a
terminal state.
\begin{code}
data _⇒'_ : (While × State) → (Either State (While × State)) → Set where
  [assₛₒₛ] : ∀ {x a s} → ⟨ x := a , s ⟩ ⇒' left (s [ x ↦ 𝓐⟦ a ⟧ s ])
  [skipₛₒₛ] : ∀ {s} → ⟨ skip , s ⟩ ⇒' left s
  [comp₁ₛₒₛ] : ∀ {S₁ S₁' S₂ s s'}
             → ⟨ S₁ , s ⟩ ⇒' right ⟨ S₁' , s' ⟩
             → ⟨ S₁ ∶ S₂ , s ⟩ ⇒' right ⟨ S₁' ∶ S₂ , s' ⟩
  [comp₂ₛₒₛ] : ∀ {S₁ S₂ s s'}
             → ⟨ S₁ , s ⟩ ⇒' left s'
             → ⟨ S₁ ∶ S₂ , s ⟩ ⇒' right ⟨ S₂ , s' ⟩
  [ifₛₒₛᵗᵗ] : ∀ {b S₁ S₂ s}
            → ℬ⟦ b ⟧ s ≡ true
            → ⟨ if' b then S₁ else S₂ , s ⟩ ⇒' right ⟨ S₁ , s ⟩
  [ifₛₒₛᶠᶠ] : ∀ {b S₁ S₂ s}
            → ℬ⟦ b ⟧ s ≡ false
            → ⟨ if' b then S₁ else S₂ , s ⟩ ⇒' right ⟨ S₂ , s ⟩
  [whileₛₒₛ] : ∀ {b S s}
            → ⟨ while b run S , s ⟩ ⇒'
               right ⟨ if' b then (S ∶ while b run  S) else skip , s ⟩
\end{code}

For some syntactic beautification we define a the following type-class such that
we can syntactically ignore the \AIC{left} and \AIC{right} constructors in the
type level.
\begin{code}

record SOSStep (A B : Set) : Set₁ where
  infix 4 _⇒_
  field
    _⇒_ : A → B → Set
open SOSStep {{...}} public



instance
  SOSStepLeft : SOSStep (While × State) State
  _⇒_ {{SOSStepLeft}} Ss  γ = Ss ⇒' left γ

  SOSStepRight : SOSStep (While × State) (While × State)
  _⇒_ {{SOSStepRight}} Ss  S's' = Ss ⇒' right S's'

  SOSStepEither : SOSStep (While × State) (Either State (While × State))
  _⇒_ {{SOSStepEither}} Ss  γ = Ss ⇒' γ
\end{code}


As for the so called derivation sequences, I decided to encode them slightly
differently. First of all the book uses a notation on the form \AB{γ₀} \AFn{⇒}
\AB{γ₁} \AFn{⇒} \AB{γ₂}... to signify the signify the propositional state of
every step in the derivation sequence. We could create a specific type for such
explicit sequences. However, there's little point in doing so as we may utilize
the slightly more verbose pair type for a similar effect. Neither does nothing
but the most simple example proofs in the book utilize this syntax and it is
therefore ignored.



The book states that the final element of a derivation sequence needs to be a
terminal or \textit{stuck} state, I fully ignore this constraint as it gives
more flexibility in some analysis. Furthermore, I define the relation by
induction, i.e. all derivation sequences must be finite. Note, we could allow
for inifite sequences if we defined the following type by coinduction. However
I see no point for such a definition for the comming lemmas. The book gives a
syntactic number that states how many steps is taken in the derivation
sequence. While it could be possible to encode this in the types such
information is reduntent as we the value is implicitly the length of the
sequence.

\begin{code}
infixr 4 _andThen_
data _⇒⁺'_ : (While × State) → (Either State (While × State)) → Set where
  finally : ∀ {S s a}
         → ⟨ S , s ⟩ ⇒' a
         → ⟨ S , s ⟩ ⇒⁺' a
  _andThen_ : ∀ {S S' s s' a}
            → ⟨ S , s ⟩ ⇒' right ⟨ S' , s' ⟩
            → ⟨ S' , s' ⟩ ⇒⁺' a
            → ⟨ S , s ⟩ ⇒⁺' a

\end{code}
This definition structurally is similar to that of a non empty list where we say
there must allways be at least one element. We define the length of the sequence
as an instanze of the aforementioned \AD{Sized} type class.
\begin{code}
instance
  Sized⇒⁺ : ∀ {S s γ} → Sized (⟨ S , s ⟩ ⇒⁺' γ)
  size {{Sized⇒⁺}} (finally _) = 1
  size {{Sized⇒⁺}} (_ andThen tail) = 1 + size tail
\end{code}


Similarly to \AD{\_⇒'\_} we define a type-class with the field \AD{\_⇒⁺\_} just to get
syntactically get rid of the \AIC{left} and \AIC{right} constructors in the type
level
\begin{code}

record SOSDeriv (A B : Set) : Set₁ where
  infix 4 _⇒⁺_
  field
    _⇒⁺_ : A → B → Set

open SOSDeriv {{...}} public


instance
  SOSDeriv⇒⁺left : SOSDeriv (While × State) State
  _⇒⁺_ {{SOSDeriv⇒⁺left}} Ss  s = Ss ⇒⁺' left s

  SOSDeriv⇒⁺right : SOSDeriv (While × State) (While × State)
  _⇒⁺_ {{SOSDeriv⇒⁺right}} Ss  S's' = Ss ⇒⁺' right S's'

  -- Left in so we don't have to change syntax
  SOSDeriv⇒⁺either : SOSDeriv (While × State) (Either State (While × State))
  _⇒⁺_ {{SOSDeriv⇒⁺either}} Ss  γ = Ss ⇒⁺' γ
\end{code}


As a sanity check and to exemplify how to work with the small-step semantics we
will now give the proofs of some simple examples and exercises.

\myparagraph{Example 2.15}
The book exp amplifies the process of proving the following sequence. (States defined below)
\AFn{⟨} (\AB{z} \AIC{:=} \AIC{𝕧} \AB{x} \AIC{∶} \AB{x} \AIC{:=} \AIC{𝕧} \AB{y}) \AIC{∶} \AB{y} \AIC{:=} \AIC{𝕧} \AB{z}) \AFn{,} \AB{s₀} \AFn{⟩}
\AFi{⇒} \AFn{⟨} (\AB{x} \AIC{:=} \AIC{𝕧} \AB{y} \AIC{∶} \AB{y} \AIC{:=} \AIC{𝕧} \AB{z}) \AFn{,} \AB{s₁} \AFn{⟩}
\AFi{⇒} \AFn{⟨} (\AB{y} \AIC{:=} \AIC{𝕧} \AB{z}) \AFn{,} \AB{s₂} \AFn{⟩}
\AFi{⇒} \AB{s₃}

As mentioned earlier, I do not introduce a syntax capable of carpeting this
exact meaning, although i suspect it should be possible with macros. We may show
the same property using a set of separate definitions or by showing the
existence of a pair with the semantic derivation of each corresponding step.

\begin{code}
module _ where

  private
    s₀ s₁ s₂ s₃ : State
    s₀ = (ε [ x ↦ 5 ]) [ y ↦ 7 ]
    s₁ = s₀ [ z ↦ 5 ]
    s₂ = s₁ [ x ↦ 7 ]
    s₃ = s₂ [ y ↦ 5 ]

  ex-2-14 :   ⟨ ((z := 𝕧 x ∶ x := 𝕧 y) ∶ y := 𝕧 z) , s₀ ⟩ ⇒
                 ⟨ (x := 𝕧 y ∶ y := 𝕧 z) , s₁ ⟩
             × ⟨ (x := 𝕧 y ∶ y := 𝕧 z) , s₁ ⟩ ⇒
                  ⟨ (y := 𝕧 z) , s₂ ⟩
             × ⟨ (y := 𝕧 z) , s₂ ⟩ ⇒ s₃
  ex-2-14 = ([comp₁ₛₒₛ] ([comp₂ₛₒₛ] [assₛₒₛ]))
            , (([comp₂ₛₒₛ] [assₛₒₛ])
            , [assₛₒₛ])
\end{code}

However I we can easily see that the same proof (and therefore implicitly type
constraint) goes into usage of \AFi{⇒⁺} sequences.
\begin{code}
  ex-2-14' : ⟨ ((z := 𝕧 x ∶ x := 𝕧 y) ∶ y := 𝕧 z) , s₀ ⟩ ⇒⁺ s₃
  ex-2-14' = ([comp₁ₛₒₛ] ([comp₂ₛₒₛ] [assₛₒₛ]))
            andThen ([comp₂ₛₒₛ] [assₛₒₛ])
            andThen (finally [assₛₒₛ])
\end{code}

\myparagraph{Example 2.5}

To show another style in which a statement can be shown with the SOS semantics
we will show the full derivation sequence of {\AST{
\AB{y}\AIC{:=}\AN{1}\AIC{∶}
  \Awhile{\AIC{!'}(\AIC{𝕧} \AB{x} \AIC{=='} \AN{1})}
         { (\AB{y} \AIC{:=} \AIC{𝕧} \AB{y} \AFi{*} \AIC{𝕧} \AB{x} \AIC{∶} \AIC{𝕧} \AB{x} \AIC{:=} \AIC{𝕧} \AB{x} \AgdaField{-} \AN{1})}
}
{s}}

\begin{code}[hide]
module _ where
\end{code}
\begin{code}
  private
    s₀ : State
    s₀ = (ε [ x ↦ 3 ])

    body : While
    body = y := 𝕧 y * 𝕧 x ∶ x := 𝕧 x - 1
    guard : Bexp
    guard = !' (𝕧 x ==' 1)
    whileStm : While
    whileStm = while guard run body

  ex-2-15-1 : ⟨ y := 1 ∶ whileStm , s₀ ⟩ ⇒
               ⟨ whileStm , (s₀ [ y ↦ 1 ]) ⟩
  ex-2-15-1 = [comp₂ₛₒₛ] [assₛₒₛ]

  ex-2-15-2 : ⟨ whileStm , (s₀ [ y ↦ 1 ]) ⟩ ⇒
               ⟨ if' guard then (body ∶ whileStm) else skip ,  (s₀ [ y ↦ 1 ]) ⟩
  ex-2-15-2 = [whileₛₒₛ]

  ex-2-15-3 : ⟨ if' guard then (body ∶ whileStm) else skip ,  (s₀ [ y ↦ 1 ]) ⟩ ⇒
                ⟨ body ∶ whileStm , (s₀ [ y ↦ 1 ]) ⟩
  ex-2-15-3 = [ifₛₒₛᵗᵗ] refl

  ex-2-15-4 : ⟨  body ∶ whileStm , (s₀ [ y ↦ 1 ]) ⟩ ⇒
               ⟨ x := 𝕧 x - 1 ∶ whileStm , (s₀ [ y ↦ 3 ]) ⟩
  ex-2-15-4 = [comp₁ₛₒₛ] ([comp₂ₛₒₛ] [assₛₒₛ])

  ex-2-15-5 : ⟨  x := 𝕧 x - 1 ∶ whileStm , (s₀ [ y ↦ 3 ]) ⟩ ⇒
                ⟨  whileStm , (s₀ [ y ↦ 3 ] [ x ↦ 2 ]) ⟩
  ex-2-15-5 = [comp₂ₛₒₛ] [assₛₒₛ]


  ex-2-15 : ⟨ y := 1 ∶ whileStm , s₀ ⟩ ⇒⁺ ⟨  whileStm , (s₀ [ y ↦ 3 ] [ x ↦ 2 ]) ⟩
  ex-2-15 = ex-2-15-1
            andThen ex-2-15-2
            andThen ex-2-15-3
            andThen ex-2-15-4
            andThen (finally ex-2-15-5)
\end{code}

\subsection{SOS Properties}
\myparagraph{Size not zero}

In our definition of size it should be apparent that the size of any derivation
sequence will always be greater then zero. Or similarly worded, there exists a
natural number \AB{n} such that the \AFi{size} of some derivation sequence is
the successor on \AB{n} (\AIC{suc} \AB{n}).

\begin{code}

sos-size-suc : {S : While} {s : State } {γ : Either State (While × State) }
               → (seq : ⟨ S , s ⟩ ⇒⁺ γ)
               → Σ Nat (λ n → size seq ≡ suc n)
sos-size-suc (finally _) = 0 , refl
sos-size-suc (_ andThen seq) =
  suc (fst (sos-size-suc seq))
  , cong suc (snd (sos-size-suc seq))
\end{code}


\myparagraph{Lemma 2.19 - composition destructor}

Lemma 2.19 shows that if we have a proof that a composition statement terminates
\ASOSD{\AB{S₁} \AIC{∶} \AB{S₂}}{\AB{s}}{s''}, then we can give a proof that the
left statement \AB{S₁} executed from the initial state terminates to some state
\AB{s'} (\ASOSD{\AB{S₁}}{\AB{s}}{s'}) such that the right statement \AB{S₂}
evaluated from \AB{s'} terminates to the final state \AB{s''}
(\ASOSD{\AB{S₂}}{\AB{s'}}{s''})

The book version simultaneously speaks about the lengths of the derivation
sequences stating that the length of the assumed proof is equal to the sum of
the two resulting proofs. Since we do not define the length of the statement as
part of the type we provide a separate proof of this property afterwards.

\begin{code}
lem-2-19 : {S₁ S₂ : While} {s s'' : State}
         → ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ s''
         → Σ State (λ s' → ⟨ S₁ , s ⟩ ⇒⁺ s' ×  ⟨ S₂ , s' ⟩ ⇒⁺ s'')
lem-2-19 (finally ())
\end{code}
Agda is unable to disregard the case for the \AIC{finally} constructor
automatically but is capable of concluding the absurdity of its contents.
\begin{code}
lem-2-19 (([comp₂ₛₒₛ] {s' = s'} r) andThen t) = s' , (finally r , t)
\end{code}
For the case where the proof is \AIC{[comp₂ₛₒₛ]} then we can trivially split the
proof into two parts by creating a new derivation sequence using the head and
leaving the tail as is.
\begin{code}
lem-2-19 (([comp₁ₛₒₛ] {s' = s'} r) andThen t) =
  let (s' , r' , t') = lem-2-19 t
  in s' , (r andThen r') , t'
\end{code}
If the left hand side of the composition has not been fully evaluated we may
induct on the tail and consing the resulting left hand side with the head proof.

The proofs that the length that the sum of the acquired size of the left and
right hand derivation sequences equal the size of the original statement is done
similarly by induction on the proof.

\begin{code}
lem-2-19-nat : {S₁ S₂ : While} {s s'' : State}
             → (seq : ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ s'')
             → size seq ≡ size (fst (snd (lem-2-19 seq)))
                         + size (snd (snd (lem-2-19 seq)))
lem-2-19-nat (finally ())
lem-2-19-nat (([comp₂ₛₒₛ] r) andThen t) = refl
lem-2-19-nat (([comp₁ₛₒₛ] r) andThen t) =
  cong suc (lem-2-19-nat t)
\end{code}

As a corollary to lemma 2.19 we can show that it has an inverse.
\begin{code}
lem-2-19-inv : {S₁ S₂ : While } {s s' s'' : State}
             → ⟨ S₁ , s ⟩ ⇒⁺ s'
             → ⟨ S₂ , s' ⟩ ⇒⁺ s''
             → ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ s''
lem-2-19-inv (finally x₁) x₂ = [comp₂ₛₒₛ] x₁ andThen x₂
lem-2-19-inv (x₁ andThen x₃) x₂ = ([comp₁ₛₒₛ] x₁) andThen (lem-2-19-inv x₃ x₂)
\end{code}

\myparagraph{Exercise 2.21 - compose append}
Note to self - leave out?

\begin{code}
ex-2-21 : {S₁ S₂ : While} {s s' : State}
          → ⟨ S₁ , s ⟩ ⇒⁺ left s'
          → ⟨ S₁ ∶ S₂ , s ⟩ ⇒⁺ right ⟨ S₂ , s' ⟩
ex-2-21 (finally x) = finally ([comp₂ₛₒₛ] x)
ex-2-21 (x andThen xs) = ([comp₁ₛₒₛ] x) andThen (ex-2-21 xs)
\end{code}

\myparagraph{Exercise 2.22 - SOS deterministic}

A quite important property is to show that the small-step semantics is
deterministic. We shall now shows such properties in two steps. First we shall
show that is true for one small step \ASOS{S}{s}{γ} then show that it is true
for derivation sequence terminating in some state \ASOSD{S}{s}{s'}. More
generally we could prove it to be true for all derivation sequences of equal
\AFi{size}, however, that property is unnecessary for the purposes of this
write-up.

Note that the following lemmas differs slightly from the book. Since we split
the lemma into two parts I also rename the lemmas slightly.
\begin{code}
step-deterministic :
  {S : While} {s : State} {γ γ' : Either State (While × State)}
  → ⟨ S , s ⟩ ⇒ γ → ⟨ S , s ⟩ ⇒ γ'
  → γ ≡ γ'
step-deterministic {x₁ := x₂} [assₛₒₛ] [assₛₒₛ] = refl
step-deterministic {skip} [skipₛₒₛ] [skipₛₒₛ] = refl
step-deterministic {if' x₁ then S₁ else S₂} ([ifₛₒₛᵗᵗ] x₂) ([ifₛₒₛᵗᵗ] x₃) = refl
step-deterministic {if' x₁ then S₁ else S₂} ([ifₛₒₛᵗᵗ] x₂) ([ifₛₒₛᶠᶠ] x₃) =
  ⊥-elim (true≢false (trans (sym x₂) x₃))
step-deterministic {if' x₁ then S₁ else S₂} ([ifₛₒₛᶠᶠ] x₂) ([ifₛₒₛᵗᵗ] x₃) =
  ⊥-elim (false≢true (trans (sym x₂) x₃))
step-deterministic {if' x₁ then S₁ else S₂} ([ifₛₒₛᶠᶠ] x₂) ([ifₛₒₛᶠᶠ] x₃) = refl
step-deterministic {while x₁ run S₁} [whileₛₒₛ] [whileₛₒₛ] = refl
step-deterministic {x₁ := x₂ ∶ S₂} ([comp₂ₛₒₛ] a) ([comp₂ₛₒₛ] b)
  rewrite left-inj (step-deterministic a b) = refl
step-deterministic {skip ∶ S₂} ([comp₂ₛₒₛ] a) ([comp₂ₛₒₛ] b)
  rewrite left-inj (step-deterministic a b) = refl
step-deterministic {(S₁ ∶ S₃) ∶ S₂} ([comp₁ₛₒₛ] a) ([comp₁ₛₒₛ] b)
  rewrite fst (pair-inj (right-inj (step-deterministic a b)))
  rewrite snd (pair-inj (right-inj (step-deterministic a b)))
  = refl
step-deterministic {(if' x₁ then S₁ else S₃) ∶ S₂} ([comp₁ₛₒₛ] a) ([comp₁ₛₒₛ] b)
  rewrite fst (pair-inj (right-inj (step-deterministic a b)))
  rewrite snd (pair-inj (right-inj (step-deterministic a b)))
  = refl
step-deterministic {(while x₁ run S₁) ∶ S₂} ([comp₁ₛₒₛ] a) ([comp₁ₛₒₛ] b)
  rewrite fst (pair-inj (right-inj (step-deterministic a b)))
  rewrite snd (pair-inj (right-inj (step-deterministic a b)))
  = refl
\end{code}

\begin{code}
halt-deterministic :
  {S : While} {s s' s'' : State}
  → ⟨ S , s ⟩ ⇒⁺ s' → ⟨ S , s ⟩ ⇒⁺ s''
  → s' ≡ s''
halt-deterministic {S = x₁ := x₂} (finally [assₛₒₛ]) (finally [assₛₒₛ]) = refl
halt-deterministic {S = skip} (finally [skipₛₒₛ]) (finally [skipₛₒₛ]) = refl
halt-deterministic {S = if' x₁ then S else S₁} (x₂ andThen a) (x₃ andThen b)
  rewrite fst (pair-inj (right-inj (step-deterministic x₂ x₃)))
  rewrite snd (pair-inj (right-inj (step-deterministic x₂ x₃)))
  rewrite halt-deterministic a b
  = refl
halt-deterministic {S = while x₁ run S} (x₂ andThen a) (x₃ andThen b)
  rewrite fst (pair-inj (right-inj (step-deterministic x₂ x₃)))
  rewrite snd (pair-inj (right-inj (step-deterministic x₂ x₃)))
  rewrite halt-deterministic a b
  = refl
halt-deterministic {S = S ∶ S₁} (x₃ andThen a) (x₄ andThen b)
  rewrite fst (pair-inj (right-inj (step-deterministic x₃ x₄)))
  rewrite snd (pair-inj (right-inj (step-deterministic x₃ x₄)))
  rewrite halt-deterministic a b
  = refl
\end{code}


\myparagraph{Exercise 2.20}

A small interesting exercise in the book is to show that it is not necisarily
the case that if \ASOSD{\AB{S₁} \AIC{∶} \AIC{S₂}}{\AB{s}}{\AST{S₂}{s'}} then \ASOSD{\AB{S₁}}{s}{s'}
.

\subsection{SOS semantical equivalence}

Similar to natural semantics, we may define the semantic equivalence of two
derivations of small-step semantics using the \AFn{\_≌\_} relation defined
earlier. Since \AIC{\_⇒\_} is embeddable in \AIC{\_⇒⁺\_} we shall only do define
the equivalence for \AIC{\_⇒⁺\_}. The book technically says that two states are
equivalent if they reach the same such state. However, since we defined the
small-step semantics by induction \textit{stuck} states are absurd.

\begin{code}
_≌ₛₒₛ_ : (S₁ S₂ : While) → Set
_≌ₛₒₛ_ S₁ S₂ = {s s' : State}
             → ⟨ S₁ , s ⟩ ⇒⁺ s' ≌  ⟨ S₂ , s ⟩ ⇒⁺ s'
\end{code}

And to verify that this definition makes sense we give the following example:
\begin{code}
ex-2-23a : ∀ {S} → (S ∶ skip) ≌ₛₒₛ S
_≌_.from (ex-2-23a {x₁ := x₂}) ([comp₂ₛₒₛ] [assₛₒₛ] andThen finally [skipₛₒₛ]) =
  finally [assₛₒₛ]
_≌_.from (ex-2-23a {skip}) ([comp₂ₛₒₛ] [skipₛₒₛ] andThen finally [skipₛₒₛ]) =
  finally [skipₛₒₛ]
_≌_.from (ex-2-23a {S₁ ∶ S₂}) ([comp₁ₛₒₛ] {S₁' = S₁'} x₁ andThen p) =
  x₁ andThen (_≌_.from (ex-2-23a {S₁'}) p)
_≌_.from (ex-2-23a {if' x₁ then S₁ else S₂}) ([comp₁ₛₒₛ] ([ifₛₒₛᵗᵗ] x₂) andThen p) =
  ([ifₛₒₛᵗᵗ] x₂) andThen (_≌_.from ex-2-23a p)
_≌_.from (ex-2-23a {if' x₁ then S₁ else S₂}) ([comp₁ₛₒₛ] ([ifₛₒₛᶠᶠ] x₂) andThen p) =
  ([ifₛₒₛᶠᶠ] x₂) andThen (_≌_.from ex-2-23a p)
_≌_.from (ex-2-23a {while x₁ run S}) ([comp₁ₛₒₛ] [whileₛₒₛ] andThen p) =
  [whileₛₒₛ] andThen (_≌_.from ex-2-23a p)

_≌_.to (ex-2-23a {x₁ := x₂}) (finally [assₛₒₛ]) =
  [comp₂ₛₒₛ] [assₛₒₛ] andThen finally [skipₛₒₛ]
_≌_.to (ex-2-23a {skip}) (finally [skipₛₒₛ]) =
  [comp₂ₛₒₛ] [skipₛₒₛ] andThen finally [skipₛₒₛ]
_≌_.to (ex-2-23a {S₁ ∶ S₂}) (x₁ andThen p) =
  [comp₁ₛₒₛ] x₁ andThen _≌_.to ex-2-23a p
_≌_.to (ex-2-23a {if' x₁ then S else S₁}) (x₂ andThen p) =
  [comp₁ₛₒₛ] x₂ andThen _≌_.to ex-2-23a p
_≌_.to (ex-2-23a {while x₁ run S}) (x₂ andThen p) =
  [comp₁ₛₒₛ] x₂ andThen _≌_.to ex-2-23a p

\end{code}

\subsection{An equivalence result - NS ≌ SOS}

The book technically speaks about the equivalence of the semantic functions
corresponding to the constraint of the semantics. Since we do not care about
these kinds of statements (yet) we will instead show that the two proof
structures \ANS{\AB{S}}{\AB{s}}{\AB{s'}} and \ASOSD{\AB{S}}{\AB{s}}{\AB{s'}} are
"proof" equivalent. Which will be quite useful as a lemma since it entail that
lemmas proven using one semantic type can be transported to that of the other semantic.

However, a naive approach for the case where we create a natural semantic proof
from a small-step proof does not work in Agda due to problems with the
termination checker. To solve this problem we have to show the property by
induction on the \AFi{size} of the proof instead of the proof itself. We will
therefore show this proof separately first and then apply it in the equivalence
result. The agda-prelude library provides a nice utility structure \AD{Acc} in
the module \AB{Control.WellFounded} to help the termination checker conclude
that the function is terminating.
\begin{code}
data Acc' {a} {A : Set a} (_<_ : A → A → Set a) (x : A) : Set a where
  acc' : (∀ y → y < x → Acc' _<_ y) → Acc' _<_ x
\end{code}
The \AD{Acc'} type is only defined here an example the actual one used comes
from the library.

\begin{code}
open import Control.WellFounded
SOS⇒NS-wf : ∀ {S s s'}
          → (sos : ⟨ S , s ⟩ ⇒⁺ left s')
          → (Acc _<_ (size sos))
          → (⟨ S , s ⟩ ⇾ s')
\end{code}
Here we use the \AIC{Acc} type using the \AFi{\_<\_} relation of the naturals
which has a pre-defined constructor function \AFn{wfNat} defined in the library.
\begin{code}
SOS⇒NS-wf {x := x₁} (finally [assₛₒₛ]) _ = [assₙₛ]
SOS⇒NS-wf {skip} (finally [skipₛₒₛ]) _ = [skipₙₛ]
\end{code}
If the statement \AB{S} is an assignment or \AIC{skip} then we have our
inductive base-cases base cases. For both cases the resulting natural semantics
is the corresponding natural semantic constructor.

\begin{code}
SOS⇒NS-wf {if' b then S₁ else S₂} ([ifₛₒₛᵗᵗ] ℬ⟦b⟧ andThen tail) (acc getLt) =
  [ifₙₛᵗᵗ] ℬ⟦b⟧ (SOS⇒NS-wf {S₁}
                      tail
                      (getLt (size tail)
                             (diff 0 refl)))
SOS⇒NS-wf {if' b then S₁ else S₂} ([ifₛₒₛᶠᶠ] ¬ℬ⟦b⟧ andThen tail) (acc getLt) =
  [ifₙₛᶠᶠ] ¬ℬ⟦b⟧ (SOS⇒NS-wf {S₂}
                      tail
                      (getLt (size tail)
                             (diff 0 refl)))
\end{code}

For the \AIC{if} cases we have that the guard either evaluates to \AIC{true} or
\AIC{false}, for each case we construct the new proof using the corresponding
\AIC{if} proof constructor constructing the inner derivation by induction on the
tail of the derivation sequence. Here we have provide a new \AFn{Acc} element
for the induction, which is trivial since definitionally the sizes differ by
one.

\begin{code}
SOS⇒NS-wf {while b run S} ([whileₛₒₛ] andThen tail) (acc getLt) =
  _≌_.to (lem-2-5 b S)
          (SOS⇒NS-wf tail
                  (getLt (size tail)
                         (diff 0 refl)))
\end{code}

For the \AIC{while} case the small-step semantic function transforms the
statement into an \AIC{if} statement. We can induct on the \AIC{if} statement
since the \AFn{Acc} instance will decrease in size. However the resulting
natural semantic proof does not correspond to the input type since its statement
is of type \Aif{\AB{b}}{(\AB{S} \AIC{∶} \Awhile{\AB{b}}{\AB{S}})}{\AIC{skip}}. We
did prove in lemma 2.5 that this statement is semantically equivalent to and may
utilize it to reach our goal.

\begin{code}
SOS⇒NS-wf {S₁ ∶ S₂} {s} {s''} c (acc getLt) =
  let (s' , S₁ₛₒₛ , S₂ₛₒₛ) = lem-2-19 c
      lhs = (fst (snd (lem-2-19 c)))
      rhs = (snd (snd (lem-2-19 c)))
      (n , a) = sos-size-suc lhs
      (m , b) = sos-size-suc rhs
  in [compₙₛ] (SOS⇒NS-wf {S₁} S₁ₛₒₛ
                     (getLt (size S₁ₛₒₛ)
                            (diff (size S₂ₛₒₛ - 1)
                                  (   (lem-2-19-nat c)
                                  ⟨≡⟩ add-commute (size lhs) (size rhs)
                                  ⟨≡⟩ cong (_+ (size lhs))
                                           (suc-neg (size rhs) m b))
                            )))
            (SOS⇒NS-wf {S₂} S₂ₛₒₛ
                         (getLt (size S₂ₛₒₛ)
                                (diff (size S₁ₛₒₛ - 1)
                                  (lem-2-19-nat c
                                  ⟨≡⟩  cong (_+ (size rhs))
                                           (suc-neg (size lhs) n a))
                                )))
  where
    open import Prelude.Nat.Properties
\end{code}

The composition case is the trickiest of all the cases in this proof. We utilize
lemma 2.19 In order to split the input derivation sequences to two separate
terminating proofs of both sides of the composition. With the help on induction
of these proofs we can construct the necessary parameters to the \AIC{[compₙₛ]}
constructor. However, since Agda's termination checker loses capacity to reason
about the size of these proofs we can use \AFn{lem-2-19-nat} to derive the fact
that the \AFi{size} of the respective derivation sequence is smaller then the
original.

We are now ready to prove the full equivalence relation.
\begin{code}
SOS≌NS : {S : While} {s s' : State}
        → ⟨ S , s ⟩ ⇒⁺ s' ≌ ⟨ S , s ⟩ ⇾ s'
_≌_.from SOS≌NS sos =
  SOS⇒NS-wf sos (wfNat (size sos))
\end{code}

For the case of transforming small-step semantics to natural semantics we use
the recently defined \AFn{SOS⇒NS-wf} proof applied with the well-foundedness
principle of the natural numbers.

The part that transforms natural semantics to small-step semantic proofs can be
shown without the need for auxiliary definitions

\begin{code}
_≌_.to (SOS≌NS {S = _ := _}) [assₙₛ] = finally [assₛₒₛ]
_≌_.to (SOS≌NS {S = skip}) [skipₙₛ] = finally [skipₛₒₛ]
\end{code}

Like the other uses of inductions on the natural semantic proof structure the
assignment and skip, and false case for while loops are our base cases. (While case shown later)
\begin{code}
_≌_.to (SOS≌NS {S = S₁ ∶ S₂}) ([compₙₛ] ns₁ ns₂) =
  lem-2-19-inv (_≌_.to SOS≌NS ns₁) (_≌_.to SOS≌NS ns₂)
\end{code}

The case for composition can be shown by induction on the child elements of the
derivation tree. Giving us small-step termination proofs of the evaluation of
both statements of either side of the compassion operator. We may then utilize
the inverse of lemma 2.19 to construct the proof we needed.

\begin{code}
_≌_.to (SOS≌NS {S = if' b then S₁ else S₂}) ([ifₙₛᵗᵗ] ℬ⟦b⟧ ns) =
  [ifₛₒₛᵗᵗ] ℬ⟦b⟧ andThen (_≌_.to SOS≌NS ns)
_≌_.to (SOS≌NS {S = if' b then S₁ else S₂}) ([ifₙₛᶠᶠ] ¬ℬ⟦b⟧ ns) =
  [ifₛₒₛᶠᶠ] ¬ℬ⟦b⟧ andThen (_≌_.to SOS≌NS ns)
\end{code}

Since both if cases behave the same way for the semantics we only have to
construct the proof with the corresponding \AIC{if'} proof constructor, proof of guard
evaluation and induction on the derivation of the body.

\begin{code}
_≌_.to (SOS≌NS {S = while b run S}) ([whileₙₛᵗᵗ] ℬ⟦b⟧ ns₁ ns₂) =
  [whileₛₒₛ]
  andThen [ifₛₒₛᵗᵗ] ℬ⟦b⟧
  andThen  (lem-2-19-inv (_≌_.to SOS≌NS ns₁)  (_≌_.to SOS≌NS ns₂))
_≌_.to (SOS≌NS {S = while b run S}) ([whileₙₛᶠᶠ] ¬ℬ⟦b⟧) =
  [whileₛₒₛ]
  andThen [ifₛₒₛᶠᶠ] ¬ℬ⟦b⟧
  andThen finally [skipₛₒₛ]
\end{code}

\section{Denotational semantics}


Denotational semantics is semantics defined as a function on the domain of
semantic tuples meant to give a direct relation of the functional effect of
executing some statement. The biggest challenge of defining such a function is
to handle the fact that the evaluation of a given statement may not
terminate. The book defines the denotational semantics as a partial function
that returns an \textit{undefined} value if the function does not halt, and uses
the mathematics of domain theory in order to reason about the evaluation of such
sequences. In the language of Agda and computational logics we do not have this
luxury and consequently need to create a fully defined function that is capable
of performing the same job. In order to do so we must enter the world of
\textit{coinduction}. Coinduction is the dual to structural induction in the
sense that coinduction on some object is defined by its destructors rather the
its constructors. Similarly, co-recursive functions recur on greater elements
rather then smaller elements.

Since the application of coinduction is rather unusual I will dedicate a section
to explaining the concept that will be used in the definition of denotational
semantics before presenting the definition.

\subsection{Delay - coinductive representation of non termination}

In this section we will have a look at the datatype and its utility functions,
all datatypes a and most of the proofs in this section are taken from the
\AgdaModule{Codata.Delay} module of the standard library. A few of the
properties presented here was created during the elopement of this project, I
have sent a push request to the standard library to include them.

Since the following results are not part of the main interest of this work I
will not explain everything rigorously.

I must mention that this is the first time I have worked with this concept
logically and was inspired by the paper of \citetitle{abel2014normalization} by
\cite{abel2014normalization}.

\begin{code}[hide]
module DelayEx where
  open import Size
  open import Codata.Conat using (Conat)
  import Codata.Conat as Conat
\end{code}

For cases when working with some computation that does not terminate it is
useful to use a so called \AD{Delay} monad. The delay monad is an inductive
datatype defined with two constructors \AIC{now} and \AIC{later}. The
constructor \AIC{now} symbolizes the fact that the computation has terminated
and will in such a case contain the result. The constructor \AIC{later}
symbolizes that there still are work left to be done and contains a coinductive
function that when evaluated returns \textit{the rest of the computation}. This
concept can be seen as a more explicit form lazy programming.

\begin{code}[hide]
  module ThunkEx where
\end{code}
\begin{code}
    record Thunk {ℓ} (F : Size → Set ℓ) (i : Size) : Set ℓ where
      coinductive
      field force : {j : Size< i} → F j
    open Thunk public
\end{code}
\begin{code}[hide]
  open import Codata.Thunk using (Thunk; force)
\end{code}
\begin{code}
  data Delay {ℓ} (A : Set ℓ) (i : Size) : Set ℓ where
    now   : A → Delay A i
    later : Thunk (Delay A) i → Delay A i
\end{code}
Since we do not have to evaluate the \AD{Thunk} value of the \AIC{later}
constructor during construction we are able to create an \textit{infinite}
instance of this type that never ever returns a result. To exemplify consider
the \AFn{never} function defined below.
\begin{code}[hide]
  module _ {ℓ : Level} {A : Set ℓ} where
\end{code}
\begin{code}
    never : ∀ {i} → Delay A i
    never = later λ where .force → never
\end{code}
\AFn{never} return an instance of the \AIC{later} value, but its value is a
\AD{Thunk} that evaluates to \AFn{never}. Is should be clear that evaluating the
thunk does absolutely nothing in this case.
\begin{code}
  length' : ∀ {ℓ} {A : Set ℓ} → ∀ {i} → Delay A i → Conat i
  length' (now _)   = Conat.zero
  length' (later d) = Conat.suc λ where .force → length' (d .force)
\end{code}

We may define the lengths of any delay type by counting the number of
\AIC{later} elements it produces. However, since it may be possibly (countably)
infinite in size the natural integers are not enough and we need count it using
the conatural numbers \AD{Conat}. Which is basically the natural numbers plus
infinity. We will not cover the conatural numbers in any detail here, but it is
useful to know that if we have proof that the some conatural number is finite in
size we may convert it to a corresponding natural number.


You may have noticed the extra \AB{i} parameter given to the data definitions
above, it is an element of type \AD{Size} which is meant to help Agdas
termination checker realize that definitions terminate. Since we wont explicitly
care about this type here I leave all explanations to other (more qualified)
sources found online.

\begin{code}[hide]
  module _ {ℓ : Level} {A B : Set ℓ} where
\end{code}
\begin{code}
    map-delay : (A → B) → ∀ {i} → Delay A i → Delay B i
    map-delay f (now a)   = now (f a)
    map-delay f (later d) = later λ where .force → map-delay f (d .force)

    bind : ∀ {i} → Delay A i → (A → Delay B i) → Delay B i
    bind (now a)   f = f a
    bind (later d) f = later λ where .force → bind (d .force) f
\end{code}
As mentioned earlier the \AD{Delay} type is a kind of monad, (and consequently a
functor and applicative functor). While this does not matter logically it gives
us the pleasure of using the monadic syntax.
\begin{code}[hide]
  import Relation.Unary as RU
\end{code}


--  cofix : ∀ {p} → (P : Size → Set p) → RU.∀[ Thunk P RU.⇒ P ] → RU.∀[ P ]
--  cofix f = f aux
--    module Cofix where
--    aux = λ where .force → cofix f


The \AFn{cofix} function is the main workhorse for the denotational
semantics. It will give us the greatest fix point of any \AD{Delay} function
passed to it and simultaneously making the semantics compositional. This
definition differs slightly from the one in the standard library since it turned
out that equations reasoning of pattern matching lambdas in Agda becomes
troublesome. The problem is that syntactically equivalent pattern matching
lambdas does not necessarily count as equal by \AIC{refl} since they technically
are defined as separate top level functions just printed in a certain manner,
with the definition given above we gain the capacity of referring to the
\AFn{aux} function in proofs of equality.



\begin{code}
  module _ {ℓ} {A : Set ℓ} where
\end{code}

For the purposes of this write-up we will mostly be interested about the
instances where the computation does yield a value we will now have a look at
the \AD{\_⇓} type which is the type of proofs that some \AD{Delay} instance
terminates.
\begin{code}
    data _⇓ : Delay A ∞ → Set ℓ where
      now   : ∀ a → now a ⇓
      later : ∀ {d} → d .force ⇓ → later d ⇓
\end{code}

The \AD{\_⇓} type gives two constructors similar to that of the \AD{Delay} monad
itself, however, notice that the definition is purely inductive thus describing
a finite value. Constructively we can see it as a witness that there will
eventually come a \AIC{now} value somewhere inside the structure.
\begin{code}
    extract : ∀ {d} → d ⇓ → A
    extract (now a)   = a
    extract (later d) = extract d
\end{code}
Since the \AD{\_⇓} value must necessarily know about the final value of the
computation we can define a function that extracts the value of the computation
given such a proof.

\begin{code}
  instance
    Sized⇓ : ∀ {ℓ} {P : Set ℓ} {x : Delay P ∞} → Sized (x ⇓)
    size {{Sized⇓}} (now _) = zero
    size {{Sized⇓}} (later d⇓) = suc (size d⇓)
\end{code}

It should be clear that if we have a termination proof then the proof is finite in size and we may define an instance of \AD{Sized} for it.


\begin{code}[hide]
  module _ where

\end{code}

In order to reason about the termination of some expression we must have a few
more properties of such proofs.

\begin{code}
    bind-⇓ : {ℓ : Level} {A : Set ℓ} {B : Set ℓ}
            → ∀ {m} (m⇓ : m ⇓) {f : A → Delay B ∞}
            → f (extract m⇓) ⇓ → bind m f ⇓
    bind-⇓ (now a)   fa⇓ = fa⇓
    bind-⇓ (later p) fa⇓ = later (bind-⇓ p fa⇓)
\end{code}

The first of these properties shows that if we have two termination proofs, one
that states that some \AD{Delay} instance \AB{m} terminates and one that states
that the returned value of \AB{m} applied to \AB{f}, (another \AD{Delay}
function) terminates. Then so does the resulting function from a monadic
\AFn{bind} of \AB{m} and \AB{f}

\begin{code}
  bind-⇓-injₗ :
    ∀ {a} {A B : Set a}
    → (d : Delay A ∞) {f : A → Delay B ∞}
    → bind d f ⇓ → d ⇓
  bind-⇓-injₗ (now s) foo = now s
  bind-⇓-injₗ (later s) (later foo) =
    later (bind-⇓-injₗ (force s) foo)

  bind-⇓-injᵣ :
    ∀ {a} {A B : Set a}
    → (d : Delay A ∞) {f : A → Delay B ∞}
    → (bind⇓ : bind d f ⇓)
    → f (extract (bind-⇓-injₗ d {f = f} bind⇓)) ⇓
  bind-⇓-injᵣ (now s) foo = foo
  bind-⇓-injᵣ (later s) {f} (later foo) =
    (bind-⇓-injᵣ (force s) {f = f} foo)
\end{code}

We can furthermore give a kind of inverse of the \AFn{bind-⇓} operator by
showing that it is injective (ish) in both of its arguments.

\begin{code}
  ⇓-unique : ∀ {a} → {A : Set a}
            → {d : Delay A ∞}
            → (d⇓₁ : d ⇓) → (d⇓₂ : d ⇓)
            → d⇓₁ ≡ d⇓₂
  ⇓-unique {d = now s} (now s) (now s) = refl
  ⇓-unique {d = later d'} (later l) (later r) =
    cong later (⇓-unique {d = force d'} l r)
\end{code}

We can show that all termination proofs are unique, this will be useful later on when reasoning abstractly about the semantics.
\begin{code}
  extract-now : ∀ {a} → {A : Set a} → {s : A} → (s⇓ : now s ⇓)
              → extract s⇓ ≡ s
  extract-now {s = s} s⇓@(now s') = refl
\end{code}

\AFn{extract-now} is shows that the extracted value from a termination proof of
something that yields this very instant returns the its value. This lemma could
be made reduntant by the with abstraction, however I keep it defined as the with
abstraction tend to make proofs a bit messy.

\begin{code}
  extract-bind :
    ∀ {a} → {A B : Set a}
    → {m : Delay A Size.∞} → {f : A → Delay B Size.∞}
    → (m⇓ : m ⇓) → (f⇓ : ((f (extract m⇓)) ⇓))
    → extract (bind-⇓ m⇓ {f} f⇓) ≡ extract f⇓
  extract-bind (now a) f⇓ = refl
  extract-bind (later t) f⇓ = extract-bind t f⇓
\end{code}

Another usefull property of the \AD{Delay} termination proofs showing that if
the some \AD{Delay} \AB{(f (extract m⇓))} terminates and returns a final value
then so does \AB{m} \AFi{>>=} \AB{f}.

\begin{code}
  bind-now :
      ∀ {a} {A B : Set a}
      → {d : A} {f : A → Delay B ∞}
      → (bind⇓ : (bind (now d) f ⇓))
      → (f⇓ : (f d ⇓))
      → bind⇓ ≡ f⇓
  bind-now = ⇓-unique
\end{code}

\AFn{bind-now} is a minor lemma that shows that termination proof of some
\AFn{bind} (\AIC{now} \AB{d}) \AB{f} is equivalent to a termination proof of
\AB{f}.

\begin{code}[hide]


\end{code}


\begin{code}
  bind-⇓-length-add :
      ∀ {a} {A B : Set a}
      → {d : Delay A ∞} {f : A → Delay B ∞}
      → (bind⇓ : bind d f ⇓)
      → (d⇓ : d ⇓) → (f⇓ : f (extract d⇓) ⇓)
      → size bind⇓ ≡ size d⇓ + size f⇓
  bind-⇓-length-add {f = f} bind⇓ d⇓@(now s') f⇓ =
     cong size (bind-now {d = s'} {f = f} bind⇓ f⇓)
  bind-⇓-length-add {d = d@(later dt)} {f = f}
                     bind⇓@(later bind'⇓) d⇓@(later r) f⇓ =
     cong suc (bind-⇓-length-add bind'⇓ r f⇓)
\end{code}
\AFn{bind-⇓-length-add} is a minor lemma stating that the size of a termination
proof of some \AB{bind} is equal to the size of its arguments.



\subsection{Denotational semantics - the definition}

With the definitions of the previous section we are now ready to define this
denotational semantics function itself. The function requires some mutually
recursive definitions. But we will here go through each declaration from top to
bottom.

\begin{code}[hide]
open import Prelude.Extras.Delay
open import Relation.Unary hiding (_⇒_)
\end{code}

\begin{code}
import Relation.Unary as RU

Domain : Size → Set
Domain i = State → Delay State i

mutual
  S⟦_⟧_ : ∀ {i} → While → State → Delay State i
\end{code}
\begin{code}
  S⟦ x := a ⟧  s = now (s [ x ↦ (𝓐⟦ a ⟧ s) ])
  S⟦ skip ⟧ s = now s
\end{code}

\AIC{\_:=\_} and \AIC{skip} evaluates directly to a terminal state with the same
behavior as the other semantics only wrapped in the \AD{Delay} monad.

\begin{code}
  S⟦ S₁ ∶ S₂ ⟧  s =
    S⟦ S₁ ⟧ s >>= S⟦ S₂ ⟧_
\end{code}

We define the composition operator to be the monadic composition of the left and
right elements recursively applied to the semantic function. The book uses the
normal partial function composition \AFn{∘} for the same definition however,
such a definition is impossible when working with total functions. One could
argue that it would be better to use the monad fish operator \AFi{>=>} for the
composition, however, it was not defined in the library.


\begin{code}
  S⟦ if' guard then S₁ else S₂ ⟧ s =
    if ℬ⟦ guard ⟧ s
    then S⟦ S₁ ⟧ s
    else S⟦ S₂ ⟧ s
\end{code}

The semantics of \AIC{if'} is as usual defined by the evaluation of the
corresponding branch.

\begin{code}
  S⟦ while guard run S ⟧ s =
    cofix Domain
          (λ rec s' →
             if ℬ⟦ guard ⟧ s'
             then later (whileThunk S rec s')
             else now s')
          s

  whileThunk : ∀ {i} → While → Thunk (λ j → State → Delay State j) i
           → State → Thunk (Delay State) i
  whileThunk S rec s .force =  ((S⟦ S ⟧ s) >>= force rec)
\end{code}

We define the semantics of while to be the greatest fixpoint of shown
function. Here the \AFn{whileThunk} function is defined separately from the main
semantics for the same reason as we defined another version of the \AFn{cofix}
function above. Note that these problems could be resolved if we postulated
functional extensional or theoretically if we used cubical Agda. However I do
no such there here.

Sadly, because of the problems with pattern matching lambdas we may not directly
reason about the results of the function evaluation and are instead forced to
manipulate it more indirect means. However, for some sanity checking we will now
use the \AFn{runFor} function that evaluates the \AD{Delay} result for \AB{n}
steps, returning \AD{Maybe State} as a result, such that it returns \AIC{just}
\AB{n} if it terminates within that amount of steps and \AIC{nothing} otherwise.

\begin{code}
  ex-2-2-ds : runFor 10 (S⟦ (y := 𝕟 1 ∶ while !' (𝕧 x ==' 𝕟 1)
                                        run (y := 𝕧 y *' 𝕧 x ∶ x := 𝕧 x -' 𝕟 1)) ⟧
                            (ε [ x ↦ 3 ]))
             ≡ just (ε [ x ↦ 3 ] [ y ↦ 6 ] [ x ↦ 1 ])
  ex-2-2-ds = refl
\end{code}

\myparagraph{Example 5.1}

While we are lacking the capacity of directly reasoning about the result (in the
type level) we can still figure out the fix points of functions in a style
similar to that of the book. We consider a simple exercse in the book to
demonstrate this fact.

Instead of saying that the result of the fixpoint equals some specific value we
assume that we have a termination proof of the given expression, We may then
extract all properties of the resulting state throug the termination proof
instead.

In this context we are free to reason about any properties of the initial state
and its effects on the final state. However, as we know it must not necisarily
be the case that the assumption that the program terminates is valid, in these
cases we will be able to show absurdity.

\begin{code}
  ex-5-1 : {s : State}
         → (d⇓ : (S⟦ while (!' (𝕧 x ==' 0)) run skip ⟧ s) ⇓)
         → if (get s x ==? 0)
           then (extract d⇓ ≡ s)
           else ⊥
  ex-5-1 {s} d⇓
    with inspect (get s x ==? 0)
  ...| true with≡ x==?0 rewrite x==?0 = extract-now d⇓
  ...| false with≡ ¬x==?0 rewrite ¬x==?0 with d⇓
  ...| (later d⇓') = transport (if_then (extract d⇓' ≡ s) else ⊥) ¬x==?0 (ex-5-1 d⇓')
\end{code}

The kind of reasoning put in the above explanation has one major problem, that
is it could of course be the case that the termination assumption is invalid,
and should not be used to draw any conclusions unless a concrete proof of
termination can be given. In this particular case could show the same property
of the the program in this way instead.

\begin{code}
  ex-5-1' : {s : State}
          → Either ( get s x ==? 0 ≡ true
                   × Σ ((S⟦ while (!' (𝕧 x ==' 0)) run skip ⟧ s) ⇓)
                       (λ d⇓ → extract d⇓ ≡ s))
                   (¬ ((S⟦ while (!' (𝕧 x ==' 0)) run skip ⟧ s) ⇓))
  ex-5-1' {s} with inspect (get s x ==? 0)
  ...| true with≡ x==?0 rewrite x==?0 =
         left (refl , now s , refl)
  ...| false with≡ ¬x==?0 =
       right absurd
      where
        absurd : ¬ ((S⟦ while (!' (𝕧 x ==' 0)) run skip ⟧ s) ⇓)
        absurd d⇓ rewrite ¬x==?0 with d⇓
        ...| later d⇓' = absurd d⇓'

\end{code}


\subsection{Properties of denotational semantics}

In this section we will have a look at various properties of denotational
semantics. In order to show equivalence of the denotational semantics to that of
the other semantics we have to define quite a few lemmas creating lemmas that
correspond to the construction and destruction similar to that of the natural
semantics. We will start by defining a type representing a concept similar to
natural semantics, then define a series of lemmas acting as constructors and
then a series of lemmas acting as the destructors.

In order to reason about the denotational semantics in a similar way to the
other semantics I define a relation \ADS{\AB{S}}{\AB{s}}{\AB{s'}} To symbolize,
that \ADSF{\AB{S}}{\AB{s}} \AFn{⇓} (i.e it terminates) and that the returned
value equals \AB{s'}

\begin{code}
data _↠_ : While × State → State → Set where
  semProof : ∀ {S s s'} → (d : S⟦ S ⟧ s ⇓) → extract d ≡ s' → ⟨ S , s ⟩ ↠ s'

termProof : ∀ {S s s'} → ⟨ S , s ⟩ ↠ s' → S⟦ S ⟧ s ⇓
termProof (semProof x _ ) = x

resultProof : ∀ {S s s'} → (wp : ⟨ S , s ⟩ ↠ s')
            → extract (termProof wp) ≡ s'
resultProof (semProof _ x) = x
\end{code}
This datatype could techincally be defined as a record, but I find this
definition slightly simpler.


Like for the other semantics we define an instance of the \AD{Sized} typeclass
to reason about the sie of these proofs.
\begin{code}
instance
  SizeDSSemProof : ∀ {S s s'} → Sized (⟨ S , s ⟩ ↠ s')
  size {{SizeDSSemProof}} (semProof termProof _) =
    size termProof
\end{code}
Where we say that the size of a proof is the length of the termination proof.


\myparagraph{Theorem 2.9 (again) - Determinism of denotational semantics}

Like for the other semantics we will need a utility proof to show that the
semantic is deterministic. While it must be true due to its definition it is a
usefull lemma proof wise.
\begin{code}
ds-deterministic :
  {S : While} {s s' s'' : State}
  → ⟨ S , s ⟩ ↠ s' → ⟨ S , s ⟩ ↠ s''
  → s' ≡ s''
ds-deterministic {S} (semProof Sₗ⇓ S↠s') (semProof Sᵣ⇓ S↠s'')
  rewrite ⇓-unique Sₗ⇓ Sᵣ⇓ = (sym S↠s') ⟨≡⟩ S↠s''
\end{code}
We can show this property by utizing the fact that termination proofs are unique

\subsubsection{Denotational semantic constructors}

\myparagraph{Assignment lemma}

We can show that an assignment rule much like the other semantics exists

\begin{code}
[ass] : ∀ {x a s} → ⟨ (x := a) , s ⟩ ↠ (s [ x ↦ 𝓐⟦ a ⟧ s ])
[ass] {x} {a} {s} =
  semProof (now (s [ x ↦ (𝓐⟦ a ⟧ s) ]))
          refl
\end{code}

\myparagraph{Skip lemma}

\begin{code}
[skip] : ∀ {s} →  ⟨ skip , s ⟩ ↠ s
[skip] {s} = semProof (now s) refl
\end{code}

\myparagraph{Composision lemma}

The composision rule, much like the equivalnet for natural semantics takes two
proofs: One that shows that some statement \AB{S₁} evaluated form a state \AB{s}
returns a state \AB{s'}, and one that shows that another statement \AB{S₂}
evaluated from \AB{s'} returns a state \AB{s''} and returns a proof that the
composision of these two statements evaluated from \AB{s} returns \AB{s''}

\begin{code}
[comp] :
  ∀ {S₁ S₂ s s' s''}
  → ⟨ S₁ , s ⟩ ↠ s' → ⟨ S₂ , s' ⟩ ↠ s''
  → ⟨ S₁ ∶ S₂ , s ⟩ ↠ s''
[comp] {S₁} {S₂} {s} {s'} {s''}
         (semProof S₁⇓ S₁↠s')
         (semProof S₂⇓ S₂↠s'')
  rewrite (sym S₁↠s')
  =
   semProof (bind-⇓ S₁⇓ S₂⇓)
            ( extract (bind-⇓ S₁⇓ S₂⇓) ≡⟨ extract-bind S₁⇓ S₂⇓ ⟩
              extract S₂⇓               ≡⟨ S₂↠s'' ⟩
              s''                       ∎)
\end{code}

To show this property we construct a proof where the new termination proof is
the composision of both proofs. With the goal \AFn{bind} \ADSF{\AB{S₁}}{\AB{s}}
\ADSF{\AB{S₂}}{\_} \AFn{⇓}. Since the \AFn{bind-⇓} operator requires the initial
state of the \AB{S₂} proof to be the extracted value of the first we have to
rewrite the goal using the symmetry of \AB{S₁↠s'} that shows \AB{s'} \AD{≡}
\AFn{extract} \ADSF{S₁}{s}.

We now only have to show that the extracted value of the bind does in fact
return \AB{s''}, using the \AFn{extract-bind} lemma defined earlier, we can show
that the extraction of the \AFn{bind} is equal to the extraction of
\ADSF{\AB{S₂}}{\AB{s'}} which we know is equivalent to \AB{s''} by \AB{S₂↠s''}.

\myparagraph{If lemma(s)}
For the \AIC{if'} we give two lemmas one for each case of \ABoolSem{\AB{b}}{\AB{s}}.

\begin{code}
[ifᵗᵗ] :
  ∀ {S₁ S₂ s s' b}
  → (ℬ⟦ b ⟧ s ≡ true) → ⟨ S₁ , s ⟩ ↠ s'
  → ⟨ if' b then S₁ else S₂ , s ⟩ ↠ s'
[ifᵗᵗ] {S₁} {S₂} {s} {s'} {b} ℬ⟦b⟧s≡true (semProof S₁⇓  S₁↠s')
  rewrite (sym (cong (if_then S⟦ S₁ ⟧ s else S⟦ S₂ ⟧ s) ℬ⟦b⟧s≡true)) =
  semProof S₁⇓ S₁↠s'

[ifᶠᶠ] :
  ∀ {S₁ S₂ s s' b}
  → (ℬ⟦ b ⟧ s ≡ false) → ⟨ S₂ , s ⟩ ↠ s'
  → ⟨ if' b then S₁ else S₂ , s ⟩ ↠ s'
[ifᶠᶠ] {S₁} {S₂} {s} {s'} {b} ℬ⟦b⟧s≡false (semProof S₂⇓  S₂↠s')
  rewrite (sym (cong (if_then S⟦ S₁ ⟧ s else S⟦ S₂ ⟧ s) ℬ⟦b⟧s≡false)) =
   semProof S₂⇓ S₂↠s'
\end{code}

For both cases we rewrite the goal by showing that the \AFn{if\_then\_else\_}
evaluates to the respective case with the knowledge of result of the boolean
semantics to show that the input proof is in fact equivalent to the goal.

\myparagraph{While true lemma}

Both of the \AIC{while} rules works similarily. We will go through the true case
in detail as it is the most complicated of the two. Before we look at the proof
I must apologise for its complexity, I have not found a simpler way to do the
same thing.

\begin{code}
whileSem : Bexp → While → ∀[ Thunk Domain RU.⇒ Domain ]
whileSem guard S rec s =
    if ℬ⟦ guard ⟧ s
    then later (whileThunk S rec s)
    else now s


[whileᵗᵗ] :
  ∀ {b S s s' s''}
  → (ℬ⟦ b ⟧ s ≡ true)
  → ⟨ S , s ⟩ ↠ s'
  → ⟨ while b run S , s' ⟩ ↠ s''
  → ⟨ while b run S , s ⟩ ↠ s''
[whileᵗᵗ] {b} {S} {s} {s'} {s''} ℬ⟦b⟧s≡true (semProof S⇓ S↠s') (semProof WS⇓ WS↠s'')
  rewrite (sym S↠s')
    =
    solve (later (bind-⇓ S⇓ WS⇓))
          ( extract {d = inner-recur} (later (bind-⇓ S⇓ WS⇓))
           ≡⟨ extract-bind S⇓ WS⇓ ⟩
             extract WS⇓
           ≡⟨ WS↠s'' ⟩
             s''
            ∎
           )
  where
    inner-recur : Delay State ∞
    inner-recur = later
       (whileThunk S (Cofix.aux Domain
           (λ rec s' →
             if ℬ⟦ b ⟧ s'
             then later (whileThunk S rec s')
             else now s')) s)
    reduction : (if ℬ⟦ b ⟧ s then inner-recur else now s) ≡ inner-recur
    reduction = cong (if_then inner-recur else now s) ℬ⟦b⟧s≡true
    solve : (inner-recur⇓ : inner-recur ⇓)
          → extract inner-recur⇓ ≡ s''
          → ⟨ while b run S , s ⟩ ↠ s''
    solve ir⇓ gives-s'' rewrite (sym reduction) = semProof ir⇓ gives-s''
\end{code}

In order to understand what is going on here we should fist consider the goal of
the lemma ignoring the rewrite. That is, we want to construct an element of type
\ADS{\Awhile{\AB{b}}{\AB{S}}}{\AB{s}}{\AB{s''}} constructed by the
\AIC{semProof} constructor requiring a termination proof and witness that it
extracts to the \AB{s''}. Now, only considering the termination proof, we have
the goal of constructing a proof of type \ADSF{\Awhile{\AB{b}}{\AB{S}}}{\AB{s}}
\AFn{⇓} which normalizes to \AFn{if} \ABoolSem{\AB{b}}{\AB{s'''}} \AFn{then}
\AIC{later} (\AFn{whileThunk} \AB{S} (\AgdaSymbol{λ} \AgdaSymbol{\{}
\AFi{.force} \AgdaSymbol{→} \AFn{cofix} \_ \AgdaSymbol{\}}) \AB{s}) \AFn{else}
\AIC{now} \AB{s}. Here (\_) is a recursive call left out for brevity. Observing
the inner part of the true case it we know that the \AFn{whileThunk} function is
a monadic composision of the evaluation of \AB{S} and the remainder of the
computation. The proof should then be contractible from (\AIC{later}
(\AFn{bind-⇓} \AB{S⇓} \AB{WS⇓})). However the types does not match what Agda
expects since \AB{WS⇓} speaks about \AB{s'} and the bind requires its state
argument to be equal to the extraction of the left element of the bind. We may
then however use the rewrite abstraction to change the type of \AB{s'} to that
of \AFn{extract} \AB{S⇓} by the symmetry of \AB{S↠s'} allowing the application
of the proofs to \AFn{bind-⇓}. Sadly, Agda will still not be happy about the
type since required type is wrapped in a \AFn{if} statement, which should be an
easy fix again by the rewrite. Sadly, for some god forsaken reason a top level
rewrite on the knowledge that the boolean expression evaluates to true does not
propagate to the goal in this case and we are forced to utilize other
methods. One way to achieve the goal could be to utilize the \AFn{transport}
lemma to here, we do a similar thing in the \AFn{solve} function because since
we want to do the same change to both the termination and extraction proofs.

The extraction proof is can be shown by observing that the extraction of the
bind will be equivalent to the extraction of the right hand element which we
locally know is equivalent to \AB{s''} by \AB{WS↠s''}. As mentioned above, we
must convince Agda that the proof is the same type as the goal.


\myparagraph{While false lemma}

The \AFn{[whileᶠᶠ]} is in many ways similar to that of a \AIC{skip} and the
underlying proof should be trivial. Due to technicalities with Agda it is
however a bit tricky to convince Agda that the proof is of the correct type. In
order to not repeat myself, please see the explanation of \AFn{[whileᵗᵗ]} for an explanation.

\begin{code}
[whileᶠᶠ] :
  ∀ {S s b}
  → (ℬ⟦ b ⟧ s ≡ false)
  → ⟨ while b run S , s ⟩ ↠ s
[whileᶠᶠ] {S} {s} {b} ℬ⟦b⟧s≡false =
    solve (now s)
          refl
  where
    inner-recur : Delay State ∞
    inner-recur = later
       (whileThunk S (Cofix.aux Domain
           (λ rec s' →
             if ℬ⟦ b ⟧ s'
             then later (whileThunk S rec s')
             else now s')) s)
    reduction : (if ℬ⟦ b ⟧ s then inner-recur else now s) ≡ now s
    reduction = cong (if_then inner-recur else now s) ℬ⟦b⟧s≡false
    solve : (inner-recur⇓ : now s ⇓)
          → extract inner-recur⇓ ≡ s
          → ⟨ while b run S , s ⟩ ↠ s
    solve ir⇓ gives-s'' rewrite (sym reduction) = semProof ir⇓ gives-s''
\end{code}

\subsubsection{Denotational semantic destructors}

In order to show the equivalence between the denotational semantics and the
natural semantics we will need to be able to take some denotational semantic
proof and split it into smaller parts. Since the function performing the
translation from Denotational to natural semantics needs to be terminating we
also have to show that the elements of the split is smaller then that of the
original proof. Here I only define some of the destructors as the ones left out
(i.e. \AIC{skip}, \AIC{\_:=\_}, \AIC{while\_false\_run\_}) are trivial.

\myparagraph{If destructors}

The if cases are the simplest of the bunch not requiring a lot of though, so we
will start with them. While the destructs of the other semantics gives us a case
split on the evaluation of the boolean value we will here assume that the
boolean case split is done beforehand since it gives slightly cleaner code.

\begin{code}
[ifᵗᵗ]-destr :
  {b : Bexp} {S₁ S₂ : While} {s s' : State}
  → ⟨ if' b then S₁ else S₂ , s ⟩ ↠ s' → ℬ⟦ b ⟧ s ≡ true
  → ⟨ S₁ , s ⟩ ↠ s'
[ifᵗᵗ]-destr {b} {S₁} {S₂} {s} {s'} (semProof if⇓ if→s') ℬ⟦b⟧s≡true
  rewrite ℬ⟦b⟧s≡true = semProof if⇓ if→s'

[ifᶠᶠ]-destr :
  {b : Bexp} {S₁ S₂ : While} {s s' : State}
  → ⟨ if' b then S₁ else S₂ , s ⟩ ↠ s' → ℬ⟦ b ⟧ s ≡ false
  → ⟨ S₂ , s ⟩ ↠ s'
[ifᶠᶠ]-destr {b} {S₁} {S₂} {s} {s'} (semProof if⇓ if→s') ℬ⟦b⟧s≡false
  rewrite ℬ⟦b⟧s≡false = semProof if⇓ if→s'
\end{code}

For both cases the proof of the inner expression is equivalent to that of the
original if expression since the \AIC{if} semantic definition never yields a
\AIC{later} node. Therefore, to get the final node all we have to do is rewrite
the arguments using the fact that we know the boolean expression evaluates to
some concrete value.

\myparagraph{Composision destructor}

Given, some denotational proof of a composition statement we want to be able to
split it into two proofs, one for each side of the composition operator. The
resulting statement will, dependents on some intermediate state which we also
have to extract we therefore give the result as a sigma to capture this
fact. Since we later want to be able to show that extracted components are
smaller then the original we will simultaneously show this fact. The size proof
could be done separately, but I deemed it to be simpler to show both properties
at once since a separate proof would have to be otherwise identical.

\begin{code}
[comp]-destr-and-size :
  {S₁ S₂ : While} {s s'' : State}
  → (wp : ⟨ S₁ ∶ S₂ , s ⟩ ↠ s'')
  → Σ State (λ s' →
               Σ (⟨ S₁ , s ⟩ ↠ s' × ⟨ S₂ , s' ⟩ ↠ s'')
               (λ split → size wp ≡ size (fst split) + size (snd split)))
[comp]-destr-and-size {S₁} {S₂} {s} {s''} (semProof C⇓ C↠s'') =
  s'
  , (semProof S₁⇓  refl
  , (semProof (bind-⇓-injᵣ {d = (S⟦ S₁ ⟧ s)} C⇓)
              (trans (extract-bind-⇓-injᵣ≡extract-bind-⇓
                       {d = (S⟦ S₁ ⟧ s)} C⇓)
                      C↠s'')
    ))
  , (bind-⇓-length-add C⇓ S₁⇓ (bind-⇓-injᵣ {d = (S⟦ S₁ ⟧ s)} C⇓))
  where
    S₁⇓ : S⟦ S₁ ⟧ s ⇓
    S₁⇓ = bind-⇓-injₗ C⇓
    s' : State
    s' = extract S₁⇓
\end{code}

In order to extract the left and right hand side proofs we utilize the fact that
the composition is defined by a \AD{Delay} \AFn{bind}, which we proved is
injective in its left and right hand terms. Using this fact we may retrieve a
proof (\AB{S₁⇓}) that the left hand terminates by \AFn{bind-⇓-injₗ}, this
simultaneously gives us access to the intermediate state \AB{s'} which is
defined as the extracted value of \AB{S₁⇓}. The proof
that the right hand side terminates is given by the injectivity
\AFn{bind-⇓-injᵣ}, the only problem is that Agda now does not know that the
right hand side evaluates to the terminal state \AB{s''}. Since we know that the
extracted value of the right hand side is equal to the extracted value of the
entire bind operator by \AFn{extract-bind-⇓-injᵣ≡extract-bind-⇓} we can show
that it is fact equal to \AB{s''} since we know the entire \AFn{bind} values to
\AB{s''} by \AB{C↠s''}.


\myparagraph{While true destructor}

For the \AIC{while} case we will only consider the destructor of the case where
the boolean expression evaluates to \AIC{true} as the other case is trivial. In
destructing an denotational proof of a \AIC{while} we will want to extract two
proofs, one for the body and one for the remainder of the evaluation. Luckily
for us, the destruction of \AIC{while} is much easier then the constructor as
Agda does not get stuck in the evaluation. Consequently the desctuctor
definition is similar to that of composition.

Like that of the composition destructor we will have to extract some
intermediate state since the body of the \AIC{while} will result in such a
value. Similarily, we will later need to show that the size of the resulting
elements are smaller then the original for termination purposes. Consequently we
will simultaneously prove that the size of the while proof is equal to the sum
of the components (plus one).

\begin{code}
[whileᵗᵗ]-destr-and-size :
  {b : Bexp} {S : While} {s s'' : State}
  → (wp : ⟨ while b run S , s ⟩ ↠ s'')
  → ℬ⟦ b ⟧ s ≡ true
  → Σ State (λ s' → Σ ((⟨ S , s ⟩ ↠ s') × ⟨ while b run S , s' ⟩ ↠ s'')
                      (λ split → size wp ≡ 1 + size (fst split) + size (snd split)))
[whileᵗᵗ]-destr-and-size {b} {S} {s} {s''} (semProof WS⇓ WS→s'') ℬ⟦b⟧s≡true
  rewrite ℬ⟦b⟧s≡true
  with inspect WS⇓
...| (later bind⇓ , ingraph WS⇓≡laterd)
  rewrite WS⇓≡laterd =
       s'
       , (((semProof S⟦S⟧s⇓ refl)
       , (semProof S⟦while⟧s'⇓
                   (trans extract-S⟦while⟧s'⇓≡s''
                          extract-bind⇓≡s'')))
       , cong suc (bind-⇓-length-add bind⇓ S⟦S⟧s⇓ S⟦while⟧s'⇓))
  where
    S⟦S⟧s⇓ : _
    S⟦S⟧s⇓ = (bind-⇓-injₗ bind⇓)
    s' : State
    s' = extract S⟦S⟧s⇓
    S⟦while⟧s'⇓ : _
    S⟦while⟧s'⇓ = (bind-⇓-injᵣ {d = (S⟦_⟧_ S s)} bind⇓)

    extract-S⟦while⟧s'⇓≡s'' : extract S⟦while⟧s'⇓ ≡ extract bind⇓
    extract-S⟦while⟧s'⇓≡s'' =
      extract-bind-⇓-injᵣ≡extract-bind-⇓ {d = (S⟦_⟧_ S s)} bind⇓
    extract-bind⇓≡s'' : extract bind⇓ ≡ s''
    extract-bind⇓≡s'' with inspect WS⇓ | inspect WS→s''
    ...| (later d , ingraph WS⇓≡later) | (WS→s'' , _)
      rewrite WS⇓≡later = WS→s''
\end{code}





\subsubsection{Denotational semantic equivalence results}

Like before we may create a relation signifying the fact that two denotational
semantic proofs (as given here) are semantically equivalent.

\begin{code}
_≌ₛ_ : (S₁ S₂ : While) → Set
_≌ₛ_ S₁ S₂ = {s s' : State} → ⟨ S₁ , s ⟩ ↠ s' ≌ ⟨ S₂ , s ⟩ ↠ s'
\end{code}

We shall show that the denotational semantics stated on this form is equivalent
to natural semantics. Like the case for the small step semantics we will first
have to define a separate proof of translating denotational to natural semantics
because of termination reasons.

\begin{code}
DS⇒NS-wf : (S : While) {s s' : State}
        → (ds : ⟨ S , s ⟩ ↠ s')
        → (@0 _ : Acc _<_ ((size ds) + (size S)))
        → ⟨ S , s ⟩ ⇾ s'
\end{code}

This time however, we are forced to change the induction term slightly. Since it
must not necessarily be true that the \AFi{size} of the termination proof
decreases for every inductive step. However, the sum of the \AFi{size} of the
statement and \AFi{size} of the termination proof will!

\begin{code}
DS⇒NS-wf (x := x₁)
         (semProof {s = s}
                 {.(s [ x ↦ 𝓐⟦ x₁ ⟧ s ])}
                 (now .(s [ x ↦ 𝓐⟦ x₁ ⟧ s ]))
                 refl) _ =
    [assₙₛ]
DS⇒NS-wf skip (semProof (now _) refl) _ = [skipₙₛ]
\end{code}
Like before the \AIC{:=} and \AIC{skip} cases are trivial base cases.

\begin{code}
DS⇒NS-wf (S₁ ∶ S₂) p@(semProof d x) (acc getLt)
   with [comp]-destr-and-size p
...| (s' , (⟨S₁,s⟩↠s' , ⟨S₂,s'⟩↠s'') ,  size-proof)
  rewrite size-proof =
  [compₙₛ] (DS⇒NS-wf S₁ ⟨S₁,s⟩↠s'
                   (getLt ((size ⟨S₁,s⟩↠s') + size S₁)
                          (diff (size ⟨S₂,s'⟩↠s'' + size S₂) auto)))
          (DS⇒NS-wf S₂ ⟨S₂,s'⟩↠s'' (getLt ((size ⟨S₂,s'⟩↠s'') + size S₂)
                          (diff (size ⟨S₁,s⟩↠s' + size S₁) auto)))
  where
    open import Tactic.Nat.Auto
\end{code}

For the \AIC{∶} case we utilize the composition destructor
\AFn{[comp]-destr-and-size} to retrieve denotational semantic termination proofs
of the left and right hand side of the composition operator together with the
proof that their sizes sum to the size of the original proof. We can then
construct a natural semantic proof by applying these proofs to an inductive call
to the lemma. Since we need to show that the function is total we need to show
that the size of the new parameters is smaller then the original. In this case
each will differ by the size of the other statement plus the size of the
corresponding proof. Instead of giving a manual proof, we utilize
the \AgdaMacro{auto} tactic macro defined in agda-prelude.


\begin{code}
DS⇒NS-wf (if' x then S₁ else S₂) {s = s} {s' = s'}
         (semProof d x₁) (acc getLt)
  with inspect (ℬ⟦ x ⟧ s)
...| (true , ingraph ℬ⟦x⟧s≡true) rewrite ℬ⟦x⟧s≡true =
     [ifₙₛᵗᵗ] ℬ⟦x⟧s≡true (DS⇒NS-wf S₁ (semProof {S₁} {s} {s'}  d x₁)
                                 (getLt (size (semProof {S₁} {s} {s'} d x₁) + size S₁)
                                        (diff (size S₂) auto)))
     where
       open import Tactic.Nat.Auto
...| (false , ingraph ℬ⟦x⟧s≡false) rewrite ℬ⟦x⟧s≡false =
     [ifₙₛᶠᶠ] ℬ⟦x⟧s≡false (DS⇒NS-wf S₂ (semProof {S₂} {s} {s'}  d x₁)
                                 (getLt (size (semProof {S₂} {s} {s'} d x₁) + size S₂)
                                        (diff (size S₁) auto)))
     where
       open import Tactic.Nat.Auto
\end{code}

The \AIC{if} case is shown by a case split on the result of the evaluation of
the boolean expression. For either case we can rewrite input proof to retrieve
the termination proof of the respective inner expression, giving us the capacity
to induct on the proof directly. Like for the other cases we need to show that
the size of the parameters decreases, in these cases the termination proof size
does not decrease but instead, the size of the statement does. Therefore, the
difference between the sizes is the size of the other expression plus one. For
both cases we allow the \AgdaMacro{auto} tactic to provide a proof of this fact
this fact.

\begin{code}
DS⇒NS-wf (while x run S) (semProof {s = s} {s' = s''} d x₁) (acc getLt)
  with inspect (ℬ⟦ x ⟧ s)
...| true , ingraph ℬ⟦x⟧s≡true
  with [whileᵗᵗ]-destr-and-size (semProof {(while x run S)} {s} {s''} d x₁)
                              ℬ⟦x⟧s≡true
...| (s' , (body-proof , loop-proof) , size-proof)
  rewrite size-proof =
  [whileₙₛᵗᵗ] ℬ⟦x⟧s≡true
            (DS⇒NS-wf S body-proof
                      (getLt (size body-proof + size S)
                             (diff (suc (size loop-proof)) auto)))
            (DS⇒NS-wf (while x run S)
                     loop-proof
                     (getLt (size loop-proof + suc (size S))
                            (diff (size body-proof) auto)
                            ))
  where
    open import Tactic.Nat.Auto
DS⇒NS-wf (while x run S)
         (semProof {(while x run S)} {s} {s''} d s≡s'' )
         (acc getLt)
  | false , ingraph ℬ⟦x⟧s≡false
    rewrite ℬ⟦x⟧s≡false
    rewrite extract-now d
    rewrite sym s≡s'' =
      [whileₙₛᶠᶠ] ℬ⟦x⟧s≡false
\end{code}

For the \AIC{while} case we first perform a case split on the result of the
boolean evaluation. If it evaluates to true we can use the
\AFn{[whileᵗᵗ]-destr-and-size} destructor to get denotational proofs of the
evaluation of the body and the rest of the evaluation. Using these properties we
can inductively transform these proofs to that of natural semantics which we
then can apply to the \AIC{[whileₙₛᶠᶠ]} constructor. Similar to the other cases
we need to provide a proof of well foundedness. In this case, the inductive case
for the body is differs by the size of the loop proof plus one and the size of
the tail of the execution differs by the size of the body proof. Both of these
differences are proved by the \AgdaMacro{auto} tactic.

For the false case, all we have to do is to rewrite the proof with the knowledge
that the boolean expression evaluates to \AIC{false}, and then show that the
initial state is equivalent to the final state, but is otherwise trivially
provable by the \AIC{[whileₙₛᶠᶠ]} constructor.


Using the above definition we are now ready to show the equivalence between
denotational and natural semantics.

\begin{code}
DS≡NS : {S : While} {s s' : State}
       → ⟨ S , s ⟩ ↠ s' ≌ ⟨ S , s ⟩ ⇾ s'
_≌_.from (DS≡NS {S} {s} {s'}) ⟨S,s⟩↠s' =
  DS⇒NS-wf S ⟨S,s⟩↠s' (wfNat (size ⟨S,s⟩↠s' + size S))
\end{code}
For the denotational to natural semantic case we apply the recently defined
\AFn{DS⇒NS-wf} function applied with an \AFn{Acc} instance defined on the
natural numbers.

The natural to denotational semantic direction is a simple structural induction
on the structure of the natural semantics. Similar to that of natural semantics
to small-step semantics. I will therefore not explain it again.

\begin{code}
_≌_.to (DS≡NS {x₁ := x₂} {s} {.(stateSet s x₁ (𝓐⟦ x₂ ⟧ s))}) [assₙₛ] =
  [ass]
_≌_.to (DS≡NS {skip} {s} {.s}) [skipₙₛ] =
  [skip]
_≌_.to (DS≡NS {S ∶ S₁} {s} {s'}) ([compₙₛ] ns₁ ns₂) =
  [comp] (_≌_.to DS≡NS ns₁)  (_≌_.to DS≡NS ns₂)
_≌_.to (DS≡NS {if' x₁ then S else S₁} {s} {s'}) ([ifₙₛᵗᵗ] ℬ⟦b⟧ ns) =
  [ifᵗᵗ] ℬ⟦b⟧ (_≌_.to DS≡NS ns)
_≌_.to (DS≡NS {if' x₁ then S else S₁} {s} {s'}) ([ifₙₛᶠᶠ] ¬ℬ⟦b⟧ ns) =
  [ifᶠᶠ] ¬ℬ⟦b⟧ (_≌_.to DS≡NS ns)
_≌_.to (DS≡NS {while x₁ run S} {s} {s'}) ([whileₙₛᵗᵗ] ℬ⟦b⟧ ns₁ ns₂) =
  [whileᵗᵗ] ℬ⟦b⟧ (_≌_.to DS≡NS ns₁) (_≌_.to DS≡NS ns₂)
_≌_.to (DS≡NS {while x₁ run S} {s} {.s}) ([whileₙₛᶠᶠ] ¬ℬ⟦b⟧) =
  [whileᶠᶠ] ¬ℬ⟦b⟧
\end{code}

\section{Axiomatic semantics / Hoare logic}

A common proof technique in formal methods is the utilization of Hoare logic
since it can give a somewhat smooth way to perform validaiton of programs. The
book defines the Hoare logic as a relation tripple $⦃ P ⦄ S ⦃ Q ⦄$ such that
assuming some predicate $P$ holds on the initial state, then if $S$ terminates
evaluated on the state the predicate $Q$ holds on the terminal state. There are
of course two variants of hoare logic, one that also shows that the program
terminates. However, we will only define the partially correct variant.  The
book refers to this kind of semantics as \textit{axiomatic semantics} and we
will therefore use the same name here.

Since the symbols \AgdaSymbol{⦃⦄⦇⦈} are key symbols in Agda we use the
\AgdaSymbol{⟦⟧} symbols instead.

When defining axiomatic semantics one must consider what the logical language of
the predicates should be, the book uses a arbirary, slightly stronger form of
the boolean expression language in its examples. Here I will utilize Agda as the
logical language instead. Since we will need to perform substitutions on the
state of the predicates I define an instance of \AD{Substitutable} fore state
predicates.
\begin{code}
instance
  SubstitutableStatePred :
    ∀ {ℓ}
     → Substitutable (State → Set ℓ) (State → Int)
  _[_↦_] {{SubstitutableStatePred}} P var val =
        λ state → P (stateSet state var (val state))
\end{code}

Similar to the other semantics we define a set of constructors \textit{axioms}
acting as constuctors of the datatype.
\begin{code}
data ⟦_⟧_⟦_⟧ {ℓ} : (State → Set ℓ) → While → (State → Set ℓ) → Set (lsuc ℓ) where
\end{code}
In general, we define the axiomatic semantics as a tripple \AD{⟦} \AB{P} \AD{⟧}
\AB{S} \AD{⟦} \AB{P} \AD{⟧} such that \AB{P} and \AB{Q} are Agda predicates of
type (\AD{State} \AgdaSymbol{→} \AD{Set} \AB{ℓ}).

\begin{code}
  [skipₐₓ] : {P : State → Set ℓ} → ⟦ P ⟧ skip ⟦ P ⟧
\end{code}
For skip, we say that if P holds initially then P holds afterwards.
\begin{code}
  [assₐₓ] : {var : Var} {val : Aexp} {P : State → Set ℓ}
            → ⟦ (P [ var ↦ (𝓐⟦ val ⟧_) ]) ⟧ (var := val) ⟦ P ⟧
\end{code}

For assignment we say that if there exists some preidcate \AD{P} that holds true
with a state where the variable is assigned to the computev value then \AD{P}
will hold true after the assigment is completed.

\begin{code}
  [compₐₓ] : {S₁ S₂ : While} {P Q R : State → Set ℓ}
             → ⟦ P ⟧ S₁ ⟦ Q ⟧ → ⟦ Q ⟧ S₂ ⟦ R ⟧
             → ⟦ P ⟧ (S₁ ∶ S₂) ⟦ R ⟧
\end{code}

For composision we say that if some \AB{P} \AD{⟧} \AB{S₁} \AD{⟦} \AB{Q} \AD{⟧}
and \AD{⟦} \AB{Q} \AD{⟧} \AB{S₂} \AD{⟦} \AB{R} \AD{⟧} then clearly we can show
that if \AB{P} holds as a preccondition of the coposision then \AB{R} must hold
as a post condition.
\begin{code}
  [ifₐₓ] : {S₁ S₂ : While} {b : Bexp} {P Q : State → Set ℓ}
           → ⟦ (λ s → (ℬ⟦ b ⟧ s ≡ true) × P s) ⟧ S₁ ⟦ Q ⟧
           → ⟦ (λ s → (ℬ⟦ b ⟧ s ≡ false) × P s) ⟧ S₂ ⟦ Q ⟧
           → ⟦ P ⟧ (if' b then S₁ else S₂) ⟦ Q ⟧
\end{code}

For the case of the \AIC{if'} statement we want to show that both possible paths
will ensure the same post-condition such that it is composable with the other
axioms of the system. Therefore, given that some pre-condition \AB{P} holds, and
we can show that for either path entails \AB{Q} given that the knowledge of the
boolean evaluation result (and \AB{P}). Then clearly \AB{Q} must hold as a post
condition for the entire statement.
\begin{code}
  [whileₐₓ] : {S : While} {b : Bexp} {P : State → Set ℓ}
           → ⟦ (λ s → (ℬ⟦ b ⟧ s ≡ true) × P s) ⟧ S ⟦ P ⟧
           → ⟦ P ⟧ (while b run S) ⟦ (λ s → (ℬ⟦ b ⟧ s ≡ false) × P s) ⟧
\end{code}
For the while constuct we can only generally show that some kind of invariant
\AB {P} holds for the expression in conjuction with the respective boolean
expression evalion result.

\begin{code}
  [consₐₓ] : {S : While} {P P' Q' Q : State → Set ℓ}
           → ((s : State) → P s → P' s) → ((s : State) → Q' s → Q s)
           → ⟦ P' ⟧ S ⟦ Q' ⟧
           → ⟦ P ⟧ S ⟦ Q ⟧
\end{code}

The cons rule is meant to act as a way for the prover to inject extra logic that
may may modify the pre and post condition initally. It allows for the user to
insert arbitrary changes of the pre and post conidion at any place in the
derivation tree.


\myparagraph{Example 9.9}

In order to perform a sanity check that the defined axiomatic semantics is capable of
analyzing programs we will consider exercise 9.9 from the book that shows the
partial correctness of a program that computes the faculty of some value. We
will not go into any greater detail of the proof, but only leave it as a proof
that the system works.

Since the exercise requires the concept of faculty to be present in the logic
system we start by defining a naive version of it for the natural
integer.

\begin{code}
fac : Nat → Nat
fac 0 = 1
fac (suc n ) = (suc n) * fac n
\end{code}

Now, since our axiomatic semantics is concerned with the integers we have to
define a another function who's domain accepts the negative integers. We will
here only say that if the input value is negative the function returns negative
one (-1).

\begin{code}
faci : Int → Int
faci (pos n) = fromNat $ fac n
faci (negsuc n) = -1
\end{code}

Before observing the proposition and proof I want to state that I believe
general tactics defined for the state would make the proof conditions simpler.

\begin{code}
open import Prelude.Int.Properties
ex-9-9 : {n : Nat}
         → ⟦ (λ s → get s x ≡ fromNat n) ⟧
           (y := 1 ∶ while (!' (𝕧 x ==' 1))
                    run (y := (𝕧 y * 𝕧 x) ∶ x := (𝕧 x - 1)))
           ⟦ (λ s → get s y ≡ fromNat (fac n)) ⟧
ex-9-9 {n} =
  [compₐₓ] ([consₐₓ] (λ s p → trans (lookup[y]∘set[x]≡lookup[y]
                                      s x y 1 (λ ())) p
                                   , lookup[x]∘set[x]≡v s y 1)
                    (λ s p → p)
                    ([assₐₓ] {P = (λ s → (get s x ≡ fromNat n)
                                       × (get s y ≡ 1))})
                    )
          ([consₐₓ] PRE⇒INV
                   INV⇒POST
                   ([whileₐₓ] {P = INV}
                              ([consₐₓ]
                                WHILE-PRE
                                (λ s p → p)
                                ([compₐₓ]
                                [assₐₓ]
                                ([assₐₓ] {P = INV})
                              )))
                    )
       where
         open import Prelude.Nat.Properties
         INV : State → Set _
         INV s =
           ((get s x > 0)
           → (((get s y) * faci (get s x)) ≡ fromNat (fac n)))
           × fromNat n ≥ (get s x)

         PRE⇒INV : (s : State) → get s x ≡ pos n × get s y ≡ pos 1 → INV s
         PRE⇒INV s (x≡n , y≡1)
           rewrite x≡n
           rewrite y≡1
           rewrite add-zero-r (fac n)
           = (λ x>0 → refl) , (diff zero refl)
         INV⇒POST : (s : State)
                   → not (get s x ==? pos 1) ≡ false × INV s
                   → get s y ≡ pos (fac n)
         INV⇒POST s (≡false , (inv-f , n≥x ))
           with a==?b⇒a≡b $ not[x]≡false⇒x≡true ≡false
         ...| x≡1 rewrite x≡1
           with inv-f (diff zero refl)
         ...| fac-eq rewrite int-mul-one-r (get s 1) = fac-eq

         WHILE-PRE : (s : State)
                   → (not (get s x ==? pos 1)) ≡ true × INV s
                   → INV ((s [ y ↦ get s y * get s x ])
                          [ x ↦ get (s [ y ↦ get s y * get s x ]) x
                                 + negsuc 0 ])
         WHILE-PRE s (≡true , (inv-f , n≥x ))
           with ==?≡false⇒≢ $ not[x]≡true⇒x≡false ≡true
         ...| foo
           rewrite lookup[y]∘set[x]≡lookup[y] s x y (get s y * get s x) (λ ())
           rewrite lookup[x]∘set[x]≡v
                      (s [ 1 ↦ get s 1 * get s 0 ])
                      x ((get s 0) - 1)
           rewrite lookup[y]∘set[x]≡lookup[y]
                     (s [ 1 ↦ get s 1 * get s 0 ])
                     y x
                     (get s 0 + negsuc 0) (λ ())
           rewrite lookup[x]∘set[x]≡v s y (get s 1 * get s 0)
             = inv-p , (lt-predIntₗ _ _ n≥x)
             where
               inv-p : 0 < (get s x - 1)
                    → get s y * get s x * faci (get s x - 1) ≡ pos (fac n)
               inv-p pred[x]>0 with int-suc (get s x) (lt-predIntᵣ _ _ pred[x]>0)
               ...| m , get[s,x]≡m
                 rewrite get[s,x]≡m
                 = get s y * pos (suc m) * faci (suc m -NZ 1)
                 ≡⟨ cong (λ z → get s y * pos (suc m) * faci z) (-NZ-spec (suc m) 1) ⟩
                   get s y * pos (suc m) * faci (diffNat (suc m) 1)
                 ≡⟨ refl ⟩
                    (get s y * pos (suc m)) * pos (fac m)
                 ≡⟨ sym (mulInt-assoc (get s y) (pos (suc m)) (pos (fac m))) ⟩
                    get s y * pos (fac (suc m))
                 ≡⟨ inv-f 0<sucm ⟩
                   pos (fac n)
                 ∎
                 where
                   0<sucm : 0 < pos (suc m)
                   0<sucm = lt-predIntᵣ 0 (pos (suc m)) pred[x]>0
\end{code}

\subsection{Properties of the axiomatic semantics}

\myparagraph{Soundness}
We want to show that the axiomatic semantics is sound, i.e if we know that some
predicate \AB{P} holds on some initial state \AB{s} and we know that the
evaluation of the statement terminates to some state \AB{s'} then \AB{Q s'}. We
will utilize the structure of the natural semantics as a proof that the
evaluation terminates.

The proof is by structural induction on the structure of the Hoare proof.

\begin{code}
AS-sound :
  ∀ {ℓ} → (S : While) → {s s' : State}
  → {P Q : State → Set ℓ}
  → ⟦ P ⟧ S ⟦ Q ⟧ → ⟨ S , s ⟩ ⇾ s'
  → P s → Q s'
AS-sound (x₁ := x₂)  [assₐₓ] [assₙₛ] Ps =
  Ps
AS-sound skip [skipₐₓ] [skipₙₛ] Ps = Ps
AS-sound (S₁ ∶ S₂)
         ([compₐₓ] {Q = R} hoareₗ hoareᵣ)
         ([compₙₛ] {s' = s'} nsₗ nsᵣ) Ps =
  AS-sound S₂ hoareᵣ nsᵣ (AS-sound S₁ hoareₗ nsₗ Ps)
AS-sound (if' b then S₁ else S₂)
         ([ifₐₓ] hoareₗ hoareᵣ)
         ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ns) Ps =
  AS-sound S₁ hoareₗ ns (ℬ⟦b⟧≡true , Ps)
AS-sound (if' b then S₁ else S₂)
         ([ifₐₓ] hoareₗ hoareᵣ)
         ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ns) Ps =
  AS-sound S₂ hoareᵣ ns (ℬ⟦b⟧≡false , Ps)
AS-sound (while b run S)
         ([whileₐₓ] hoare)
         ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true ns ns₁) Ps =
  AS-sound (while b run S) ([whileₐₓ] hoare) ns₁
              (AS-sound S hoare ns (ℬ⟦b⟧≡true , Ps))
AS-sound (while b run S)
         ([whileₐₓ] hoare)
         ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false) Ps =
  (ℬ⟦b⟧≡false , Ps)
\end{code}

For all cases except where the axiomatic semantics is utilizes the
\AIC{[consₐₓ]} rule we can show soundness by straight forward induction.

For each case where the axiomatic semantics is of type \AIC{[consₐₓ]} we will
have to perform and intermediate translation between the predicate witnesses
given the assumption.

\begin{code}
AS-sound (x₁ := x₂) {s}
         ([consₐₓ] P→P' Q'→Q hoare)
         [assₙₛ] Ps =
  Q'→Q (s [ x₁ ↦ 𝓐⟦ x₂ ⟧ s ])
       (AS-sound (x₁ := x₂) hoare [assₙₛ] (P→P' s Ps))
AS-sound skip {s}
         ([consₐₓ] P→P' Q'→Q hoare)
         [skipₙₛ] Ps =
  Q'→Q s (AS-sound skip hoare [skipₙₛ] (P→P' s Ps))
AS-sound (S₁ ∶ S₂) {s} {s'}
         ([consₐₓ] P→P' Q'→Q hoare)
         ([compₙₛ] ns ns₁) Ps =
  Q'→Q s' (AS-sound (S₁ ∶ S₂) hoare ([compₙₛ] ns ns₁) (P→P' s Ps))
AS-sound (if' b then S₁ else S₂) {s} {s'}
         ([consₐₓ] P→P' Q'→Q hoare)
         ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ns) Ps =
  Q'→Q s' (AS-sound (if' b then S₁ else S₂)
                    hoare
                    ([ifₙₛᵗᵗ] ℬ⟦b⟧≡true ns)
                    (P→P' s Ps))
AS-sound (if' b then S₁ else S₂) {s} {s'}
         ([consₐₓ] P→P' Q'→Q hoare)
         ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ns) Ps =
  Q'→Q s' (AS-sound (if' b then S₁ else S₂)
                    hoare
                    ([ifₙₛᶠᶠ] ℬ⟦b⟧≡false ns)
                    (P→P' s Ps))
AS-sound (while b run S) {s} {s'}
         ([consₐₓ] P→P' Q'→Q hoare)
         ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true ns ns₁) Ps =
  Q'→Q s' (AS-sound (while b run S)
                    hoare
                    ([whileₙₛᵗᵗ] ℬ⟦b⟧≡true ns ns₁)
                    (P→P' s Ps))
AS-sound (while b run S) {s} {s'}
         ([consₐₓ] P→P' Q'→Q hoare)
         ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false) Ps =
  Q'→Q s (AS-sound (while b run S)
                   hoare
                   ([whileₙₛᶠᶠ] ℬ⟦b⟧≡false)
                   (P→P' s Ps))
\end{code}


\section{An abstract machine}

The book defines a simple abstract machine as an example of how one may
correlate languages to that of machine code. I personally do not believe that
this particular machine is of any greater interest. However I will give it a
full wealthy for the sake of completeness. The abstract machine is built up to
evaluate a list of instructions that is capable of performing expressing the
computations needed in the basic While language.
\begin{code}
data Inst : Set where
  PUSH : Nat → Inst
  ADD : Inst
  MULT : Inst
  SUB : Inst
  TRUE : Inst
  FALSE : Inst
  EQ : Inst
  LE : Inst
  AND : Inst
  NEG : Inst
  FETCH : Var → Inst
  STORE : Var → Inst
  NOOP : Inst
  BRANCH : List Inst → List Inst → Inst
  LOOP : List Inst → List Inst → Inst
\end{code}
For some syntactic simplicity we define a type synonym \AD{Code} for \AD{List Inst}
\begin{code}
Code : Set
Code = List Inst
\end{code}
The machine utilizes a simple stack for its evaluation, since the machine
requires a stack capable of storing both boolean and integer values define the stack to be a list of either boolean or integers.
\begin{code}
AMStack : Set
AMStack = List (Either Bool Val)
\end{code}

As for the semantics of the machine, it could easily be defined by a function,
however, since the book gives the definition in the form of a small-step state
transition relation we will give the semantics as a dependent datatype as we did
for the semantic definitions.

\begin{code}
data _⊳_ : (Code × AMStack × State) → (Code × AMStack × State) → Set where
  [PUSH] : ∀ {n c e s}
         → ((PUSH n ∷ c) , e , s) ⊳
           (c , (right (fromNat n) ∷ e) , s)
  [ADD] : ∀ {z₁ z₂ c e s}
        → ((ADD ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
          (c , ( right (z₁ + z₂) ∷ e) , s)
  [MULT] : ∀ {z₁ z₂ c e s}
         → ((MULT ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
           (c , ( right (z₁ * z₂) ∷ e) , s)
  [SUB] : ∀ {z₁ z₂ c e s}
        → ((SUB ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
          (c , ( right (z₁ - z₂) ∷ e) , s)
  [TRUE] : ∀ {c e s}
         → ((TRUE ∷ c) , e , s) ⊳
           (c , ( left true ∷ e) , s)
  [FALSE] : ∀ {c e s}
          → ((FALSE ∷ c) , e , s) ⊳
            (c , ( left false ∷ e) , s)
  [EQ] : ∀ {z₁ z₂ c e s}
       → ((EQ ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
         (c , ( left (z₁ ==? z₂) ∷ e) , s)
  [LE] : ∀ {z₁ z₂ c e s}
       → ((LE ∷ c) , (right z₁ ∷ right z₂ ∷ e) , s) ⊳
         (c , ( left (z₁ ≤? z₂) ∷ e) , s)
  [AND] : ∀ {t₁ t₂ c e s}
        → ((AND ∷ c) , (left t₁ ∷ left t₂ ∷ e) , s) ⊳
          (c , ( left (t₁ && t₂) ∷ e) , s)
  [NEG] : ∀ {t c e s}
        → ((NEG ∷ c) , (left t ∷ e) , s) ⊳
          (c , (left (not t) ∷ e) , s)
  [FETCH] : ∀ {x c e s}
          → ((FETCH x ∷ c) , e , s) ⊳
            (c , (right (get s x) ∷ e) , s)
  [STORE] : ∀ {x z c e s}
          → ((STORE x ∷ c) , (right z ∷ e) , s) ⊳
            (c , e , s [ x ↦ z ])
  [NOOP] : ∀ {c e s}
         → ((NOOP ∷ c) , e , s) ⊳  (c , e , s)
  [BRANCH] : ∀ {c₁ c₂ t c e s}
           → (((BRANCH c₁ c₂) ∷ c) , (left t ∷ e) , s) ⊳
             ((if t then c₁ else c₂) ++  c , e , s)
  [LOOP] : ∀ {c₁ c₂ c e s}
           → ((LOOP c₁ c₂ ∷ c) , e , s) ⊳
             (c₁ ++ ((BRANCH (c₂ ++ [ (LOOP c₁ c₂) ] ) [ NOOP ]) ∷ c) , e , s)
\end{code}

Since the above semantics is a kind of small-step semantics we require a way
about speaking about derivation sequences. Consequently we define a type \AD{⊳⁺} similar to the small-step semantics of while.

\begin{code}
data _⊳⁺_ : (Code × AMStack × State) → (Code × AMStack × State) → Set where
  finally : ∀ {c e s c' e' s'}
          → (c , e , s) ⊳ (c' , e' , s')
          → (c , e , s) ⊳⁺ (c' , e' , s')
  _andThen_ : ∀ { c e s c' e' s' c'' e'' s''}
          → (c , e , s) ⊳ (c' , e' , s')
          → (c' , e' , s') ⊳⁺ (c'' , e'' , s'')
          → (c , e , s) ⊳⁺ (c'' , e'' , s'')
\end{code}

The main interest of defining an abstract machine is of course, to show that we
may compile the While language to machine code that calculates the same result.
In order to do so we first need to define a compiler. We will now define a
compiler for the arithmetic, boolean and statement language in three separate
functions. We will show that the following translations yields a machine with
the expected behavior in the properties section.

\begin{code}
CA⟦_⟧ : Aexp → Code
CA⟦ 𝕟 n ⟧ = [ PUSH n ]
CA⟦ 𝕧 v ⟧ = [ FETCH v ]
CA⟦ a₁ +' a₂ ⟧ = CA⟦ a₂ ⟧ ++ CA⟦ a₁ ⟧ ++ [ ADD ]
CA⟦ a₁ *' a₂ ⟧ = CA⟦ a₂ ⟧ ++ CA⟦ a₁ ⟧ ++ [ MULT ]
CA⟦ a₁ -' a₂ ⟧ = CA⟦ a₂  ⟧ ++ CA⟦ a₁ ⟧ ++ [ SUB ]
\end{code}

\begin{code}
CB⟦_⟧ : Bexp → Code
CB⟦ true' ⟧ = [ TRUE ]
CB⟦ false' ⟧ = [ FALSE ]
CB⟦ a₁ ==' a₂ ⟧ = CA⟦ a₂ ⟧ ++ CA⟦ a₁ ⟧ ++ [ EQ ]
CB⟦ a₁ <=' a₂ ⟧ = CA⟦ a₂ ⟧ ++ CA⟦ a₁ ⟧ ++ [ LE ]
CB⟦ !' b ⟧ = CB⟦ b ⟧ ++ [ NEG ]
CB⟦ b₁ &&' b₂ ⟧ = CB⟦ b₂ ⟧ ++ CB⟦ b₁ ⟧ ++ [ AND ]
\end{code}

\begin{code}
CS⟦_⟧ : While → Code
CS⟦ v := a ⟧ = CA⟦ a ⟧ ++ [ STORE v ]
CS⟦ skip ⟧ = [ NOOP ]
CS⟦ c₁ ∶ c₂ ⟧ = CS⟦ c₁ ⟧ ++ CS⟦ c₂ ⟧
CS⟦ if' b then c₁ else c₂ ⟧ = CB⟦ b ⟧ ++ [ BRANCH CS⟦ c₁ ⟧ CS⟦ c₂ ⟧ ]
CS⟦ while b run c ⟧ = [ LOOP CB⟦ b ⟧ CS⟦ c ⟧ ]
\end{code}


\subsection{Properties of AM}

\myparagraph{No derivation sequence from empty code}

Later it will be useful to have a lemma stating there exist no derivation
sequence from a state with an empty code sequence.

\begin{code}
no-deriv-seq-from-empty-code :
   ∀ {e s c' e' s'}
   → ¬ ([] , e , s) ⊳⁺ (c' , e' , s')
no-deriv-seq-from-empty-code (finally ())
no-deriv-seq-from-empty-code (() andThen _)
\end{code}

This proof is (almost) trivially provable given that we expand on the derivation
sequence type. The only reason this lemma is defined is because it is otherwise
a bit cumbersome to do in the middle of other proofs.


\myparagraph{Machine determinism}

As usual it is quite useful to know that the semantic is deterministic. We shall
show this property in two lemmas the first one shows that a single step in the
machine is deterministic. Secondly we shall show that all derivation sequences
resulting in a terminating state (i.e no code left to execute) is deterministic.

\begin{code}
AM-step-deterministic :
  ∀ {c e s c' e' s' c'' e'' s''}
  → (c , e , s) ⊳ (c' , e' , s')
  → (c , e , s) ⊳ (c'' , e'' , s'')
  → (c' , e' , s') ≡ (c'' , e'' , s'')
AM-step-deterministic [PUSH] [PUSH] = refl
AM-step-deterministic [ADD] [ADD] = refl
AM-step-deterministic [MULT] [MULT] = refl
AM-step-deterministic [SUB] [SUB] = refl
AM-step-deterministic [TRUE] [TRUE] = refl
AM-step-deterministic [FALSE] [FALSE] = refl
AM-step-deterministic [EQ] [EQ] = refl
AM-step-deterministic [LE] [LE] = refl
AM-step-deterministic [AND] [AND] = refl
AM-step-deterministic [NEG] [NEG] = refl
AM-step-deterministic [FETCH] [FETCH] = refl
AM-step-deterministic [STORE] [STORE] = refl
AM-step-deterministic [NOOP] [NOOP] = refl
AM-step-deterministic [BRANCH] [BRANCH] = refl
AM-step-deterministic [LOOP] [LOOP] = refl
\end{code}

The deterministic step property is trivial and provable by \AIC{refl} given a
proper expansion.

\begin{code}
AM-deterministic :
  ∀ {c e s e' s' e'' s''}
  → (c , e , s) ⊳⁺ ([] , e' , s')
  → (c , e , s) ⊳⁺ ([] , e'' , s'')
  → (e' , s') ≡ (e'' , s'')
\end{code}

In order to speak about the determinism of the abstract machine we will confine
ourselves to only speak about the full evaluation of the code. When the machine
is fully evaluated the remaining code sequence is empty.

\begin{code}
AM-deterministic (finally am₁) (finally am₂) =
  snd $ pair-inj $ AM-step-deterministic am₁ am₂
\end{code}

If both derivation sequences are done we may apply the
\AFn{AM-step-deterministic} lemma directly.

\begin{code}
AM-deterministic (finally step₁) (step₂ andThen am)
  with pair-inj (AM-step-deterministic step₁ step₂)
...| []≡c''' , _ rewrite sym ([]≡c''') =
  ⊥-elim $ no-deriv-seq-from-empty-code am
AM-deterministic (step₁ andThen am) (finally step₂)
  with pair-inj (AM-step-deterministic step₂ step₁)
...| []≡c''' , _ rewrite sym ([]≡c''') =
  ⊥-elim $ no-deriv-seq-from-empty-code am
\end{code}

Given that one termination sequence states that the program terminates and the
other states that there exists more steps before completion we can derive a
contradiction since we can show that the remaining code sequence must be empty
together with the \AFn{no-deriv-seq-from-empty-code} stating that there exist
no derivation sequence from a state with an empty code sequence.

\begin{code}
AM-deterministic (step₁ andThen am₁) (step₂ andThen am₂)
  rewrite AM-step-deterministic step₁ step₂ =
    AM-deterministic am₁ am₂
\end{code}

For the cases where both derivation sequences states that there exists more
steps until termination we may rewrite using the determinism of the step to show
that the intermediate states are equivalent, and then take the inductive step on
the tail of both derivation sequences.

\myparagraph{Step terminating single instruction}

Another useful property of the abstract machine semantics is to show that if the
machine terminates in a single step, then it must necessarily be so that the
initial code segment only contains a single instruction.

\begin{code}
step-terms-tail-empty :
  ∀ {i c e s e' s'}
  → (i ∷ c , e , s) ⊳ ([] , e' , s')
  → c ≡ []
step-terms-tail-empty {PUSH x} [PUSH]   = refl
step-terms-tail-empty {ADD}     [ADD]   = refl
step-terms-tail-empty {MULT}    [MULT]  = refl
step-terms-tail-empty {SUB}     [SUB]   = refl
step-terms-tail-empty {TRUE}    [TRUE]  = refl
step-terms-tail-empty {FALSE}   [FALSE] = refl
step-terms-tail-empty {EQ}      [EQ]    = refl
step-terms-tail-empty {LE}      [LE]    = refl
step-terms-tail-empty {AND}     [AND]   = refl
step-terms-tail-empty {NEG}     [NEG]   = refl
step-terms-tail-empty {FETCH x} [FETCH] = refl
step-terms-tail-empty {STORE x} [STORE] = refl
step-terms-tail-empty {NOOP}    [NOOP]  = refl
step-terms-tail-empty {BRANCH [] x₁} {c} {e = left true ∷ e} {s} p₁ =
   fst $ pair-inj (AM-step-deterministic [BRANCH] p₁)
step-terms-tail-empty {BRANCH x@(_ ∷ _) x₁} {c} {e = left true ∷ e} {s} p₁
  with fst $ pair-inj $ AM-step-deterministic [BRANCH] p₁
...| ()
step-terms-tail-empty {BRANCH x []} {e = left false ∷ e} p₁ =
   fst $ pair-inj (AM-step-deterministic [BRANCH] p₁)
step-terms-tail-empty {BRANCH x x₁@(_ ∷ _)} {e = left false ∷ e} p₁
  with fst $ pair-inj $ AM-step-deterministic [BRANCH] p₁
...| ()
step-terms-tail-empty {LOOP x x₁} {c} {e} {s} p₁ =
  ⊥-elim (xs++y∷ys≢[]
            _ _ _
            (fst $ pair-inj $ AM-step-deterministic [LOOP] p₁))
\end{code}




\myparagraph{Consing step proof}

We will soon want to show that we can concatenate derivation sequences given
that the intermediate state matches. In order to do so we have to define a kind
of inductive base case showing that is possible cons the proof of one single
step to the head of a derivation sequence.

\begin{code}
cons-terminating :
  ∀ {i e s c' e' s' c'' e'' s''}
  → ( [ i ] , e , s) ⊳ ([] , e' , s')
  → ( c' , e' , s') ⊳⁺ ( c'' , e'' , s'')
  → ( i ∷ c' , e , s) ⊳⁺ ( c'' , e'' , s'')
\end{code}

Since all instructions except for \AIC{BRANCH} and \AIC{LOOP} are identical we
will show them all simultaneously. For all such cases it suffices to cons the
given termination proof to the head of the derivation sequence.
\begin{code}
cons-terminating {PUSH x}  [PUSH]  am = [PUSH] andThen am
cons-terminating {ADD}     [ADD]   am = [ADD] andThen am
cons-terminating {MULT}    [MULT]  am = [MULT] andThen am
cons-terminating {SUB}     [SUB]   am = [SUB] andThen am
cons-terminating {TRUE}    [TRUE]  am = [TRUE] andThen am
cons-terminating {FALSE}   [FALSE] am = [FALSE] andThen am
cons-terminating {EQ}      [EQ]    am = [EQ] andThen am
cons-terminating {LE}      [LE]    am = [LE] andThen am
cons-terminating {AND}     [AND]   am = [AND] andThen am
cons-terminating {NEG}     [NEG]   am = [NEG] andThen am
cons-terminating {FETCH x} [FETCH] am = [FETCH] andThen am
cons-terminating {STORE x} [STORE] am = [STORE] andThen am
cons-terminating {NOOP}    [NOOP]  am = [NOOP] andThen am
\end{code}

For the cases where the instruction is some kind of branch the property only
holds true given that the chosen branch does not result in any more
instructions.
\begin{code}
cons-terminating {BRANCH [] _} {e = left true ∷ e} step am
  with tripple-inj $ AM-step-deterministic [BRANCH] step
...| _ , e≡e' , s≡s'  rewrite e≡e' rewrite s≡s' =
   [BRANCH] andThen am
cons-terminating {BRANCH (_ ∷ _) _} {e = left true ∷ e} step am
  with tripple-inj $ AM-step-deterministic [BRANCH] step
...| () , e≡e' , s≡s'
cons-terminating {BRANCH _ [] } {e = left false ∷ e} step am
  with tripple-inj $ AM-step-deterministic [BRANCH] step
...| _ , e≡e' , s≡s'  rewrite e≡e' rewrite s≡s' =
   [BRANCH] andThen am
cons-terminating {BRANCH _ (_ ∷ _)} {e = left false ∷ e} step am
  with tripple-inj $ AM-step-deterministic [BRANCH] step
...| () , e≡e' , s≡s'
\end{code}

Since the loop semantics always appends at least one \AIC{BRANCH} instruction to
the code sequence a proof that it would terminate in one step is absurd.

\begin{code}
cons-terminating {LOOP x x₁} step am =
  ⊥-elim (xs++y∷ys≢[]
            _ _ _
            (fst $ pair-inj $ AM-step-deterministic [LOOP] step))
\end{code}

\myparagraph{Prepending a terminating sequence}

The concatenation of derivation sequences turns out to be one of the more
powerful properties of the abstract machine semantics. We will now have a look a
the proof of this property.  The proof is by structural induction on the
derivation sequence that is to be prepended.

\begin{code}
prepend-terminating :
  ∀ {c₁ c₂ e s e' s' c'' e'' s''}
  → (c₁ , e , s) ⊳⁺ ([] , e' , s')
  → (c₂ , e' , s') ⊳⁺ (c'' , e'' , s'')
  → (c₁ ++ c₂ , e , s) ⊳⁺ (c'' , e'' , s'')
\end{code}
\begin{code}
prepend-terminating {c₁ = []} am₁ am₂ =
  ⊥-elim $ no-deriv-seq-from-empty-code am₁
\end{code}
As shown earlier, if the code sequence is empty it is impossible to have a proof
that it does anything.

\begin{code}
prepend-terminating {c₁ = i₁ ∷ c₁} (finally step) am
  with step-terms-tail-empty step
...| c₁≡[] rewrite c₁≡[] = cons-terminating step am
\end{code}
In case there is only one final step left before the termination of the sequence
the goal is equivalent to that of \AFn{cons-terminating} and we utilize this
property.


All cases except for \AIC{BRANCH} and \AIC{LOOP} is similar, and are solved by induction on the tail of the first derivation sequence.
\begin{code}
prepend-terminating ([PUSH] andThen am₁) am₂ =
  [PUSH] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([ADD] andThen am₁) am₂ =
  [ADD] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([MULT] andThen am₁) am₂ =
  [MULT] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([SUB] andThen am₁) am₂ =
  [SUB] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([TRUE] andThen am₁) am₂ =
  [TRUE] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([FALSE] andThen am₁) am₂ =
  [FALSE] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([EQ] andThen am₁) am₂ =
  [EQ] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([LE] andThen am₁) am₂ =
  [LE] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([AND] andThen am₁) am₂ =
  [AND] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([NEG] andThen am₁) am₂ =
  [NEG] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([FETCH] andThen am₁) am₂ =
  [FETCH] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([STORE] andThen am₁) am₂ =
  [STORE] andThen (prepend-terminating am₁ am₂)
prepend-terminating ([NOOP] andThen am₁) am₂ =
  [NOOP] andThen (prepend-terminating am₁ am₂)
\end{code}


For the branch cases we must consider each possible path. But both paths are
defined similarly. I.e the selected branch \AB{xs} will be appended to the head
of the sequence, a direct inductive application yields a result where the code
sequence is of type (\AB{xs} \AFn{++} \AB{c₁}) ++ \AB{c₂} but the goal expect an
element of type \AB{xs} \AFn{++} (\AB{c₁} ++ \AB{c₂}). To solve this we utilize
the commutative monoid properties of list concatenation to rewrite the type of the inductive case.

\begin{code}
prepend-terminating {c₁ = BRANCH xs _ ∷ c₁}
                    {c₂ = c₂}
                    {e = left true ∷ e}
                    ([BRANCH] andThen am₁) am₂
  with (prepend-terminating am₁ am₂)
...| tail-proof rewrite (monoid-comm xs c₁ c₂) =
     [BRANCH] andThen tail-proof
prepend-terminating {c₁ = BRANCH _ xs ∷ c₁}
                    {c₂ = c₂}
                    {e = left false ∷ e}
                    ([BRANCH] andThen am₁) am₂
  with (prepend-terminating am₁ am₂)
...| tail-proof rewrite (monoid-comm xs c₁ c₂) =
     [BRANCH] andThen tail-proof
\end{code}

The loop case is very similar to the branch case but we have to append a more
specific kind of sequence.

\begin{code}
prepend-terminating {c₁ = LOOP guard body ∷ c₁}
                    {c₂ = c₂}
                    ([LOOP] andThen am₁) am₂
  with (prepend-terminating am₁ am₂)
...| tail-proof rewrite (monoid-comm
                          guard
                          (BRANCH (body ++ [ LOOP guard body ])
                                  [ NOOP ] ∷ c₁)
                          c₂) =
     [LOOP] andThen tail-proof
\end{code}





\myparagraph{Arithmetic behavior}

The first step of proving that the abstract machine translation matches the
other semantics is to show that the arithmetic translation behaves in a way in
which we expect. For the case of arithmetic we are interested in showing that
any code sequence resulting from our translation for any initial stack and state
will result in the return value of the arithmetic semantics being located at the front of the stack in the final configuration. Furthermore we want to make sure that the state is otherwise unchanged.

The following proof is by induction on the arithmetic syntax.

\begin{code}
airth-behavior :
  ∀ {e s} (a : Aexp)
  → ( CA⟦ a ⟧ , e , s) ⊳⁺ ( [] , right (𝓐⟦ a ⟧ s) ∷ e , s)
airth-behavior {e} {s} (𝕟 x) = finally [PUSH]
airth-behavior {e} {s} (𝕧 x) = finally [FETCH]
\end{code}

For both cases of constants and variable reference the proof is trivial and
shown by the rule of the corresponding instruction

\begin{code}
airth-behavior (a₁ +' a₂) =
  prepend-terminating (airth-behavior a₂)
    (prepend-terminating (airth-behavior a₁)
                         (finally [ADD]))
airth-behavior (a₁ *' a₂) =
  prepend-terminating (airth-behavior a₂)
    (prepend-terminating (airth-behavior a₁)
                         (finally [MULT]))
airth-behavior (a₁ -' a₂) =
  prepend-terminating (airth-behavior a₂)
    (prepend-terminating (airth-behavior a₁)
                         (finally [SUB]))
\end{code}

For all other cases we the result of the translation will yield a list of the
form \AFn{CA⟦} \AB{a₁} \AFn{⟧} \AFn{++} \AFn{CA⟦} \AB{a₂} \AFn{⟧} \AFn{++}
\AIC{[} \AB{instr} \AIC{]} where \AB{instr} is some of \AIC{EQ}, \AIC{LE},
\AIC{AND}. By induction we can show that the evaluation of the sub-expressions
will result in the kind of state we wanted. However since we want to construct a
proof of the concatenation of these derivation sequences we utilize the
\AFn{prepend-terminating} lemma that allows us to prepend terminating sequences.


\myparagraph{Boolean machine behavior}

Similar to the arithmetic behavior we want to show that machine evaluations
result in a state where the front of the stack contains a boolean value
corresponding to the boolean semantics defined earlier.

Since the proof is similar to the arithmetic case I will not explain it.
\begin{code}
bool-behavior :
  ∀ {e s} (b : Bexp)
  → ( CB⟦ b ⟧ , e , s) ⊳⁺ ( [] , left (ℬ⟦ b ⟧ s) ∷ e , s)
bool-behavior true' = finally [TRUE]
bool-behavior false' = finally [FALSE]
bool-behavior (a₁ ==' a₂) =
  prepend-terminating (airth-behavior a₂)
    (prepend-terminating (airth-behavior a₁)
                         (finally [EQ]))
bool-behavior (a₁ <=' a₂) =
  prepend-terminating (airth-behavior a₂)
    (prepend-terminating (airth-behavior a₁)
                         (finally [LE]))
bool-behavior (!' b) =
  prepend-terminating (bool-behavior b) (finally [NEG])
bool-behavior (b₁ &&' b₂) =
  prepend-terminating (bool-behavior b₂)
    (prepend-terminating (bool-behavior b₁)
                         (finally [AND]))

\end{code}



\myparagraph{Statement behavior}

The final part of the puzzle is to show that the semantics of the language
determines the evaluation of the machine. In this case we shall show that if we
are given some natural semantic derivation \ANS{\AB{S}}{\AB{s}}{\AB{s'}} then it
must be so that if we run the machine from an initial state where the stack is
empty it will terminate with a final state \AB{s'}.

Note that this is half of the relation required for a semantically equality
between the machine behavior on the translation of While. We will not show the
other half in the document.

\begin{code}
NS⇒AM :
  { S : While} {s s' : State}
  → ⟨ S , s ⟩ ⇾ s'
  → ( CS⟦ S ⟧ , [] , s ) ⊳⁺ ( [] , [] , s' )
\end{code}

\begin{code}
NS⇒AM { v := a } [assₙₛ] =
  prepend-terminating (airth-behavior a) (finally [STORE])
\end{code}

For the case of assignment we first show that the arithmetic behavior gives us
the appropriate terminating derivation sequence, since it is terminating we can
append the simple derivation sequence of the final store value that adds the
value to the state.

\begin{code}
NS⇒AM [skipₙₛ] = finally [NOOP]
\end{code}
The \AIC{skip} is trivialy shown by \AIC{[NOOP]}

\begin{code}
NS⇒AM ([compₙₛ] ⟨S,s⟩⇾s' ⟨S,s⟩⇾s'') =
  prepend-terminating (NS⇒AM ⟨S,s⟩⇾s') (NS⇒AM ⟨S,s⟩⇾s'')
\end{code}

For the composition we utilize induction on the child derivation trees. The
composision property is again given by \AFn{prepend-terminating}.

\begin{code}
NS⇒AM ([ifₙₛᵗᵗ]  {S₁} {S₂} {s} {s'} { b = b } ℬ⟦b⟧≡true ⟨S₁,s⟩⇾s')
  with (bool-behavior {[]} {s} b)
...| guard-p rewrite ℬ⟦b⟧≡true =
 (prepend-terminating
   guard-p
   ([BRANCH] andThen (transport (λ z → ( z , [] , s) ⊳⁺ ([] , [] , s'))
                                (sym $ right-ident CS⟦ S₁ ⟧)
                                (NS⇒AM ⟨S₁,s⟩⇾s'))))
NS⇒AM ([ifₙₛᶠᶠ] {S₁} {S₂} {s} {s'} { b = b } ℬ⟦b⟧≡false ⟨S₂,s⟩⇾s')
  with (bool-behavior {[]} {s} b)
...| guard-p rewrite ℬ⟦b⟧≡false =
 (prepend-terminating
   guard-p
   ([BRANCH] andThen (transport (λ z → ( z , [] , s) ⊳⁺ ([] , [] , s'))
                                (sym $ right-ident CS⟦ S₂ ⟧)
                                (NS⇒AM ⟨S₂,s⟩⇾s'))))
\end{code}

For the if case we can show the property using the boolean behavior concatenated
with the tail of the execution given the knowledge that the stack value
evaluates to either true or false. Since the \AIC{[BRANCH]} semantics considers
the tail of the evaluation which is empty we need provide a derivation sequence
where the code is of type \AFn{CS⟦} \AB{S₂} \AFn{⟧} \AFn{++} \AIC{[]} we need to
rewrite the inductive using the right identity property of concatenation with
the empty list.

\begin{code}
NS⇒AM ([whileₙₛᵗᵗ] {b} {S} {s} {s'} {s''} ℬ⟦b⟧≡true ⟨S,s⟩⇾s' ⟨WS,s⟩⇾s'')
  with (bool-behavior {[]} {s} b)
...| guard-p rewrite ℬ⟦b⟧≡true =
  [LOOP]
  andThen (prepend-terminating
            guard-p
            ([BRANCH] andThen
              (transport
                (λ z → (z , [] , s) ⊳⁺
                       ([] , [] , s''))
                (sym $ right-ident (CS⟦ S ⟧ ++ [ LOOP CB⟦ b ⟧ CS⟦ S ⟧ ]))
                (prepend-terminating (NS⇒AM ⟨S,s⟩⇾s')
                                     (NS⇒AM ⟨WS,s⟩⇾s''))
              )))
\end{code}
The while true case is similar to the if case only and left unexplained.

\begin{code}
NS⇒AM ([whileₙₛᶠᶠ] {S} {s} {b} ℬ⟦b⟧≡false)
  with (bool-behavior {[]} {s} b)
...| guard-p rewrite ℬ⟦b⟧≡false =
     [LOOP] andThen (prepend-terminating
                       guard-p
                       ([BRANCH] andThen (finally [NOOP])))
\end{code}




\printbibliography
\end{document}
